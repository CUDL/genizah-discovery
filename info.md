This repository contains Chris' textmining program which he used to generate
the tag data for Genizah fragments which can be found in the CUDL database.

I asked Chris:
> [...] we were wondering about generating tag data for the Genizah items. Is
> (re)generating the tag something we'd reasonably need to do? e.g. for future
> Genizah items being added to CUDL. If so, where does the code for it live?

Chris replied:
> The tags for all items that were mentioned in the literature I gathered about
> the genizah are in the database so if you add an item and there were mentions
> of it in the literature the tags should already be there. You shouldn't need
> to regenerate the tag data. The code is in carets SVN server - can't remember
> the repo but it has genizah in the title.

This repo is the code he referrs to.
