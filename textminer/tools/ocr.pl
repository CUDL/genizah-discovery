#!/usr/bin/perl

use strict;
my $count = 1;

#my $r = `gs -dNOPAUSE -dBATCH -r300 -sDEVICE=tiffg4 -sOutputFile=scan_%d.tif @ARGV[0]`;

#print "$r\n";
#my $r = `convert scan_???.tif -crop 50%x100% +repage page_%d.tif`;

while ($count >= 0) {

	if (-e "scan_$count.tif")
	{
		my $r = `convert scan_$count.tif -page +0+0 -fuzz 75% -deskew 75% -colorspace Gray -threshold 50%  ocr_$count.tif`;
		`tesseract ocr_$count.tif Goitein_Friedman_$count`;
		`cat Goitein_Friedman_$count.txt >> clean.txt`;
		$count++;
	}
	else { $count = -100; }
}

#`rm -rf scan*`;
#`rm -rf page*`;

