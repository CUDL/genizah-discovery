#!/usr/local/bin/perl

use strict;

my $count = ($ARGV[2]);
my $text = "";

unless (open(FILE, "$ARGV[0]")) { die "Couldn't open global $ARGV[0]" }
while (<FILE>) { $text .= $_; }
close(FILE);

my @pages = split(/\f\n/, $text);
my $total = scalar(@pages);
foreach my $page (@pages) {
	unless (open(FILE, ">$ARGV[1]_$count.txt")) { die "Couldn't open global $ARGV[0]" }
	print FILE $page;
	close(FILE);
	print "Outputting page $count of $total from $ARGV[0]\n";
	$count++;
}
