#!/usr/bin/env perl
####################################################################
##Author:       Chris Stokoe
##File:         fragment-keyword.pl 
##Date:         19/05/13
##Version:      1.0
##
## Associates fragments with texts from the collection and produce
## raw frequency vectors
####################################################################
use Term::ProgressBar;

#Perl Internal Modules
use strict;
use utf8;
use encoding 'utf8';

#CPAN Venilla
use MLDBM qw(DB_File Storable);
use Fcntl qw(O_RDWR O_CREAT);
use Data::Dumper;
use XML::Simple;
use FindBin qw($Bin);
use lib "$Bin/../lib";
#Application Librairies
use library::bibliography;
use nlp::keywords;
use nlp::ngrams;
use nlp::features;
use nlp::timex;
use nlp::topic;

#####################################################################
#Global Variables#
##################

if ( scalar( @ARGV ) < 1 ) { die( "No config file specified\n" ); }
my $config = XMLin(@ARGV[0]) || die ("Couldn't parse config file)\n" );
my %citations = ();

###################################################################
sub corpus_mappings {

	my $file;

	if (@ARGV[1] eq '.') { 
		$file = "Full"; 
	} else {
        	$file = @ARGV[1];
	}

	opendir( DATA_DIR, $config->{'corpus'}) || die "Cannot open $config->{'corpus'}\n";
        while ( my $name = readdir(DATA_DIR) ) {
        	next if ( $name eq '.' or $name eq '..' );
		next if ( $name =~ m/\.git|\.svn/ );
		next unless ( $name =~ m/@ARGV[1]/ );
        	$citations{$file}{$name}++;
	}
}

######################################################################
my $keywords = nlp::keywords->new('corpus' => $config->{'corpus'},
                                  'general-stopwords' => $config->{'general-stopwords'},
                                  'corpus-stopwords' => $config->{'corpus-stopwords'},
                                  'authors-stopwords' => $config->{'authors-stopwords'});

my $ngrams = nlp::ngrams->new('corpus' => $config->{'corpus'},
                                  'general-stopwords' => $config->{'general-stopwords'},
                                  'corpus-stopwords' => $config->{'corpus-stopwords'},
                                  'authors-stopwords' => $config->{'authors-stopwords'});

my $features = nlp::features->new('corpus' =>  $config->{'corpus'},
                                  'authors-stopwords' => $config->{'authors-stopwords'});

my $temporal = nlp::timex->new('corpus' => $config->{'corpus'});

my $c = 1;
my $count = 9;
corpus_mappings();
print Dumper %citations;



foreach my $fragment (keys %citations) {
        my %document = ();
        print "Creating document representation for @ARGV[1]\n";

        my $progress = Term::ProgressBar->new({name => 'I/O write',
                                              count => $count,
                                              ETA => 'linear'});	
        my @keywords = $keywords->extract_keywords($fragment, $citations{$fragment});
	$progress->update($c++);
       my @bigrams = $ngrams->extract_ngrams($fragment, $citations{$fragment}, 2);
	$progress->update($c++);
        my @trigrams = $ngrams->extract_ngrams($fragment, $citations{$fragment}, 3);
	$progress->update($c++);
        my @qgrams = $ngrams->extract_ngrams($fragment, $citations{$fragment}, 4);
	$progress->update($c++);
        my @fivegrams = $ngrams->extract_ngrams($fragment, $citations{$fragment}, 5);
	$progress->update($c++);
	my @sixgrams = $ngrams->extract_ngrams($fragment, $citations{$fragment}, 6);
	$progress->update($c++);
	my @sevengrams = $ngrams->extract_ngrams($fragment, $citations{$fragment}, 7);
	$progress->update($c++);
	my @eightgrams = $ngrams->extract_ngrams($fragment, $citations{$fragment}, 8);
	$progress->update($c++);
 #       my (%features) = $features->extract_features($fragment, $citations{$fragment});
#	$progress->update($c++);
        my @timex = $temporal->extract_temporal_references($fragment,  $citations{$fragment});
	$progress->update($c++);
        my $dec = '<?xml version="1.0" encoding="utf-8"?>';
        my $document = {'ID' => $fragment,
                       'KEYWORDS' => {KEYWORD => \@keywords},
                        'BIGRAMS' => {BIGRAM => \@bigrams},
                        'TRIGRAMS' => {TRIGRAM => \@trigrams},
                       'QUADGRAMS' => {QUADGRAM => \@qgrams},
		'FIVEGRAMS' => {FIVEGRAM => \@fivegrams},
			'SIXGRAMS' => {SIXGRAM => \@sixgrams},
			'SEVENGRAMS' => {SEVENGRAM => \@sevengrams},
			'EIGHTGRAMS' => {EIGHTGRAM => \@eightgrams},
#                       'LOCATIONS' => {LOCATION =>  \@{$features{'LOCATIONS'}}},
#                        'PEOPLE' => {PERSON => \@{$features{'NAMES'}}},
                        'TEMPORAL' => {TIMEX => \@timex}};
	my $file = "./$fragment.xml";
        XMLout($document, XMLDecl => $dec, RootName => 'DOCUMENT', KeyAttr => [ ], OutputFile => $file);
	$progress->update($c++);
}
