#!/usr/bin/env perl
####################################################################
##Author:       Chris Stokoe
##File:         fragment-keyword.pl 
##Date:         19/05/13
##Version:      1.0
##
## Associates fragments with texts from the collection and produce
## raw frequency vectors
####################################################################
#Perl Internal Modules
use strict;
use utf8;
use encoding 'utf8';

#CPAN Venilla
use MLDBM qw(DB_File Storable);
use Fcntl qw(O_RDWR O_CREAT);
use Data::Dumper;
use XML::Simple;
use FindBin qw($Bin);
use lib "$Bin/../lib";
#Application Librairies
use library::bibliography;
use nlp::keywords;

#####################################################################
#Global Variables#
##################

if ( scalar( @ARGV ) < 1 ) { die( "No config file specified\n" ); }
my $config = XMLin(@ARGV[0]) || die ("Couldn't parse config file)\n" );
my %citations = ();

###################################################################
sub bibliographic_mappings {

        my $bibliography = library::bibliography->new('server' => $config->{'server'},
                                                      'port' => $config->{'port'},
                                                      'username' => $config->{'username'},
                                                      'password' => $config->{'password'},
                                                      'database' => $config->{'database'});

        foreach my $item ( @{ $config->{scholarly_works}{item} }) {
                my $mappings = "";
                if ($item->{'processor'} eq "manual") {
                        $mappings = $bibliography->associate_manual($item->{shortname},
                                                                    $item->{mapping},
                                                                    $item->{pageoffset},
                                                                    9999);
                }
                if ($item->{'processor'} eq "auto") {
                    $mappings = $bibliography->associate_auto($item->{shortname},
                                                             $item->{mapping},
                                                             $item->{pageoffset},
                                                             $item->{dropindex});
                }
                foreach my $fragment ( keys %{ $mappings } ) {
                        my $pointer = $citations{$fragment};
                        foreach my $citation ( keys %{ $mappings->{$fragment} }) {
                                $pointer->{$citation}++;
                        }
                        $citations{$fragment} = $pointer;
                }
        }
}

######################################################################
my $keywords = nlp::keywords->new('corpus' => $config->{'corpus'},
                                  'general-stopwords' => $config->{'general-stopwords'},
                                  'corpus-stopwords' => $config->{'corpus-stopwords'},
                                  'authors-stopwords' => $config->{'authors-stopwords'});

my $running = 0;
bibliographic_mappings();
open(FOO, ">fragments-new.csv") || die "can't open fragments.csv"; 
foreach my $fragment (keys %citations) {
	my $text;
        $running++;
        print "$running Creating document representation for $fragment\n";
	my @keywords = $keywords->extract_keywords($fragment, $citations{$fragment});
	foreach my $keyword ( @keywords ) {
			next if $keyword->{raw} <= 1;
			my $term = $keyword->{name};
			$term =~ s/^-//g;
                	$term =~ s/-$//g;
                	$term =~ s/^'//g;
                	$term =~ s/'$//g;
			for ( my $i = 1; $i <= $keyword->{raw}; $i++ ) {	
				$text .= "$term ";
			}	
		}
	$text =~ s/\s$//g;
	print FOO "$fragment,$text\n";
}
close (FOO);
