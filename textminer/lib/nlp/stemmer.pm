####################################################################################
##Author:	Chris Stokoe
##File:		stemmer.pm 
##Date:		28/02/13
##Version:	1.0
####################################################################################
package nlp::stemmer;

sub new {
	my $class = shift;
	my %defaults =  ();
	my $self = {%defaults, @_};
	bless $self, $class;
        return $self;
}

####################################################################################
sub porter {

  my $self=shift;
  my $word=shift;

  if(length($word)>2){
    	my $c='(?:[^aiueoy]|(?:(?<=[aiueo])y)|\by)';
    	my $v='(?:[aiueo]|(?:(?<![aiueo])y))';  
    	my $extra=0;
    	my $m_gt_0 = "^(?:$c+)?(?:$v+$c+){1,}(?:$v+)?\$";
    	my $m_gt_1 = "^(?:$c+)?(?:$v+$c+){2,}(?:$v+)?\$";
    	my $m_eq_1="^(?:$c+)?(?:$v+$c+){1}(?:$v+)?\$";
    	my $o="$c$v(?:[^aiueowxy])\$";
    	my $d="($c)\\1\$";

    	#STEP 1a
    	if($word =~ /(.+)sses$/){ $word=$1."ss"; }
    	elsif($word =~ /(.+)ies$/){ $word=$1."i"; }
    	elsif($word =~ /(.+[^s])s$/){ $word=$1; }
    
    	#STEP 1b
    	if($word =~ /(.+)eed$/) {
      		if (($w=$1) =~ /$m_gt_0/o){ $word=$w."ee"; }
    	}
    	elsif($word =~ /(.+)ed$/) {
      		if (($w=$1) =~ /$v/o) {
        		$word=$w;
        		$extra=1;
      		}
    	}
    	elsif($word =~ /(.+)ing$/){
      		if(($w=$1) =~ /$v/o) {
        		$word=$w;
        		$extra=1;
      		}
    	}

	if($extra){
      		if($word =~ /(.+)at$/){ $word=$1."ate"; }
      		elsif($word =~ /(.+)bl$/){ $word=$1."ble"; }
      		elsif($word =~ /(.+)iz$/){ $word=$1."ize"; }
      		elsif(($word =~ /$d/o) and ($word !~ /[lsz]$/)){ $word=substr($word,0,-1); }
      		elsif(($word =~ /$m_eq_1/o) and ($word =~ /$o/o)){ $word.='e'; }
    }
    
       #STEP 1c
       if($word =~ /(.+)y$/){
      		if (($w=$1) =~ /$v/o){
			$word=$w."i";
      		}
    	}

	#STEP 2
	my $letter=substr($word,-2,1);
    	if($letter eq "a"){
      		if($word =~ /(.+)ational$/){
        		if(($w=$1) =~ /$m_gt_0/o){ $word=$w."ate"; }
       		}
      		elsif($word =~ /(.+)tional$/){
        		if(($w=$1) =~ /$m_gt_0/o){ $word=$w."tion"; }
      		}
    	}
    	elsif($letter eq "c"){
      		if($word =~ /(.+)enci$/) {
        		if(($w=$1) =~ /$m_gt_0/o){ $word=$w."ence"; }
      		}
      		elsif($word =~ /(.+)anci$/) {
        		if(($w=$1) =~ /$m_gt_0/o){ $word=$w."ance"; }
      		}
    	}
    	elsif($letter eq "e"){
      		if($word =~ /(.+)izer$/) {
        		if(($w=$1) =~ /$m_gt_0/o){ $word=$w."ize"; }
      		}
    	}
    	elsif($letter eq "g"){
      		if($word =~ /(.+)logi$/) {
        		if(($w=$1) =~ /$m_gt_0/o){ $word=$w."log"; }
      		}
    	}
    	elsif($letter eq "l"){
      		if($word =~ /(.+)bli$/) {
        		if(($w=$1) =~ /$m_gt_0/o){ $word=$w."ble"; }
      		}
      		elsif($word =~ /(.+)alli$/) {
        		if(($w=$1) =~ /$m_gt_0/o){ $word=$w."al"; }
      		}
      		elsif($word =~ /(.+)entli$/) {
        		if(($w=$1) =~ /$m_gt_0/o){ $word=$w."ent"; }
      		}
      		elsif($word =~ /(.+)eli$/) {
        		if(($w=$1) =~ /$m_gt_0/o){ $word=$w."e"; }
      		}
      		elsif($word =~ /(.+)ousli$/) {
        		if(($w=$1) =~ /$m_gt_0/o){ $word=$w."ous"; }
      		}
    	}
    	elsif($letter eq "o"){
      		if($word =~ /(.+)ization$/) {
        		if(($w=$1) =~ /$m_gt_0/o){ $word=$w."ize"; }
      		}
      		elsif($word =~ /(.+)ation$/) {
        		if(($w=$1) =~ /$m_gt_0/o){ $word=$w."ate"; }
      		}
      		elsif($word =~ /(.+)ator$/) {
        		if(($w=$1) =~ /$m_gt_0/o){ $word=$w."ate"; }
      		}
    	}
    	elsif($letter eq "s"){
      		if($word =~ /(.+)alism$/) {
        		if(($w=$1) =~ /$m_gt_0/o){ $word=$w."al"; }
      		}
      		elsif($word =~ /(.+)iveness$/) {
        		if(($w=$1) =~ /$m_gt_0/o){ $word=$w."ive"; }
      		}
      		elsif($word =~ /(.+)fulness$/) {
        		if(($w=$1) =~ /$m_gt_0/o){ $word=$w."ful"; }
      		}
      		elsif($word =~ /(.+)ousness$/) {
        		if(($w=$1) =~ /$m_gt_0/o){ $word=$w."ous"; }
      		}
    	}
	elsif($letter eq "t"){
      		if($word =~ /(.+)aliti$/) {
        		if(($w=$1) =~ /$m_gt_0/o){ $word=$w."al"; }
      		}
      		elsif($word =~ /(.+)iviti$/) {
        		if(($w=$1) =~ /$m_gt_0/o){ $word=$w."ive"; }
      		}
      		elsif($word =~ /(.+)biliti$/) {
        		if(($w=$1) =~ /$m_gt_0/o){ $word=$w."ble"; }
      		}
    	}
	#STEP 3
    	$letter=substr($word,-1,1);
    	if($letter eq "e"){
      		if($word =~ /(.+)icate$/) {
        		if(($w=$1) =~ /$m_gt_0/o){ $word=$w."ic"; }
      		}
      		elsif($word =~ /(.+)ative$/) {
        		if(($w=$1) =~ /$m_gt_0/o){ $word=$w; }
      		}
      		elsif($word =~ /(.+)alize$/) {
        		if(($w=$1) =~ /$m_gt_0/o){ $word=$w."al"; }
      		}
    	}
    	elsif($letter eq "i"){
      		if($word =~ /(.+)iciti$/) {
        		if(($w=$1) =~ /$m_gt_0/o){ $word=$w."ic"; }
      		}
    	}
    	elsif($letter eq "l"){
      		if($word =~ /(.+)ical$/) {
        		if(($w=$1) =~ /$m_gt_0/o){ $word=$w."ic"; }
      		}
      		elsif($word =~ /(.+)ful$/) {
        		if(($w=$1) =~ /$m_gt_0/o){ $word=$w; }
      		}
    	}
    	elsif($letter eq "s"){
      		if($word =~ /(.+)ness$/) {
        		if(($w=$1) =~ /$m_gt_0/o){ $word=$w; }
      		}
    	}
	
	#STEP 4
    	$letter=substr($word,-2,1);
    	if($letter eq "a"){
      		if($word =~ /(.+)al$/) {
        		if(($w=$1) =~ /$m_gt_1/o){ $word=$w; }
      		}
    	}
    	elsif($letter eq "c"){
      		if($word =~ /(.+)ance$/) {
        		if(($w=$1) =~ /$m_gt_1/o){ $word=$w; }
      		}
      		elsif($word =~ /(.+)ence$/) {
        		if(($w=$1) =~ /$m_gt_1/o){ $word=$w; }
      		}
    	}
    	elsif($letter eq "e"){
      		if($word =~ /(.+)er$/) {
        		if(($w=$1) =~ /$m_gt_1/o){ $word=$w; }
      		}
    	}
    	elsif($letter eq "i"){
      		if($word =~ /(.+)ic$/) {
        		if(($w=$1) =~ /$m_gt_1/o){ $word=$w; }
      		}
    	}
    	elsif($letter eq "l"){
      		if($word =~ /(.+)able$/) {
        		if(($w=$1) =~ /$m_gt_1/o){ $word=$w; }
      		}
      		elsif($word =~ /(.+)ible$/) {
        		if(($w=$1) =~ /$m_gt_1/o){ $word=$w; }
      		}
    	}
    	elsif($letter eq "n"){
      		if($word =~ /(.+)ant$/) {
        		if(($w=$1) =~ /$m_gt_1/o){ $word=$w; }
      		}
      		elsif($word =~ /(.+)ement$/) {
        		if(($w=$1) =~ /$m_gt_1/o){ $word=$w; }
      		}
      		elsif($word =~ /(.+)ment$/) {
        		if(($w=$1) =~ /$m_gt_1/o){ $word=$w; }
       	 	}
      		elsif($word =~ /(.+)ent$/) {
        		if(($w=$1) =~ /$m_gt_1/o){ $word=$w; }
      		}
    	}	
    	elsif($letter eq "o"){
      		if($word =~ /(.+)ion$/) {
        		if((($w=$1) =~ /[st]$/) and ($w =~ /$m_gt_1/o)){ $word=$w; }
      		}
      		elsif($word =~ /(.+)ou$/) {
        		if(($w=$1) =~ /$m_gt_1/o){ $word=$w; }
      		}
    	}
    	elsif($letter eq "s"){
      		if($word =~ /(.+)ism$/) {
        		if(($w=$1) =~ /$m_gt_1/o){ $word=$w; }
      		}
    	}
    	elsif($letter eq "t"){
      		if($word =~ /(.+)ate$/) {
        		if(($w=$1) =~ /$m_gt_1/o){ $word=$w; }
      		}
      		elsif($word =~ /(.+)iti$/) {
       	 		if(($w=$1) =~ /$m_gt_1/o){ $word=$w; }
      		}
    	}
    	elsif($letter eq "u"){
      		if($word =~ /(.+)ous$/) {
        		if(($w=$1) =~ /$m_gt_1/o){ $word=$w; }
      		}
    	}
    	elsif($letter eq "v"){
      		if($word =~ /(.+)ive$/) {
        		if(($w=$1) =~ /$m_gt_1/o){ $word=$w; }
      		}
    	}
    	elsif($letter eq "z"){
      		if($word =~ /(.+)ize$/) {
        		if(($w=$1) =~ /$m_gt_1/o){ $word=$w; }
      		}
    	}

    	#STEP 5a
    	if($word =~ /(.+)e$/) {
      		if((($w=$1) =~ /$m_gt_1/o) or (($w =~ /$m_eq_1/o) and ($w !~ /$o/o))){ $word=$w; }
    	}
	
	#STEP 5b
    	if($word =~ /ll$/) {
      		if($word =~ /$m_gt_1/o){ $word=substr($word,0,length($word)-1); }
    	}
  }
  return $word;
}
######################################################################################
sub stem
{  

   my %step2list =
   ( 'ational'=>'ate', 'tional'=>'tion', 'enci'=>'ence', 'anci'=>'ance', 'izer'=>'ize', 'bli'=>'ble',
     'alli'=>'al', 'entli'=>'ent', 'eli'=>'e', 'ousli'=>'ous', 'ization'=>'ize', 'ation'=>'ate',
     'ator'=>'ate', 'alism'=>'al', 'iveness'=>'ive', 'fulness'=>'ful', 'ousness'=>'ous', 'aliti'=>'al',
     'iviti'=>'ive', 'biliti'=>'ble', 'logi'=>'log');

   my %step3list =
   ('icate'=>'ic', 'ative'=>'', 'alize'=>'al', 'iciti'=>'ic', 'ical'=>'ic', 'ful'=>'', 'ness'=>'');


   my $c =    "[^aeiou]";          # consonant
   my $v =    "[aeiouy]";          # vowel
   my $C =    "${c}[^aeiouy]*";    # consonant sequence
   my $V =    "${v}[aeiou]*";      # vowel sequence

   my $mgr0 = "^(${C})?${V}${C}";               # [C]VC... is m>0
   my $meq1 = "^(${C})?${V}${C}(${V})?" . '$';  # [C]VC[V] is m=1
   my $mgr1 = "^(${C})?${V}${C}${V}${C}";       # [C]VCVC... is m>1
   my $_v   = "^(${C})?${v}";                   # vowel in stem


   my ($stem, $suffix, $firstch);
   my $self = shift;
   my $w = shift;
   if (length($w) < 3) { return $w; } # length at least 3
   # now map initial y to Y so that the patterns never treat it as vowel:
   $w =~ /^./; $firstch = $&;
   if ($firstch =~ /^y/) { $w = ucfirst $w; }

   # Step 1a
   if ($w =~ /(ss|i)es$/) { $w=$`.$1; }
   elsif ($w =~ /([^s])s$/) { $w=$`.$1; }
   # Step 1b
   if ($w =~ /eed$/) { if ($` =~ /$mgr0/o) { chop($w); } }
   elsif ($w =~ /(ed|ing)$/)
   {  $stem = $`;
      if ($stem =~ /$_v/o)
      {  $w = $stem;
         if ($w =~ /(at|bl|iz)$/) { $w .= "e"; }
         elsif ($w =~ /([^aeiouylsz])\1$/) { chop($w); }
         elsif ($w =~ /^${C}${v}[^aeiouwxy]$/o) { $w .= "e"; }
      }
   }
   # Step 1c
   if ($w =~ /y$/) { $stem = $`; if ($stem =~ /$_v/o) { $w = $stem."i"; } }

   # Step 2
   if ($w =~ /(ational|tional|enci|anci|izer|bli|alli|entli|eli|ousli|ization|ation|ator|alism|iveness|fulness|ousness|aliti|iviti|biliti|logi)$/)
   { $stem = $`; $suffix = $1;
     if ($stem =~ /$mgr0/o) { $w = $stem . $step2list{$suffix}; }
   }

   # Step 3

   if ($w =~ /(icate|ative|alize|iciti|ical|ful|ness)$/)
   { $stem = $`; $suffix = $1;
     if ($stem =~ /$mgr0/o) { $w = $stem . $step3list{$suffix}; }
   }

   # Step 4

   if ($w =~ /(al|ance|ence|er|ic|able|ible|ant|ement|ment|ent|ou|ism|ate|iti|ous|ive|ize)$/)
   { $stem = $`; if ($stem =~ /$mgr1/o) { $w = $stem; } }
   elsif ($w =~ /(s|t)(ion)$/)
   { $stem = $` . $1; if ($stem =~ /$mgr1/o) { $w = $stem; } }


   #  Step 5

   if ($w =~ /e$/)
   { $stem = $`;
     if ($stem =~ /$mgr1/o or
         ($stem =~ /$meq1/o and not $stem =~ /^${C}${v}[^aeiouwxy]$/o))
        { $w = $stem; }
   }
   if ($w =~ /ll$/ and $w =~ /$mgr1/o) { chop($w); }

   # and turn initial Y back to y
   if ($firstch =~ /^y/) { $w = lcfirst $w; }
   return $w;
}
1;









