####################################################################################
##Author:       Chris Stokoe
##File:         gazetteer.pm 
##Date:         04/05/13
##Version:      1.0
####################################################################################
package nlp::features;

use strict;
use utf8;
use encoding 'utf8';
use open qw(:std :utf8);

use nlp::gazetteer::occupations qw(%occupations);
use nlp::gazetteer::religions qw (%religions);
use nlp::gazetteer::nationalities qw (%nationalities);
use nlp::gazetteer::names qw ( %names );
use nlp::gazetteer::locations qw ( %locations );
use nlp::tokeniser;
use nlp::tfidf;
use nlp::stemmer;
use nlp::editdistance;
use Data::Dumper;
use Geo::GeoNames;
use IO::Socket;

my %catch = ('cairo' => 'LOCATION',
	     'damiyatta' => 'LOCATION',
	     'qus' => 'LOCATION',
	     'hasor' => 'LOCATION',
	     'manasseh' => 'LOCATION',
	     'fustat' => 'LOCATION',
	     'Jerusalem' => 'LOCATION',
	     'yemenite' => 'NATIONALITY',
	     'arab' => 'NATIONALITY', 
	     'karaites' => 'RELIGION',
	     'berliner' => 'NATIONALITY',
	     'jewry' => 'RELIGION',
	     'rabbanite' => 'RELIGION',
	     'nagid' => 'OCCUPATION',
	     'new' => 'O',
	     'old' => 'O',
	     'genizah' => 'O',
	     'geniza' => 'O', 
	     'god' => 'O',
	     'leiden' => 'O',
	     'brill' => 'O',
	     'doc' => 'O',
	     'heb' => 'O',
	     'med' => 'O',
	     'hebrew' => 'O',
	     'son' => 'O',
	     'karaite' => 'RELIGION');

sub new {
        my $class = shift;
	 my %defaults =  ('stopword' => '1',
                          'corpus' => './',
                         );

        my $self = {%defaults, @_};
	
	$self->{'tokeniser'} = nlp::tokeniser->new('stopword' => $self->{'stopword'},
                                                   'authors' => $self->{'authors-stopwords'},
						   'minimum_length' => 0);

	$self->{'editdistance'} = new nlp::editdistance();

	$self->{'document_cache'} = ();
	$self->{'location_cache'} = ();
	$self->{'geonames'} = new Geo::GeoNames(username => 'cam_cul');
        bless $self, $class;
        return $self;
}
####################################################################################
sub _get_location_geo {

	my $self = shift;
        my $location = shift;

        my $result = $self->{'geonames'}->search('q' => $location,
                                  'maxRows' => 1,
                                  'style' => "FULL",
                                  'north' => "46",
                                  'south' => "0",
                                  'east' => "77",
                                  'west' => "-20",
                                  'fuzzy'=> "1",
				  'featureClass' => ["A","P"]);
        my $coordinates = "$result->[0]->{'lat'},$result->[0]->{'lng'}";
        my $type = $result->[0]->{'fclName'};
        my $cc = $result->[0]->{'countryCode'};
        return($coordinates, $type, $cc);
}
####################################################################################
sub _get_raw_features_from_text {

	my $self = shift;
	my $text = shift;
        my $raw = $self->{'tokeniser'}->clean($text);
        $raw =~ s/\b(I|II|III|IV|V|VI|VII|VIII)\b/ /ig;
        my $sock = IO::Socket::INET->new( PeerAddr => '127.0.0.1',
                                          PeerPort => '1234',
                                          Proto    => 'tcp',
                                  );

        die "$!" if !$sock;
#	print "$raw\n";
        print $sock $raw,"\n";
        my $responce = do {local $/;<$sock>};
#	print "-$responce\n";
        $responce =~ s/\n//g;
        my $string = "";
        my @tokens = split(/\s+/,$responce);
        my %list = ();
        my $last_tag = '';
        my $taglist = {};
        my $pos = 1;

        for my $token (@tokens) {
                my ($t, $n) = $token =~ m{(.*)/(.*)$};
                #print "\t$token --> $t,$n ";
                if (exists $names{lc($t)}) { $n = "PERSON"; }
                if (exists $locations{lc($t)}) { $n = "LOCATION"; }
                if (exists $occupations{lc($t)}) { $n = "OCCUPATION"; }
                if (exists $nationalities{lc($t)}) { $n = "NATIONALITY"; }
                if (exists $religions{lc($t)}) { $n = "RELIGION"; }
                if (exists $catch{lc($t)}) { $n = $catch{lc($t)}; }
                if (! $taglist->{$n}) {
                        $taglist->{$n} = [ ];
                }
	#	print "--> $t,$n\n";

                if ($n ne $last_tag) {
                        push @{$taglist->{$n}}, [$t, $pos++];
                } else {
                        push @{ $taglist->{$n}->[ $#{ $taglist->{$n}} ] }, [$t, $pos++];
                }

                $last_tag = $n;
                $string .= " $t//$n ";
        }

        foreach my $d (keys %$taglist) {
                foreach my $l ($taglist->{$d}) {
                        for my $i ( 0 .. $#{$l}) {
                                my $words = $l->[$i];
                                my $firstword = $words->[0];
                                my $entity = $firstword;
                                map { $entity .= ' ' . $words->[$_]->[0] } (2 .. $#$words);
                                $list{$d}{$entity}++;
                        }
                }
        }
        $list{'RAW'} = $string;
        return \%list;

}
####################################################################################
#sub _raw {
#	
#	my $self = shift;
#        my $text = shift;
#
#        my %list = ();
#
#        $text = $self->{'tokeniser'}->clean($text);
#        my $result = $self->{'stanfordNLP'}->process($text);
#
#        my $opentag = "";
#        my $string = "";
#        for my $sentence (@{$result->toArray}) {
#                my $opentag = "";
#                for my $token (@{$sentence->getTokens->toArray}) {
#                        my $t = $token->getWord;
#                        my $n = $token->getNERTag;
#                        my $p = $token->getPOSTag;
#                        #$n =~ s/PERSON1/PERSON/g;
#                        if ($n eq "O" && $opentag eq "") { $string .= " $t "; }
#                        if ($n eq "O" && $opentag ne "") {
#                                 $string .= " <\/$opentag> ";
#                                $string .= " $t ";
#                                $opentag = "";
#                        }
#                        if ($n ne "O") {
#                                if ($opentag eq "") {
#                                        $string .= " <$n> ";
#                                        $string .= " $t ";
#                                        $opentag = $n;
#                                } elsif ($opentag eq $n) {
#                                        $string .= " $t ";
#                                } elsif ($opentag ne $n ) {
#                                        $string .= " <\/$opentag> ";
#                                        $string .= " <$n> ";
#                                        $string .= " $t ";
#                                        $opentag = $n;
#                                }
#                        }
#                }
#                if ($opentag ne "") { $string .= " <\/$opentag> "; }
 #       }
#        $string =~ s/\s+/ /g;
#        return $string;
#}
###################################################################################
sub normalise_names {
	
       my $self = shift;
       my (%names) = @_;

       my @results = ();

       #Conflate scores for shortenings of type {{Abraham {ben} Yiju}}
       my %shortenings = ();
       my %seen = ();
       my @subs = ();
       my @shortenings = ();
       foreach my $outer ( sort { length($b) <=> length($a) } keys %names ) {
                next if (exists $seen{$outer} || length($outer) <= 3);
                my @components = split(/\s/, lc($outer));
                my $match = 0;
                foreach my $part (@components) { 
			if ($self->{'tokeniser'}->stopword($part)) { 
				$match = 1; 
			} 
		}
                next if $match == 1;
                my @subs = ();
                $shortenings{$outer} = $names{$outer};
                foreach my $inner ( keys %names ) {
                        next if $outer eq $inner;
			#if (($outer =~ /^$inner/) || ($outer =~ /$inner$/)) {
		 	if ($outer =~ /^$inner/) {
				#print "l/r hand join $inner -> $outer\n";
				$shortenings{$outer} += $names{$inner};
                                $seen{$inner}++;
                                push @subs, {'name' => $inner,
                                             'weight' => $names{$inner}};
			} else {

				#my $sub_distance = $self->{'editdistance'}->sub_edit_distance(lc($inner), lc($outer));
                        	#if ($sub_distance <= (length($inner) * 0.18)) {
				#	print "Sub string join $inner -> $outer\n";
                                #	$shortenings{$outer} += $names{$inner};
                                #	$seen{$inner}++;
                                #	push @subs, {'name' => $inner,
                                #             	     'weight' => $names{$inner}};
				#}
                        }
			
		}
		push @subs, {'name' => $outer,
                             'weight' => $names{$outer}};
                push @shortenings, {'name' => $outer,
                               'weight' => $shortenings{$outer},
                               'alternatives' => {'alt' => \@subs}};

	}

        #Conflate scores for shortenings of type {transliteration/spellings/ocr errors}
        %seen = ();
        @shortenings = sort { $b->{weight} <=> $a->{weight} || length($b->{name}) <=> length($a->{name}) } @shortenings;
        foreach my $candidate (@shortenings) {
                next if exists $seen{$candidate->{'name'}};
                my $score = $candidate->{'weight'};
                foreach my $alternate (@shortenings) {
                        next if $candidate->{'name'} eq $alternate->{'name'};
                        my ($distance, $length) = "";
                        if (length($candidate->{'name'}) > length($alternate->{'name'})) {
                           $distance = $self->{'editdistance'}->sub_edit_distance(lc($alternate->{'name'}), lc($candidate->{'name'}));
                           $length = length($alternate->{'name'});
                        } else {
                           $distance = $self->{'editdistance'}->sub_edit_distance(lc($candidate->{'name'}), lc($alternate->{'name'}));
                           $length = length($candidate->{'name'});
                        }
                        if ($distance <= ($length * 0.18)) {
			    #print "Levenshtein Join $alternate->{'name'} -> $candidate->{'name'} $distance $length\n";
                            $score += $names{$alternate->{'name'}};
                            push @{$candidate->{'alternatives'}{'alt'}}, {'name' => "full ".$alternate->{'name'},
                                                                 'weight' => $names{$alternate->{'name'}}};


                            $seen{$alternate->{'name'}}++;
                        }
                }
                push @results, {'name' => $candidate->{'name'},
                                'weight' => $score,
                                'alternatives' => $candidate->{'alternatives'}};

        }
        my $max = 0;
	my $total = 0;
        @results = sort { $b->{weight} <=> $a->{weight} } @results;
	foreach my $name (@results) { $total += $name->{'weight'}; }
        foreach my $candidate (@results) {
                 if ($max == 0) { $max = $candidate->{'weight'}; }
                 $candidate->{'raw'} = $candidate->{'weight'};
		 $candidate->{'percentage'} = $candidate->{'weight'} / $total;
                 $candidate->{'weight'} = $candidate->{'weight'} / $max;
        }
        return @results;
}
###################################################################################
sub normalise_locations {

	my $self = shift;
	my (%locations) = @_;
	my $max = 0;
	my %local = ();
	my @results = ();

	foreach my $place ( sort { $locations{$b} <=> $locations{$a} } keys %locations ) {
			#next if $self->{$tokeniser}->stopword($token);
                #       next if $candidates{$place} <= 2;
                        if (!exists $self->{'location_cache'}{$place}) {
                                my ($coord, $type, $cc)  = $self->_get_location_geo($place);
                                $self->{'location_cache'}{$place}{'centroid'} = $coord;
                                $self->{'location_cache'}{$place}{'type'} = $type;
                                $self->{'location_cache'}{$place}{'cc'} = $cc;
                        }
                        next if $self->{'location_cache'}{$place}{'type'} eq "";
                        if ($max == 0) { $max = $locations{$place}; }
			push @results, {'name' => $place,
					'weight' => sprintf("%.5f", $locations{$place} / $max),
					'raw' => $locations{$place},
				        'centroid' => $self->{'location_cache'}{$place}{'centroid'},
				        'country' => $self->{'location_cache'}{$place}->{'cc'}, 
				        'type' => $self->{'location_cache'}{$place}{'type'}};
       }
       return @results;
}
####################################################################################
sub extract_features {
	
	my $self = shift;
        my $fragment = shift;
	my $citations = shift;

	my %results = ();
	my %names = ();
	my %locations = ();

        foreach my $doc ( keys %{$citations} ) {
		#print "--->$doc\n";
		next if exists $self->{'cache'}{$doc};
		my $text = "";
		unless (open(FILE, "$self->{'corpus'}/$doc")) { die "Couldn't open global $self->{'corpus'}/$doc" }
        	while (<FILE>) { $text .= $_; }

		my $features = $self->_get_raw_features_from_text($text);
		
		foreach my $place ( keys %{$features->{'LOCATION'}} ) {
                        my $token = lc($place);
                        #next if $self->{'tokeniser'}->stopword($token);
                        my $name = join " ", map {ucfirst lc} split " ", $place;
                        $self->{'cache'}{$doc}{'LOCATIONS'}{$name} += $features->{'LOCATION'}->{$place};
                }

		foreach my $name (keys %{$features->{'PERSON'}}) {
		#	print "\t$name $features->{'PERSON'}->{$name}\n";
			my $token = lc($name);
			next if $token eq "'abu";
			next if $token eq "'ibn";
			next if $token eq "abu";
			next if $token eq "ibn";
			next if $token =~ /^\w\. \w\.$/;
			next if $token =~ /^j\. q\. r\./;
                        my $n = join " ", map {ucfirst lc} split " ", $name;	
			$n =~ s/\s(i|ii|iii|iv|vi|v)$/ /ig;
			$n =~ s/\s(i|ii|iii|iv|vi|v)\s/ /ig;
			$n =~ s/(ibn|[A-Za-z]\.|\')$//ig;
			$n =~ s/^(R|G|D|M)\.//ig;
			$n =~ s/\s(ibn|ben|bint)\s/ b. /ig;
			$n =~ s/^Ben\s/b. /ig;
			$n =~ s/Ben$/b./ig;
			$n =~ s/\'s$//g;
			$n =~ s/ ll //ig;
			$n =~ s/ B\. / b\. /g;
			$n =~ s/ Ibn / ibn /g;
			$n =~ s/Al-(.)/al-\u$1/g;
			$n =~ s/b\. b\./b./gi;
			$n =~ s/\s+/ /g;
                        $n =~ s/^\s//g;
                        $n =~ s/\s$//g;
			next if length($n) <= 2;
                        $self->{'cache'}{$doc}{'NAMES'}{$n} += $features->{'PERSON'}->{$name};
			#print "$token -> $n\n";
		} 

		$self->{'cache'}{$doc}{'DATES'} = $features->{'DATE'};
	#	print "$doc\n";
	#	print Dumper $features->{'DATE'};
        }

	foreach my $doc ( keys %{$citations} ) {
		foreach my $name ( sort keys %{$self->{'cache'}{$doc}{'NAMES'}}) {
			$names{$name} += $self->{'cache'}{$doc}{'NAMES'}{$name}; 
		}
		foreach my $place (keys %{$self->{'cache'}{$doc}{'LOCATIONS'}}) {
			 $locations{$place} += $self->{'cache'}{$doc}{'LOCATIONS'}{$place};
		}
	}

	@{$results{'NAMES'}} = $self->normalise_names(%names);
	#$results{'RAW_LOCATIONS'} = \%locations;
	#$results{'RAW_NAMES'} = \%names;
	@{$results{'LOCATIONS'}} = $self->normalise_locations(%locations);
	#my $result{'TIMEX'} = normalise_timex(); 
	
	#print Dumper %candidates;
	return %results;
	
}
#############################################################################################
1;
