####################################################################################
##Author:       Chris Stokoe
##File:         keywords.pm 
##Date:         04/05/13
##Version:      1.0
####################################################################################
package nlp::timex;

use strict;
use utf8;
use open qw(:std :utf8);

use nlp::gazetteer::temporal;
use Lingua::EN::Sentence qw( get_sentences add_acronyms );
use Data::Dumper;

sub new {
        my $class = shift;
        my %defaults =  ('corpus' => './',
			 'lower' => 850, 
			 'upper' => 1800);

        my $self = {%defaults, @_};
	$self->{'tempex'} = nlp::gazetteer::temporal->new('lower' => $self->{'lower'},
                                               'upper' => $self->{'upper'});

	$self->{'cache'} = ();
        bless $self, $class;
        return $self;
}

####################################################################################
sub extract_temporal_references {
	
	my $self = shift;
        my $fragment = shift;
	my $citations = shift;

	my %combined = ();
	my @return = ();

	foreach my $doc ( keys %{$citations} ) {
                next if exists $self->{cache}{$doc};
                my $text = "";
                unless (open(FILE, "$self->{'corpus'}/$doc")) { die "Couldn't open global $ARGV[0]/$doc" }
                while (<FILE>) { $text .= $_; }
                close(FILE);
                my $sentences=get_sentences($text);
                foreach my $sentence (@$sentences) {
                        $sentence =~ s/\s\D$//g;
                        my ($tagged, $years) = $self->{'tempex'}->tagger("$sentence");
                        my (@signatures) = $tagged =~ /(<TIME.*?<\/TIME>)/g;
                        foreach my $year ( keys % { $years } ) {
                                $self->{'cache'}{$doc}{$year} += $years->{$year};
                        }
                }
        }

	foreach my $doc (keys %{$citations} ) {
                my $max = 0;
                foreach my $year ( sort { $self->{cache}{$doc}{$b} <=> $self->{cache}{$doc}->{$a} } keys %{ $self->{cache}{$doc} } ) {
                #         if  ($max == 0) { $max = $cache{$doc}{$year}; }
                 #        $combined{$year} += $cache{$doc}{$year} / $max;
			  $combined{$year} += $self->{cache}{$doc}{$year};
                }
        }

	foreach my $year (keys %combined) {
		 push @return, {'year' => $year,
                                 'value' => $combined{$year}};

	}

        return @return;
}
#############################################################################################
1;
