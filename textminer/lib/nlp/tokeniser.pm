####################################################################################
##Author:       Chris Stokoe
##File:         tokeniser.pm 
##Date:         28/02/13
##Version:      1.0
####################################################################################
package nlp::tokeniser;
use utf8;

use File::Spec;
use encoding 'utf8';
use Text::Unidecode;

sub new {
        my $class = shift;
        my ($corpus, $general, $placenames, $authors) = "";
	my %defaults =  (stopword => '1',
			 corpus => $corpus, 
			 general => $general,
			 authors => $authors,
			 placenames => $placenames,
			 minimum_length => 2,
			 );
	
	my $self = {%defaults, @_};
	if ($self->{stopword} == 1) {
		if ($self->{general} ne '') { 
			unless (open(STOPWORDS, $self->{general})) { die "Cant find stopwords file $self->{general}\n" };
   			while(chomp($stop = <STOPWORDS>)) { $self->{stopwords}{$stop} = 1; } ;
   			close(STOPWORDS);
		}
		if ($self->{corpus} ne '') {
			unless (open(STOPWORDS, $self->{corpus})) 
				{ die "Can't find stopwords file $self->{corpus}\n" };
                        while(chomp($stop = <STOPWORDS>)) { $self->{stopwords}{$stop} = 1; } ;
                        close(STOPWORDS);      
		}
		if ($self->{placenames} ne '') {
			unless (open(STOPWORDS, $self->{placenames}))
                                { die "Can't find stopwords file $self->{placenames}\n" };
                        while(chomp($stop = <STOPWORDS>)) { $self->{stopwords}{$stop} = 1; } ;
                        close(STOPWORDS);
		}
		if ($self->{authors} ne '') {
                        unless (open(STOPWORDS, $self->{authors}))
                                { die "Can't find stopwords file $self->{authors}\n" };
                        while(chomp($stop = <STOPWORDS>)) { $self->{stopwords}{$stop} = 1; } ;
                        close(STOPWORDS);
                }
	
	}
        bless $self, $class;
        return $self;
}
####################################################################################
sub terms {
        my $self = shift;	
        my $text = shift;
	my @tokens;
       	$text = $self->clean(lc($text));
	$text = lc($text);
        $text =~ s/[^A-Za-z\-\']/ /g;
        $text =~ s/\s-/ /g;
        $text =~ s/\s+/ /g;
        $text =~ s/^\s//g;
        $text =~ s/\s$//g;
	
	if ($self->{stopword} = 1) {
		foreach $candidate (split(/\s/, $text)) {
			next if (exists $self->{stopwords}{$candidate});
			$candidate =~ s/^\'//g;
                        $candidate =~ s/\'$//g;
			next if (length($candidate) <= $self->{'minimum_length'});
			push (@tokens, $candidate);
		}
	} else { @tokens = split(/\s/, $text); }
	#$self->{tokens} = \@tokens;
        return @tokens;
}
#####################################################################################
sub stopword {
	my $self = shift;
	my $candidate = lc(shift);

	if (length($candidate) <= $self->{'minimum_length'}) { return 1; }
	if (exists $self->{stopwords}{$candidate}) { return 1; }
	return;
}
#####################################################################################
#sub transliterate {
#
#	my $self = shift;
#	my $text = shift;
#
#	$text =~ s/—/-/g;
#        $text =~ s/\‘/\'/g;
#        $text =~ s/\`/'/g;
#        $text =~ s/\ʿ/'/g;
#        $text =~ s/\' / /g;
#        $text =~ s/\’/'/g;
#        $text =~ s/\ʾ/'/g;
#        $text =~ s/ù/u/g;
#        $text =~ s/Ù/U/g;
#        $text =~ s/ū/u/g;
#        $text =~ s/ü/u/g;
#        $text =~ s/Ó/O/g;
#        $text =~ s/ó/o/g;
#        $text =~ s/ë/e/g;
#        $text =~ s/ê/e/g;
#        $text =~ s/ī/i/g;
#        $text =~ s/ı/i/g;
#        $text =~ s/ï/i/g;
#        $text =~ s/î/i/g;
#        $text =~ s/ì/i/g;
#        $text =~ s/à/a/g;
#        $text =~ s/â/a/g;
#        $text =~ s/ā/a/g;
#        $text =~ s/ä/a/g;
#        $text =~ s/Ḏ/D/g;
#        $text =~ s/é/e/g;
#	 $text =~ s/é/e/g;
#        $text =~ s/š/s/g;
#        $text =~ s/ṣ/s/g;
#        $text =~ s/†/t/g;
#        $text =~ s/ß/B/g;
#        $text =~ s/∂/d/g;
#        $text =~ s/˙/'/g;
#        $text =~ s/ˆ//g;
#        $text =~ s/ˇ/'/g;
#        $text =~ s/º/'/g;
#        $text =~ s/ö/o/g;
#        $text =~ s/ˇ/'/g;
#        $text =~ s/ṭ/t/g;
#        $text =~ s/ª/'/g;
#        $text =~ s/À/A/g;
#        $text =~ s/Ḥ/H/g;
#        $text =~ s/Ṣ/s/g;
#        $text =~ s/ḥ/h/g;
#        $text =~ s/־/-/g;
#        $text =~ s/ḍ/d/g;
#        $text =~ s/É/E/g;
#        $text =~ s/Ì/I/g;
#        $text =~ s/k̠/k/g;
#        $text =~ s/š/s/g;
#       $text =~ s/al-\n(\w+?)/al-$1/g;
#        $text =~ s/-+?/-/g;
#        $text =~ s/al-\n/al-/g;
#       $text =~ s/\n/ /g;
#        $text =~ s/-\n//g;
#        $text =~ s/-(\s+?)\n//g;
#        $text =~ s/ al / al-/ig;
#	$text =~ s/-\n//g;
#        $text =~ s/-(\s+?)\n//g;
#        $text =~ s/ al / al-/ig;
#        $text =~ s/ al-(\s+?)/ al-/ig;
#        $text =~ s/(\w)\'s\s/$1 /g;
#	$text =~ s/(\d)([A-Za-z]+?)/$1 $2/g;
#        $text =~ s/([A-Za-z]+?)(\d)/$1 $2/g;
#	$text =~ s/Nehorai/Nahray/gi;
#        $text =~ s/Nahrai/Nahray/gi;
#        $text =~ s/Moshe/Moses/gi;
#        $text =~ s/\bmaimon\b/Maimonides/gi;
 #       $text =~ s/\bmaimuni\b/Maimonides/gi;
##        $text =~ s/Yusuf/Joseph/gi;
#        $text =~ s/Ya'qub/Jacob/gi;
#        $text =~ s/Yehudah/Judah/gi;
#        $text =~ s/Yehuda/Judah/gi;
#        $text =~ s/Jubara/Jabbara/gi;
 #       $text =~ s/(^|\s)wuhsha/$1al-Wuhsha/ig;
#        $text =~ s/Yishu Ben Yiju/Ben Yiju/ig;
#        $text =~ s/Yishu/Yiju/gi;
#        $text =~ s/Hasan-Japheth/Hasan/gi;
#        $text =~ s/ Japheth / Hasan /gi;
#	return $text;
#}
#####################################################################################
sub clean {
	my $self = shift;
	my $text = shift;

#	$text =~ s/\sAbi/ Abu/g;
	$text =~ s/Sitt /, Sitt /g;
	$text =~ s/—/-/g;
	$text =~ s/\‘/\'/g;
	$text =~ s/\`/'/g;
	$text =~ s/\ʿ/'/g;
	$text =~ s/\’/'/g;
	$text =~ s/\ʾ/'/g;
	$text =~ s/ˇ/'/g;
        $text =~ s/º/'/g;
	$text =~ s/˙/'/g;
	$text =~ s/ˆ//g;
	$text =~ s/\' / /g;
	$text =~ s/ù/u/g;
	$text =~ s/Ù/U/g;
	$text =~ s/ū/u/g;
	$text =~ s/ü/u/g;
	$text =~ s/Ó/O/g;
	$text =~ s/ó/o/g;
	$text =~ s/ë/e/g;
	$text =~ s/ê/e/g;
	$text =~ s/ī/i/g;
	$text =~ s/ı/i/g;
	$text =~ s/ï/i/g;
	$text =~ s/î/i/g;
	$text =~ s/ì/i/g;
	$text =~ s/à/a/g;
	$text =~ s/á/a/g;
	$text =~ s/â/a/g;
	$text =~ s/ā/a/g;
        $text =~ s/ä/a/g;
	$text =~ s/Ḏ/D/g;
	$text =~ s/é/e/g;
	$text =~ s/š/s/g;
	$text =~ s/ṣ/s/g;
	$text =~ s/†/t/g;
	$text =~ s/ß/B/g;
	$text =~ s/∂/d/g;
	$text =~ s/ˆ//g;
	$text =~ s/ö/o/g;
	$text =~ s/ṭ/t/g;
	$text =~ s/À/A/g;
	$text =~ s/Ḥ/H/g;
	$text =~ s/Ṣ/s/g;
	$text =~ s/ḥ/h/g;
	$text =~ s/־/-/g;
	$text =~ s/ḍ/d/g;
	$text =~ s/É/E/g;
	$text =~ s/Ì/I/g;
	$text =~ s/k̠/k/g;
	$text =~ s/ḵ/k/g;
	$text =~ s/š/s/g;
	$text =~ s/[\[\]]//g;
	$text =~ s/[\(\)]//g;
	$text =~ s/-+?/-/g;
	$text =~ s/al-\n/al-/g;
	$text =~ s/-\n//g;
	$text =~ s/-(\s+?)//g;
	$text =~ s/ al / al-/ig;
	$text =~ s/ al-(\s+?)/ al-/ig;
	$text =~ s/[^A-Za-z0-9\-\'\.\,\;\:\\\/]/ /g;
	$text =~ s/\s-/ /g;
	$text =~ s/\s([A-Za-z])\./ $1€ /g;
	$text =~ s/\./ \. /g;
	$text =~ s/\,/ \, /g;
	$text =~ s/\:/ \: /g;
	$text =~ s/\;/ \; /g;
	$text =~ s/\s([A-Za-z])€/ $1. /g;
	$text =~ s/(\w)\'s\s/$1 /g;
	$text =~ s/ +/ /g;
	$text =~ s/^ //g;
	$text =~ s/ $//g;
	$text =~ s/ -//g;
	$text =~ s/\n$//g;
	$text =~ s/(\d)([A-Za-z]+?)/$1 $2/g;
	$text =~ s/([A-Za-z]+?)(\d)/$1 $2/g;
	$text =~ s/Nehorai/Nahray/gi;
	$text =~ s/Nahrai/Nahray/gi;
	$text =~ s/Moshe/Moses/gi;
	$text =~ s/Yusuf/Joseph/gi;
	$text =~ s/Ya'qub/Jacob/gi;
	$text =~ s/Yehudah/Judah/gi;
	$text =~ s/Yehuda/Judah/gi;
	$text =~ s/(Jubara|Jabarah)/Jabbara/gi;
	$text =~ s/al-dallala//gi;
	$text =~ s/(^|\s)wuhsha/$1al-Wuhsha al-dallala/ig;
	$text =~ s/Yishu Ben Yiju/Ben Yiju/ig;
	$text =~ s/Yishu/Yiju/gi;
	$text =~ s/Hasan-Japheth/Hasan/gi;
	$text =~ s/ Japheth / Hasan /gi;
	$text = unidecode($text);
        $text =~ s/[^[:ascii:]]//mg;
		
	return $text;
}
1;
