####################################################################################
##Author:       Chris Stokoe
##File:         stemmer.pm 
##Date:         28/02/13
##Version:      1.0
####################################################################################
package nlp::gazetteer::religions;

use strict;
use warnings;

use base qw(Exporter);

our @EXPORT_OK = qw( %religions );
our %religions = map { $_ => 1 } split(/\n/, "church of christ
episcopal
anglican
baptist
catholic
huguenot
jewish
jews
jew
judaism
karaite
sephardi
sephardic
lutheran
mennonite
methodist
assembly of god
presbyterian
quaker
buddhism
mythology
bioethics
hinduism
muslim
islam
muslims
islamic
native spirituality
christianity
hinduism
nonreligious
agnostic
atheist
buddhism
chinese traditional religion
primal-indigenous
sikhism
yoruba religion
juche
spiritism
jainism
shinto
cao dai
tenrikyo
neo-paganism
unitarian-universalism
scientology
rastafarianism
zoroastrianism");
1;
