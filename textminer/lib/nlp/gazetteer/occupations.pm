####################################################################################
##Author:       Chris Stokoe
##File:         stemmer.pm 
##Date:         28/02/13
##Version:      1.0
####################################################################################
package nlp::gazetteer::occupations;

use strict;
use warnings;

use base qw(Exporter);

our @EXPORT_OK = qw( %occupations );

our %occupations = map { $_ => 1 } split(/\n/, "attar
gaon
nagid
eseq
aadvisor
abbess
abbot
abecedarian
able seaman
able bodied seaman
absolver
academic
academic administrator
acater
accipitary
accipitrary
accompanist
accomptant
accoucheur
accoucheus
accoucheuse
accountant
accounting
accoutre
accoutrement maker
ackerman
acolyte
acreman
acrobat
actor
actress
actuary
acupressurist
acupuncturist
adjudicator
adjutant
adjutant general
administrative official
administrator
admiral
admiralty
adventurer archaeologist
advertisement conveyancer
advertiser
advertising
advocate
advocate depute
advowee
aerialist
aeronaut
aeronautical engineer
aerospace engineer
aesthetician
affeeror
agent
aggressor
agister
agricultural
agricultural and food scientist
agricultural laborer
agricultural labourer
agriculture
agriculture secretary
agriculturist
agronomist
aide
aide de camp
air hostess
air traffic controller
aircraftman
aircraftsman
aircrewman
airman
airwoman
alabasterer
alblastere
alchemist
alderman
aldermen
ale conner
ale draper
ale founder
ale taster
ale tunner
ale conner
alewife
algebraist
alienist
all spice
almanac man
almoner
almsman
alnager
altar boy
amah
amanuensis
ambassador
ambassadress
amber cutter
ambler
amen man
amir
anaesthetist
analyst
analysts
anarchist
anatomist
anchor smith
anchoress
anchorite
anchorman
anesthesiologist
angle iron smith
angler
anilepman
animal stuffer
animal trainer
animator
ankle beater
annatto maker
announcer
anthologist
anthropologist
antigropelos maker
antiquarian
anvil smith
apiarian
apiariana
apiarist
apostle
apothecary
apotheosis
apparitor
appointee
appraiser
apprentice
aproneer
apronman
aquarius
aquavita seller
arbalestier
arbiter
arbitrator
arborist
arcanist
archaeologist
archbishop
archdeacon
archduchess
archduke
archeologist
archer
archiator
archil maker
architect
architectural critic
architectural historian
archivist
archpriest
argolet
aristocrat
arithmetician
arkwright
armiger
armor bearer
armorer
armourer
army officer
army reservist
army scout
arpenteur
arranger
arrowsmith
art critic
art deco designer 
art director
art editor
art handler
art historian
art therapist
artificer
artilleryman
artisan
artist
artists model
ashman
asphalter
assassin
assay master
assayer
assembler
assemblyman
assessor
assistant
assistant foreman
assistants
associate
associate professor
astrobiologist
astrologer
astronaut
astronomer
astrophysicist
athlete
athletic trainer
attendant
attendent
attorney
attorney at law
attorney general
auctioneer
audiologist
auditor
auger maker
aulnager
aurifaber
aurifex
author
auto mechanic
automobile salesman
automotive mechanic
autopsy surgeon
auxiliary
avenator
aviation pioneer
aviator
aviatress
aviatrix
avowry
axel tree maker
axel tree turner
ayah
ayatollah
baas
babysitter
bacchant
bacchante
bachelor
bachelor at arms
back washer
back us boy
backbencher
background artist
backmaker
backmann
backster
bacteriologist
baddi
badger
badgy fiddler
bagage carrier
baggageman
bagger
bagman
bagniokeeper
bailie
bailiff
baillee
baillie
bairman
baker
bal maiden
balancemaker
balancer
baler
balister
ballad monger
ballard master
ballast heaver
baller
baller up
balloonist
band filer
bandit
bandito
bandster
bang beggar
bank commissioner
bank robber
bank teller
banker
banks man
banksman
banneret
banqueter
barber
barber surgeon
barber chirurgeon
bard
bareman
barge mate
bargee
bargeman
barilla manufacturer
barista
barkeep
barkeeper
barker
barkman
barm brewer
barmaid
barman
barnstormer
baron
baroness
baronet
barrel filer
barrel maker
barrister
bart
bartender
bartoner
baseball coach
baseball player
basil worker
basket maker
basketball coach
basketmaker
basketman
basketweaver
bass
bassist
bassoonist
bast dresser
bath attendent
bather
bathing machine proprietor
batman
batt maker
batter out
batting coach
battledore maker
bauer
bawd
baxter
bayweaver
bead piercer
beader
beadle
beadman
beadsman
beadworker
beamer
beamster
bear ward
bearer
bearleader
beater
beautician
beauty queen
beauty therapist
beaver
beck
becker
bedder
bedel
bedell
bedesman
bedman
bedral
bedweaver
bedwevere
beekeeper
beer seller
beerbrewer
beeskepmaker
beetler
beggar
beguine
belhoste
bell captain
bell founder
bell hanger
bell person
bell ringer
bellboy
belleyetere
bellfounder
bellhop
bellmaker
bellman
bellowfarmer
bellows maker
bellpeople
bellwether
belly builder
belter
bender
berner
besom maker
besswarden
best man
bever
bey
bibliographer
bibliothecary
bibliotherapist
bid stand
bidder
biddy
bill poster
bill sticker
billier
billiter
billyman
binder
biochemist
bioengineer
biographer
biologist
biomedical engineer
biomedical scientist
biometrician
bird boy
bird catcher
birds nest seller
birlyman
bishop
black borderer
black tray maker
blacking maker
blackleg
blacksmith
blacksmith armorer
bladesmith
blaster
blaxter
bleacher
blemmere
blentonist
blindsman
block maker
block printer
blockcutter
blocker
bloodletter
bloodman
bloomer
blower
blowfeeder
blue collar worker
bluejacket
bluestocking
bluffer
bosun
boarding officer
boardman
boardwright
boatbuilder
boater
boatman
boatswain
boatwright
bobber
bobbin carrier
bobbin turner
bobby
bocher
bodeys
bodeys maker
bodger
body maker
body servant
bodybuilder
bodyguard
bodyservant
boiler plater
boilermaker
boll
bolter
bombardier
bondager
bondman
bondsman
bone button turner
bone lace maker
bone mould turner
bone picker
bonecarver
bonesetter
boniface
book agent
book gilder
book guilder
book keeper
bookbinder
bookholder
bookkeeper
bookkeeping clerk
bookman
bookprinter
bookseller
boonmaster
boot catcher
boot closer
boot catcher
bootbinder
boothaler
boothman
bootlegger
borer
borers
borler
borlera
borsholder
boss
bosun
botanist
botanists
botcher
bottelier
bottiler
bottle boy
bottler
bottom knocker
bottom maker
bottomer
bouncer
bounty hunter
bouquinist
bowdler
bower
bowker
bowler
bowlman
bowlminder
bowlwoman
bowman
bowyer
boxer
bozzler
brabener
brachygrapher
braider
brailler
brakeman
brakesman
brasiator
brasiler
brass cutter
brass finisher
brass founder
brass hat
brayer
brazier
breach maker
bread maker
breader
breadwinner
breaker
breechesmaker
brewer
brewmaster
brewster
brickburner
bricker
bricklayer
brickmaker
brickman
bridesmaid
bridewell keeper
bridgeman
brigadier
brigadier general
brightsmith
broad cooper
broadcaster
broadcloth weaver
broderer
brogger
broiderer
broker
bronco buster
bronzefounder
broom dasher
broom squire
broom dasher
broommaker
brotherer
brow girl
browderer
brownsmith
brushbinder
brushmaker
buck private
buck washer
buckaroo
buckle maker
buckle tongue maker
buckler
bucklesmith
buckram maker
buddleboy
buddler
buffalo soldier
buffoon
builder
bullocky
bullwhacker
bumboat man
bummaree
bummer
bunny girl
bunter
bureaucrat|bureaucrats
burgess
burglar
burgomaster
burgrave
burler
burlesque performer
burmaiden
burneman
burye man|buryeman
busboy
bushel maker
busheler
bushwhacker
business analyst
business editor
business executive
business man|businessman
business person|businessperson
business owner
business women|businesswoman
busker
buss maker
butcher
butler
butner
butter carver
button burnisher
buttonmaker
butty
buyer
ceo
cfo
cab driver
cabbie
cabin boy
cabinet member
cabinet minister
cabinet officer
cabinet maker
cabinetmaker
cad
caddie
caddies
caddy
caddy butcher
cadet
cadger
caesar
cafender
caffler
cainer
caird
calciner
calender
calender worker
calenderer
calenderman
calico printer
call girl
calligrapher
calligraphist
cambist
cambric maker
cameraman
camerist
camister
camlet merchant
camp cook
camp counsellor
camp counselor
camp follower
campaigner
campaner
canadian mountie
canaller
cancellarius
candidate
candle maker
candler
candy man
candy striper
caner
cannaller
cannon fodder
cannoneer
cannonsmith
canoeist
canon
canter
canting caller
cantor
canvaser
canvasser
cap maker
cape merchant
caper
capillaire maker
capitalist
capper
capt.
captain
captain of the guard
car designer
car mechanic
carabineer
carabinier
card nailer
carder
cardinal
cardiologist
cardmaker
cardroomer
caretaker
carhop
carman
carner
carnifex
carpentarius
carpenter
carpenter joiner
carpet knight
carriage driver
carrier
carry in boy
cart wheeler
carter
cartier
cartographer
cartomancer
cartoonist
cartwright
carver
caseworker
cashier
cashmarie
caster
castor
castora
castrator
cats paw
catagman
cataloger
catcher
catchpole
catchpolla
catechista
cathar perfect
catholic deacon
cattle baron
cattle jobber
cattle rustler
cattleman
caulker
causticiser
cavalier
cavalry officer
cavalryman
ccountant
ceapman
ceiler
celebrant
cellarer
cellarman
cellist
cemmer
censor
census taker
ceo
ceos
ceramicist
ceramist
certified public accountant
chaff cutter
chafferer
chainmaker
chair
chair bodger
chair bottomer
chairman
chairman of the board
chairperson
chairwoman
chaise maker
chaloner
chamber master
chamberlain
chambermaid
chambermaster
champion
chancellor
chancellor of the exchequer
chandler
chantry priest
chanty man
chapeler
chaplain
chapman
char
charcoal burner
charcoalburner
charge d'affaires
charge of quarters
charioteer
charlatan
charman
chartmaster
charwoman
chaser
chatelaine
chauffeur
chaunter
check girl
checker
cheerleader
cheese factor
cheese maker
cheese monger
cheese maker
cheesemaker
cheeseman
chef
chemical engineer
chemical technologist
chemist
cherub
chess player
chevalier
chicken butcher
chicken sexer
chief
chief assistant
chief engineer
chief executive
chief executive officer
chief financial officer
chief information officer
chief inspector
chief justice
chief mate
chief of police
chief of staff
chief of state
chief operating officer
chief petty officer
chief scientist
chief secretary
chieftain
chiffonier
warehouse keeper
childcare worker
chiliarch
chimney sweep
chimney sweeper
chimneysweep
chimneysweeper
chinese launderer
chinglor
chip
chippers labourer
chiropodist
chiropractor
chirugion or chirurgeon
chirurgeon
choragus
choreographer
choreographers
chorepiscopi
chowder
chronicler
chronologist
church historian
church officer
church usher
churchman
churchwarden
cia agent
cicerone
cinder wench
cinematographer
circuit judge
circuit preacher
circus performer
city editor
city father
city manager
civic leader
civil engineer
civil leader
civil servant
claim jumper
claker
clapman
clarinetist
clark
classicist
classman
claviger
clay carrier
clayman
cleaner
cleaning lady
cleaning woman
clergy
clergyman
clergymen
cleric
clericus
clerk
clerk of the works
clerks
clerks of the work
cleyman
clicker
client
climber
clipper
clipper off
clipper on
clock maker
clockmaker
clocksmith
clod hopper
clod hopper
clogger
cloth lapper
cloth linter
cloth picker
clothesman
clothier
clothman
clouter
clower
clown
coach
coach driver
coachbuilder
coachmaker
coachman
coal backer
coal burner
coal drawer
coal heaver
coal miner
coal runner
coal whipper
coalman
coalmeter
coaly
coast guard
coast surveyor
coastguardsman
cobbler
cobleman
cockfeeder
cocus
cod placer
codman
cofferer
cogmen
cognitive scientist
cohen
coiffeur
coiffeuse
coillor
coiner
coistsell
collar maker
collectivist
collector
collector of internal revenue
college co ed
college president
collier
colonel
colonol
colonus
color bearer
color guard
color sergeant
coloratora
colorist
colour man
colourator
colporteur
columnist
comb maker
comber or combere
combmaker
comedian
comedians
comedienne
comediennes
commandant
commander
commander in chief
commanding officer
commando
commerce secretary
commercial traveler
commercial traveller
commie
commissar
commission merchant
commissioned officer
commissioner
commodor
commodore
communist
companion
company man
company secretary
compasssmith
compiler
composer
compositor
comptroller
comptroller general
comptroller of the currency
computer engineer
computer hardware engineer
computer programmer
computer scientist
computer software engineer
computer support specialist
comrade
comte de saxe
con artist
con man
concierge
concreter
conder
conditioner
conductor
coney catcher
confectioner
confectionery
confederate soldier
conferencier
confessor
congressman
congresswoman
conman
conner
connoisseur
connor
conscript
conservation scientist
conservator
constable
construction engineer
construction manager
construction worker
consul
consulat
consultant
consultant paediatrician
consulting
conte
contract killer
contract manager
contracting
contractor
contrator
controller
convener
cook
cookie
cooky
cooper
cooper or cuper
coordinator
copeman
coper
copilot
copperbeater
copperbeter
coppersmith
copy editor
copyist
copyreader
copywriter
coracle maker
corder
cordiner
cordon bleu
cordwainer
cork cutter
corn cutter
cornhusker
coroner
corporal
corporate executive
corporate executive officer
corporate financier
correctional officer
corrector
correspondent
corsetier
corviner
corvisor
cosmetician
cosmetologist
cosmographer
cosmologist
cosmologists
cosmonaut
cost estimator
coster wife
costermonger
costume designer
cotiler
cottier
cotyler
councilman
counsel
counsellor
counselor
count
counterman
countess
couper
couranteer
coureur de bois
courier
court jester
court reporter
court wizard
courtesan
courtier
cover girl
cowboy
cowgirl
cowhand
cowherd
cowkeeper
cowman
cowper
cowpoke
cowpuncher
cox
coxswain
coxwain
cpa
cracker boy
crackerjack
craftiman
craftsman
craftsmen
craftswoman
cramer
craneman
cratch maker
crate man
creative engineering
crew member
crewman
crewmen
cricketer
crier
criminal
criminalist
criminologist
criminologists
crimper
crimpet maker
critic
crocker
crocodile hunter
crofter
crookmaker
cropper
crosier
crossbencher
crossbowman
crossbowyer
crown prince
crown princess
crowner
crozier
cryptographer
cryptozoologist
crystallographer
cto
cuhreur 
cunreur
cuirassier
cupbearer
curate
curator
curer
curretter
currier
custodian
customer
customs officer
cutler
cutpurse
cutter
cyberathlete
cybernetician
cytotechnologist
cytotechnologists
czarina
czaritza
daguerreotype artist
dairymaid
dairyman
dallal
damster
dancer
dancers
danter
dapifer
dastur
database administrator
dateler
dauphin
day laborer
day labourer
day man
daytaleman
dayyan
dba
deacon
deaconess
deacons
dealer
dean
dean 
deathsman
decimer
decision maker
deckhand
decorator
decoyman
decretist
deemer
deemster
deep cover agent
defense minister
defense secretary
deitiy
deity
delegat
delegate
delineator
deliverer
delivery boy
delivery man
delivery men
delivery woman
deliveryman
delver
demigod
democrat
demographer
demolitionist
demonstrator
dempster
demster
dental assistant
dental hygienist
dental technician
dentist
dentists
denturist
department head
depater
deputie
deputy
deputy assistant defense secretary
deputy foreign minister
dermatologist
derrickman
dervish
designer
desk clerk
desk officer
desktop publisher
detective
determined homesteader
developer
devil
devil dog
deviller
dexter
dey wife
diamantaire
diarist
dicator
dictator
diemaker
diesinker
dietician
dietitian
digger
dikeman
diker
dilettante
dining room attendant
diocesan
diplomat
diplomatic minister
diplomatist
dipper
director
director of communications
director of finance
director of marketing
director of public affairs
director of research
directors
disc jockey
dish thrower
dish turner
disher
dishwasher
disinfestation officer
dispatcher
distiller
distributor
district manager
ditch digger
ditcher
diver
diviner
docent
docents 
dock labourer
dock master
dock walloper
dock worker
dock walloper
docker
dockhand
doctor
doctor of chiropractic
doctor of medicine
doctor of physical therapy
documentalist
dog breaker
dog breeder
dog catcher
dog leech
dog trainer
dog walker
dog whipper
dogcatcher
doge
dogsbody
dollmaker
domainer
domesman
domestic
domestic help
domestic worker
dominatrix
domine
dominee
dominie
dominotier
dominus
donkey boy
donkey man
door keeper
door keeper
doorkeeper
doorman
dosimetrist
dosimetrists
doubler
dowser
dozener
dr
draftee
drafter
draftsman
draftsmen
draftsperson
dragman
dragoman
dragoon
dragsman
drainer
dramatist
dramaturg
draper
draughtsman
drawboy
drawer
drayman
dredgerman
dresser
dressing machine maker
dressmaker
dressmaker's model
drift makera
drill instructor
drill master
driller
dripping man
driver
drover
drudge
drug dealer
drugger
druid
drummer
dry salter
dry stone waller
dry stane dyker
drycooper
drysalter
drywaller
dubbere
duchess
dude ranch cowboy
duffer
duke
dung carter
dustbin man
dustman
dvm
dyer
dykeman
dyker
ealdorman
earer
earl
earner
earth scientist
earth stopper
ebonite turner
ecclesiastic
ecologist
ecologists
economist
editor
education secretary
educationalist
educator
egg factor
eggler
egyptologist
elder
elder statesman
elected official
electrical engineer
electrician
electronic engineer
elephants teeth dealer
elevator mechanic
ellerman
elliman
elymaker
embalmer
embassador
embedded reporter
embosser
embroiderer
embryologist
emergency medical technician
emperor
employee
employer
empresario
empress
emptor
emt
enameler
endholdernn
enforcer
engine driver
engine tenter
engineer
engineering manager
engineering technician
engineman
engraver
enlisted man
enlisted person
enlisted woman
enologist
ensign
entertainer
entomologist
entrepreneur
entymologist
entymologists
enumerator
environmental designer
environmental scientist
envoy
envoy extraordinary
epigrapher
epistemologist
equerry
erector
eremite
ergonomist
erite
ervad
escort
esquire
essence peddler
estafette
estate agent
esthetician
estimator
etcher
ethicist
ethnohistorian
ethnologist
ethologist
etymologist
evangelist
event planner
ewe herd
ex mayor
ex president
ex serviceman
examiner
excavator
exchequer
exciseman
executioner
executive
executive director
executive officer
executive secretary
executive vice president
executive vp
executor
exobiologist
exotic dancer
explorer
exporter
expressman
exterminator
extra
eyer
faber
fabricator
fabricshearer
facilitator
factor
factory worker
factotum
fagetter
faker
fakhkhar
fakhurani
falconer
famulus
fancy man
fancy woman
fancy pearl worker
fanner
fanwright
faqih
farandman
farm worker
farmer
farmerette
farmhand
farming
farrier
fashion arbiter
fashion designer
fashion model
fashioner
favorite son
favourite son
fawkner
fayj
fbi agent
fbi special agent
fear nothing maker
feather beater
feather driver
feather beater
feather dresser
feather wife
featherman
federal agent
federal official
federalist
feeder
fell monger
fellah
feller
fellmonger
felter
feltmaker
female aristocrat
female monarch
fence
fence viewer
feroner
ferrer
ferreter
ferrour
ferryman
fettler
feudatory
fever
fewster
fewterer
fewtrer
fiddler
field marshal
field officer
field grade officer
fieldhand
fighter
fighter pilot
file clerk
file cutter
filer
filibuster
filibusterer
filing clerk
fille de chambre
filler
film director
film producer
finance
finance director
finance minister
financial adviser
financial analyst
financial manager
financial planner
financier
fine drawer
finisher
fire fighter
fire marshal
fire marshall
fire officer
fire safety officer
fire warden
firebeater
firefighter
fireman
firemen
first hand
first lieutenant
first mate
first sergeant
fiscere
fish fag
fisher
fisherman
fishermen
fishmonger
fishwife
fitter
flag captain
flag officer
flamen
flasher
flatman
flauner
flautist
flavorist
flax dresser
flaxdresser
fleet admiral
flesher
fleshewer
fleshmonger
fletcher
flight attendant
flight engineer
flight instructor
floater
floatman
floor leader
floor manager
floorwalker
florist
fluffer
flunkey
flunky
flusherman
flutist
flycoachman
flying stationer
flyman
fogger
fogler
foister
foisterer
food critic
fool
foot maiden
foot man
foot pad
foot soldier
foot straightener
foot boy
foot maiden
foot post
football coach
footballer
footman
footpad
footslogger
foreign minister
foreign official
foreman
foremen
forensic pathologist
forensic scientist
forest fire fighter
forestaller
forester
forewoman
forgeman
forger
former film star
fortune teller
fortune teller
forty niner
forty niner
fossetmaker
founder
foundry man
foundry woman
foundryman
foundrywoman
fower
fowler
frame spinner
framer
frameworker knitter
fraudster
freedman
freelance
freemason
freibauer
fresco painter
freser
fresh fish
freshwater man
friar
fringemaker
fripperer
friseur
frobisher
frontbencher
fruiterer
fruitestere
fruitier
fry cook
fueller
fugler
fulker
full admiral
full general
fuller
funambulist
functionary
fund manager
funeral director
furber
furbisher
furbour
furner
furniture maker
furniture tester
furniture worker
furrier
fusilier
fustian weaver
g man
ga'on
gabeler
gaffer
gaffman
gager
gallerist
gallery director
galley slave
galvaniser
gambler
game designer
game show host
game warden
gamekeeper
gamer
gamester
gandy dancer
ganger
gangrel
gangsman
gangster
ganneker
gaoler
garbage collector
garbage man
garcifer
garcio
garcion
gardener
garlekmonger
garment cutter
garthman
gas fitter
gas manager
gasman
gastroenterologist
gate keeper
gater
gatherer
gatherers boy
gatward
gaucho
gauger
gaunter
gaveller
gedol ha yeshiva
gelder
gem cutter
gemcutter
genealogist
general contractor
general manager
general officer
general secretary
generalissimo
gentleman
geochemist
geodesist
geographer
geologist
geomancer
geometer
geomorphologist
geophysicist
gerontologist
gerund grinder
gigolo
gilder
gillie
gimler
ginour
girdler
girl friday
gladiator
glass coachman
glass seller
glassblower
glasscutter
glassewryght
glassmaker
glasspainter
glassworker
glazer
glazier
gleaner
glimmer man
glover
goalie
goalkeeper
goaltender
goat carriage man
goatherd
goatherder
goddess
gofer
gold digger
gold miner
gold panner
goldbeater
goldbrick
goldsmith
goldworker
golf caddie
gondolier
gondoliere
gong farmer
goose herd
goose herder
gorzeman
gospeler
gospeller
governess
government agent
government employee
government minister
governor
governor general
grace wife
grader
graffer
grainer
grammarian
grand duchess
grand duke
grand master
grand mufti
grandee
granger
graphic artist
graphic designer
graphologist
gravedigger
graver
graverobber
grayback
grazier
grease monkey
greave
green grocer
greengrocer
greensmith
grenade thrower
grenadier
grieve
grimbribber
grinder
grocer
grocers
grocery boy
groom
groomsman
groover
groundsel
chickweed seller
groundskeeper
groundsman
group captain
grower
guard
guard of honor
guardian ad litem
guards
guardsman
guide
guides
guild master
guinea pig
guitarist
gummer
gumshoe detective
gun moll
gunner
gunnery sergeant
gunslinger
gunsmith
gunstocker
guru
gutta percha manufacturer
guvnor
gymnasiarch
gymnast
gynecologist
gynour
gyp
haberdasher
hack
hacker
hackler
hackman
hackner
hackney man
hair seating 
hairdresser
hairman
hairstylist
hairweaver
halberdier
halbert carrier
hammerman
hand flowerer
hand woman
handle sticker
handler
handmaid
handmaiden
handseller
handwoman
handyman
hankyman
hansard
harberdasher
harbourmaster
harlot
harmer beck
harness maker
harper
harpist
harpooneer
harpooner
harvester
hatcheck girl
hatcheler
hatchet man
hatchler
hatmaker
hatter
hawker
hay merchant
haymonger
hayward
hazan
hazzan
head of household
head of state
head waiter
headman
headmaster
headmistress
headsman
headswoman
heald knitter
healer
hearing examiner
hearing officer
heck maker
heckler
hedge looker
hedger
heelmaker
hellier
helmsman
helper
helper up
hempheckler
henchman
hensman
henter
herald
herbalist
herder
herdsman
heresiarch
hermit
hero
herpetologist
herpetologists
hetheleder
hewer
hierarch
higger
higgler
high commissioner
high priest
highlander
highwayman
hillard
hiller
hillier
hind
hired gun
hired hand
hired man
hireling
hirer
histologist
historian
historian of science
historiographer
hit man
hobbler
hobo
hockey coach
hod
hod carrier
hodman
hodsman
hoggard
holdover
holster
home secretary
homeowner
homoeopath
honcho
honey dipper
honor guard
hoodoo
hoofer
hooker
hooper
hoplite
horner
horse coper
horse courser
horse knave
horse leech
horse marine
horse rustler
horse trainer
horse wrangler
horse capper
horse hair curler
horseleech
horseshoer
horticultural therapist
hosier
hospital attendant
hospital chaplain
host
hosteler
hosteller
hostess
hostler
hotel clerk
hotel desk clerk
hotel manager
hotelier
hotelkeeper
hotelman
hotpresser
house cleaner
house husband
house joiner
house painter
house servant
house wife
house wright
housebreaker
househusband
housekeeper
housemaid
housemaker
housewife
housewives
housewrecker
housewright
housing commissioner
howdy wife
hoyman
huckster
huissher
human resources assistant
hunter
hunter trapper
hunters
huntress
huntsman
hurdle maker
hurdleman
hurdler
hurriers
husbandman
hush shop keeper
hussar
hydraulic engineer
hydrographer
hydrologist
hygienist
hypnotist
iceman
ichthyologist
icthyologist
idleman
illuminator
illusionist
illustrator
imam
imaum
impersonator
importer
impressionist
incumbent
indexer
indian chief
industrial designer
industrial engineer
industrial painter
industrialist
industrialists
industry and commerce minister
infantryman
infirmarian
information architect
information designer
information scientist
information technologist
initiator
inker
inlayer
innholder
innkeeper
insider
inspector
inspector general
instigator
instructor
instrument maker
insurance agent
insurer
intelligence officer
intelligencer
intendant
intendent
interfactor
interim vice president
interior designer
interior minister
interior secretary
intermediary
intern
internal revenue agent
internationalist
internist
internuncio
interpreter
interrogator
intrepid merchant
inventor
investigator
investment analyst
investment banker
investment broker
investment counsel
investor
investors
invigilator
iron founder
iron master
iron monger
iron smith
ironmaster
ironmonger
ironworker
ivorist
ivory worker
ivory workers
jack
jack frame tenter
jack smith
jack tar
jacksmith
jagger
jailer
jakes farmer
jakhuri
janissary
janitor
japanner
jazz musician
jerquer
jersey comber
jester
jeweler
jewler
jiggerman
jobber
jobholder
jobling gardener
jobmaster
jockey
joiner
jongleur
jorman
journalist
journeyman|journeymen
jouster
joiner|joyner
judge|judges
judge advocate
judge advocate general
juggler
jurist|juror
justice minister
justice of the peace|justices of the peace
justiciar|justiciary
juvenile
kaiser
karate master
kedger
keeker
keeler
keelman
keeper
kempster
kennel attendant
kickboxer
kiddier
kilner
kinesiologist
king
kisser
knacker
knapper
knappers
kneller
knifeman
knifesmith
knight
knight bachelor
knight banneret
knight of the square flag
knight templar
knight errant
knitter
knocker up
knockknobbler
knoller
knuller
labor
labor leader
labor secretary
laborer
laborers
labourer
labourite
lace drawer
lace master
lace runner
lacemaker
laceman
lacewoman
lackey
lacquerer
lady in waiting
ladys maid
lady in waiting
lagger
lagraetman
laity
lama
lamplighter
lampwright
lance corporal
lancer
lancier
land agent
land waiter
landed gentry
landgrave
landlady
landlord
lands jobber
landscape architect
landscaper
landsman
landwaiter
lanternmaker
lapidarie
lapidary
lapidist
lardner
laryngologist
lascar
lasher
laster
latoner
lattener
launderer
laundress
laundresses
laundryman
laundrywoman
lavendar
lavender
law enforcement
law enforcement agent
law reporter
lawgiver
lawmaker
lawyer
layer
leader
leadworker
leather dresser
leatherer
leatherneck
leatherworker
leavelooker
lecturer
leder
ledgeman
leech
leech or sawbones
left winger
leftenant
leftist
legal assistant
legerdemainist
legger
legionary
legionnaire
legislator
lehrer
leightonward
lengthsman
lens maker
lensgrinder
letter carrier
level designer
lexicographe
lexicographer
liaison
librarian
librettist
lictor
lieutenant
lieutenant colonel
lieutenant commander
lieutenant general
lieutenant governor
lieutenant jg
lieutenant junior grade
life peer
lifeguard
lifesaver
light colonel
lighter man
lighterman
lighthouse keeper
lighthouse keeper
lighting technician
limner
line coach
line officer
line worker
lineman
linen armorer
linen draper
linener
linenspinner
liner
linesman
linguist
link boy
link man
linkboy
linkerboy
linkerman
linkman
lion hunter
lister
literary historian
litster
litter bearer
litterman
liturgist
liveryman
livestock breeder
llb
loader
loadsman
loan officer
lobbyist
loblolly boy
lobsterback
lobsterman
local government officials
local historian
local mayor
lock keeper
lockkeeper
lockman
lockmaster
locksmith
loder
lodesman
loftsman
loftswoman
logger
logician
logistician
long song seller
longshoreman
lord chancellor
lord high chancellor
lord mayor
lord privy seal
loresman
lorimer
lotseller
lotter
lum swooper
lumberjack
lumberman
lumbermen
lumper
lungs
lutemaker
lutenist
luthier
lyner
lyricist
macebearer
macer
machine gunner
machine politician
machinist
maderer
magazine editor
magician
magistrat
magistrate
magnate
magus
maharaja
maharajah
maharanee
maharani
mahout
maid
maidservant
mail carrier
mail clerk
mail guard
mail man
mailer
mailmaker
mailman
mailman or mail carrier
maintenance
maintenance man
maintenance men
maise maker
maitre d'hotel
major
major domo
major general
majority leader
make up artist
male aristocrat
malemaker
malender
malster
mammalogist
mammographer
man friday
man of the cloth
man at arms
management
management consultant
manager
manageress
managing director
managing editor
manakin
manciple
mandarin
mangle keeper
mangler
mango
manicure
manicurist
manikin
mannequin
mannikin
manservant
mantuamaker
manual laborer
manufacturer
mapmaker
mapper
marabout
marble worker
marbler
marbreur
marcher
marchioness
margrave
marine
marine biologist
marine engineer
mariner
market gardener
marketer
marler
marleywoman
marquess
marquis
marquise
marshal
marshall
marshman
martial artist
mashmaker
mason
massage therapist
masseur
masseuse
master
master builder
master lumper
master mariner
master of ceremony
master of hounds
master of the revels
master of the rolls
master sergeant
master at arms
matador
matchet forger
material scientist
materials engineer
materials scientist
mathematical physicist
mathematician
matron
mawer
mayor
mayoress
md
meader
mealman
meat butcher
mechanic
mechanic 
mechanical engineer
mechanician
mediator
medic
medical biller
medical scientist
medical social worker
medical technologist
medical transcriptionist
medicine
medicine man
medicine peddler
meistersinger
melder
member of congress
member of parliament
menage man
mender
mercator
mercenarie
mercenary
mercer
merchant
merchant taylor
mesmerist
mesne lord
messenger
metallurgist
metalman
metalworker
metaphysician
meteorologist
meterer
metrologist
metropolitan bishop
microbiologist
midinette
midshipman
midwife
migrant worker
milady
milesman
military chaplain
military engineer
military governor
military historian
military leader
military man
military officer
military personnel
military volunteer
militia
militiaman
milkmaid
milkman
mill girl
mill hand
miller
milleress
millers carman
milliner
millner
millpeck
millwright
milord
milstone inspector
mime
miner
mineralogist
mineworker
miniaturist
minister
minister of finance
minister plenipotentiary
ministers of the crown
ministrant
minnesinger
minority leader
minstrel
minter
mintmaker or mintmaster
mintmaster
minuteman
mirrorer
miss
missionary
mistress
mixer
mixologist
mobed
mocado weaver
model
model maker
modeler
modeller
moderator
mohel
mold boy
molder
moldmaker
molecatcher
molitor
monarch
mondayman
money schrivener
moneychanger
moneyer
moneylender
monger
monitor
monk
monk or nun
monsignor
monthly nurse
mortgage banker
mortgage broker
mortician
mosaicist
moulder
mouldmaker
mountain rescuer
mountaineer
mountebank
mud digger
mudlark
muffin maker
muffin man
mufti
muggler
mugseller
mugwump
mule driver
mule minder
mule skinner
muleskinner
muleteer
mullah
multurer
mummer
municipal engineer
muralist
museologist
music director
music teacher
musician
musicker
musicologist
musiker
musketeer
mustarder
mustardman
mycologist
mystic
nagelschmiedmeister
nagsman
nailmaker
nailora
nakerer
nanny
naperer
napier
narrow weaver
national leader
national security advisor
natural philosopher
naturalist
naval officer
navigator
navvy
navy man
neatherd
necessary woman
necker
necromancer
nedeller
needler
needleworker
negotiator
netmaker
netter
neurologist
neurologists
newsboy
newscaster
newsman
newsmen
newspaper editor
newsperson
newswoman
newswriter
niellist
niellists
night auditor
night magistrate
night soilman
night watchman
nightman
nightwalker
nimgimmer
ninja
nipper
nob thatcher
nob thatcher
noble
nobleman
noblewoman
nominee
non christian priest
noncandidate
noncom
noncombatant
noncommissioned officer
noon tender
notary
notary public
noterer
novelist
novelists
nuclear engineer
numerologist
numismatist
nun
nuncio
nurse
nurse's aide
nursemaid
nursing aide
nutritional therapist
nutritionist
négociant
oarsman
oarswoman
oboist
obstetrician
occupational therapist
occupier
oceanographer
odd job man
odontologist
office assistant
office boy
office bearer
officeholder
officer
officer 
official
officials
oil merchant
oilmaker
oilman
old clothes dealer
olitor
oncologist
online vendor
ontologist
operator
ophthalmologist
optician
optometrist
oracle
oral historian
orderly
orderly sergeant
ordinand
ordinary keeper
ordinary seaman
orfever
organ builder
organist
organization man
organizer
ornithologist
orrery maker
orrice weaver
orthodontist
orthopaedist
osier peeler
ostiary
ostler
ostreger
otorhinolaryngologist
out crier
outlaw
outworker
overlooker
overman
overseer
owler
oyabun
oynter
oyster dredger
oyster raker
oysterer
pack thread spinner
packer
packman
pad maker
paddler
padre
padrone
painter
painter 
paintress
palatine
paleographer
paleontologist
paling man
palister
palmer
palsgrave
pan smith
panhandler
panner
pannier man
pannifex
panter
pantler
pantryman
paper stainer
paperboy
paperer
paperhanger
papermaker
papyrologist
paralegal
paramedic
paraprofessional
parapsychologist
paratrooper
parchmenter
pardoner
parfumier
pargeter
parish priest
paritor
park commissioner
park ranger
parker
parliamentarian
parlormaid
parlourmaid
parole officer
parson
part timer
partner
party boss
party liner
party man
party whip
party leader
passage keeper
pasteler
pastor
pastry cook
pastrycook
patent attorney
patent examiner
paterfamilias
pathologist
patrician
patten maker
pattenmaker
patternmaker
paver
pavior
paviour
pavyler
pawnbroker
peacekeeper
peager
peasant
pedaile
pedascule
peddler
pediatrician
pediatrist
pedologist
peeler
peer
peer of the realm
peeress
pelterer
pencil pusher
pendragon
penpusher
pensionary
peon
perambulator
perchemear
percussionist
peregrinator
performer
perfumer
periwig maker
persona grata
persona non grata
personal trainer
personnel
peruker
perukier
pessoner
peterman
pettifogger
petty chapman
petty officer
pew opener
pewterer
ph.d.
pharaoh
pharmacist
pharmacologist
pharmaopoeist
phd
phebotomist
phenomenologist
philanthropist
philatelist
philologist
philosopher
philosophical instrument maker
phlebotomist
photochemist
photographer
photojournalist
phrenologist
phyisicist
physical therapist
physician|physicians
physician assistant
physicist
physiognomist
physiologist
physiotherapist
pianist
piano tuner
picaroon
picker
pickler
pickpocket
pie seller
piece broker
piecener
piecer
pigmaker
pigman
pikelet maker
pikeman
piker
pilgrim
pill box lidder
piller
pilot
pimp
pin up
pinder
piner
pinkertons agent
pinko
pinmaker
pinner
pinner up
pinsetter
pioneer
pipe fitter
pipelayer
piper
pirate
pirn winder
pissprophet
pistor
pit brow lass
pitcher
pitchers
pitching coach
pitman
placeman
placeseeker
plaicher
plain worker
plaiter
planer
planker
planner
plant engineer
plasher
plasterer
plastic surgeon
platcher
platelayer
plattner
playderer
player
playwright
pleacher
pledge taker
plenipotentiary
plotter
plough jogger
ploughman
ploughwright
plowman
plowright
plowwright
plumassier
plumber
plumbum man
plumbum worker
plumer
poacher
podiatrist
poet
poetician
point man
point woman
pointer
pointmaker
pointman
pointsman
poldave worker
poleman
polentier
poleturner
police
police commissioner
police detective
police inspector
police officer
police women
policeman
policemen
political boss
political commissar
political hack
political leader
political operative
political scientist
politician
politico
poller
ponderator
pontiff
pony express rider
pope
pornstar
portable soup maker
porter
poser
post boy
post rider
postal clerk
poster
postillion
postman
postmaster
postmaster general
postmistress
pot boy
pot mender
potato badger
potboy
pothunter
potman
potter
potter carrier
potter thrower
pouch maker
poulter
powder monkey
power loom tuner
power worker
power station worker
powler
poynter
practitioner
praetor
preacher
preacher man
prebendary
preceptress
prediger
prefect
prelate
premier
premier ministre
prentis
preparator
presbyter
presenter
preserver
president
president of the united states
president elect
presiding officer
press officer
press secretary
pressman
prestidigitator
pretor
prick louise
pricker
priest
priest doctor
priestess
prig napper
primate
prime contractor
prime minister
prince
prince consort
prince bishop
princess
princess royal
principal
printer
prioress
prison chaplain
prison guard
private detective
private eye
private investigator
privateer
privateersman
privycleaner
probation officer
processor
proconsul
proctologist
proctor
procurator
producer
prof
professional
professional athelete
professional athlete
professional dominant
professor
proffessor
progamer
programmer
project architect
project manager
projectionist
prompter
promulgator
proofreader
prop bobby
propagandist
property man
property master
prophet
propman
proprietor
prospector
prostitute
protestant deacon
prothonary
provost
psychiatrist
psychic
psychoanalyst
psychodramatist
psychohistorian
psychologist
public administrator
public officer
public relations officer
public servant
public speaker
publican
publicist
publisher
puddler
pug mill operator
puggard
pugger
pugilist
pulleymaker
pullman porter
pumbum
pumpmaker
puncher
punky
punter
purchasing agent
purefinder
purse maker
purser
pursuivant
purveyor
putter in
putti
qaid
quack
quantity surveyor
quarrel picker
quarrier
quarryman
quartermaster
quartermaster general
queen
queen consort
queen dowager
queen mother
queen regent
queen regnant
quiller
quillworker
quilter
quiltress
quister
qwylwryghte
rav
rabbi
race driver
racer
rack maiden
radio journalist
radio personality
radiographer
radiologist
raff merchant
raffman
rag and bone man
rag cutter
rag gatherer
rag gatherers
rag man
rag picker
ragpicker
rail splitter
railroad baron
railroad brakeman
railroad conductor
railroad engineer
railroad laborer
railroad man
railroad stoker
railroader
railway man
railwayman
raja
rajah
raker
ranch hand
rancher
ranee
ranger
rani
ranker
rat catcher
rat catcher
ratoner
rattlewatch
raw recruit
real estate agent
real estate broker
real estate developer
real estate investor
realtor
reaper
rear admiral
reatailer
receptionist
receptionists
record producer
record keeper
recording engineer
recordist
recreation and fitness worker
recreational therapist
recruit
recruiter
recruiting sergeant
rectifier
rector
redactor
redar
redcap
redcoat
redsmith
reeder
reedmaker
reeler
reeve
reever
referee
refiner
refinisher
refuse collector
regent
regional general manager
registrar
registrar 
regulator
religious ethicist
religious historian
remedial teacher
renderer
renovator
rep
repairer
repairman
repairmen
reporter
republican
rescuer
research assistant
research director
researcher
researchers
reseller
reservist
respiratory therapist
restaurant attendant
restaurateur
restorer
retail clerk
retailer
revenuer
reverand
reverend
reviser
revisionist
revivalist
rewrite man
rewriter
rhetorician
ribbonmaker
rickmaster
riddler
rifleman
riflemen
riftere
rigger
right hand man
ripper
rippier
riverboat pilot
riverman
riveter
rivetter
road mender
roadman
robber
rockgetter
rockman
rodeo rider
rodman
role model
roll turner
roller coverer
rolleyway man
roman cementer
roofer
room clerk
rope maker
ropemaker
roper
roundsman
roustabout
rover
rower
royal
royalty
rubbisher
rubbler
rugmaker
rugman
rugweaver
ruler
runner
running mate
rustler
sacristan
saddle tree maker
saddler
safety engineer
sage
sagent
saggar maker
sagger maker
sailing master
sailmaker
sailor
sailor boy
saint
sales demonstrator
sales manager
sales personnel
salesclerk
salesgirl
saleslady
salesman
salesperson
saleswoman
saloon girl
saloon owner
saloonist
salt boiler
saltboiler
salter
samurai
sand hog
sandesman
sandwichman
sanitary engineer
sanitation worker
sapper
sarariman
sarcinet weaver
sartor
satrap
saucer
saucier
saw doctor
sawbones
sawyer
saxophonist
say weaver
sayer
scabbard maker
scabbardmaker
scaffolder
scagiola maker
scaleraker
scappler
scavelman
scavenger
scene painter
sceneshifter
scholar
school principal
school superintendent
schoolmarm
schoolmaster
schrimpschonger
schumacker
scientific photographer
scientist
scotch draper
scotchman
scourer
scout
scoutmaster
scouts
screener
screenwriter
scribbler
scribe
scriber
scribler
scrimer
scrimshander
scripture reader
scrivener
scrutineer
sculler
scullery maid
scullion
sculptor
scutcher
scythesmith
sea captain
sea dog
sea lawyer
seafarer
seal presser
sealer
sealmaker
seaman
seamen
seamstress
searcher
second lieutenant
second mate
second in command
secret service agent
secret springer
secretarial assistant
secretary
secretary general
secretary of agriculture
secretary of commerce
secretary of defense
secretary of education
secretary of energy
secretary of health and human services
secretary of housing and urban development
secretary of labor
secretary of state
secretary of state for the home department
secretary of the interior
secretary of the treasury
secretary of transportation
secretary of veterans affairs
secretary generall
secritary
section hand
security
security director
security guard
sedgeman
seedsman
selectman
selectwoman
self acting minder
self employed person
seller
semi lorer
semiskilled worker
sempster
sempstress
senator
seneschal
senior director of program management
senior executive
senior pilot
senior vice president
senior vp
sensei
serf
sergeant
sergeant at arms
sergeant first class
sergeant at arms
serjeant at arms
sermonizer
servant girl
service advisor
serviceman
serving girl
servitor
setter
sewer
sewer hunter
sewer rat
sewing clerk
sewster
sex worker
sexologist
sexton
sgt
shagreen case maker
shaman
shanty man
shanty man
sharecrop farmer
sharecropper
shearer
sheargrinder
shearman
shearman or sherman
sheath maker
sheepherder
sheepman
sheepshearer
sheik
sheikh
shepherd
shepherdess
shepster
sheriff
sheriff officer
shill
shingler
ship husband
ship master
ship provisioner
ship's captain
ship's officer
shipboard soldier
shipbuilder
shipchandler
shipper
shipping clerk
shipwright
shoe finder
shoe wiper
shoe finder
shoe shiner
shoe wiper
shoemaker
shoesmith
shop assistant
shop boy
shop clerk
shop girl
shop keeper
shop mechanic
shopkeeper
shopwalker
shorer
shoresman
shorthand typist
shot firer
showman
shrager
shrieve
shrimper
shrink
shuffler
shunter
shuster
shuttle maker
sickleman
sidesman
siege engineer
siever
sign painter
signalman
silk dresser
silk engine turner
silk mercer
silk thrower
silk throwster
silk twister
silk carder
silk dresser
silk dyer
silk maker
silk mercer
silk snatcher
silker
silkmaid
silversmith
simpler
singer
sire
sissor or cissor
sitter
sizer
skald
skeiner maker
skelper
skepper
skilled worker
skinker
skinner
skipmaker
skipper
skivvy
sky pilot
slaper
slapper
slater
slaughterer
slaughterer of goats
slave trader
slave trade
slave driver
slavey
slaymaker
sleeper
sleuth
slop seller
slopseller
slubber
slubber doffer
smackman
smallware maker
smelter
smiddy
smith
smuggler
smugsmith
snake charmer
snake oil salesman
snapper
sniper
snobber
snobscat
snow warden
snuffer maker
soap boiler
soapboiler
social bandit
social scientist
social secretary
social worker
socialist
socialite
sociologist
sociologists
soda jerk
soda jerker
software architect
software engineer
software project manager
soil scientist
sojourner clothier
soldier
solicitor
solon
sommelier
sonographer
soprano
sortor
sound engineer
soundman
souter
spallier
speakeasy employee
speaker
spearman
special agent
specialist
spectaclesmaker
spectroscopist
speech therapist
sperviter
spice merchant
spicer
spinner
spinster
spirit
spiritual leader
splitter
spokeman
spokesman
spokesperson
spokeswoman
spooner
sports editor
sportsman
spurrer
spurrier
spy
squire
stableboy
stablehand
stableman
stabler
staff member
staff officer
staff reporter
staff sergeant
staffer
stage designer
stage director
stage manager
stage technician
stagehand
stager
stainer
stalker
stalking horse
stallman
stampman
standard bearer
stapler
starship captain
state representative
state senator
statesman
statesmen
station agent
stationary tender
stationer
stationmaster
statist
statistician
stay maker
steeple jacker
steeplejack
steersman
stenographer
stenterer
step boy
stevedore
steward
steward 
stewardess
stewsman
sticher
stillroom
stipendiary magistrate
stitcher
stock broker
stock brokers
stockbroker
stockinger
stoker
stone breaker
stone cutter
stone picker
stone worker
stone workers
stonecarver
stonecutter
stoneman
stonemason
stoner
stonewarden
store manager
storekeeper
storekeepers
storeman
storyteller
stowyer
stratigrapher
stravaiger
straw boss
straw joiner
straw plaiter
streaker
street artist
street cleaner
street musician
street orderly
street sweeper
street vendor
stretcher bearer
strike leader
strikebreaker
striker
stringer
stripper
striver
structural engineer
structural engineers
stuccoist
student
students
stuff gowsman
stuff weaver
stuffer
stuffgownsman
stunt coordinator
stunt double
stunt flier
stunt performer
stunt pilot
stylist
subaltern
subcontractor
subeditor
sublieutenant
submariner
sucksmith
suffragan
suffragan bishop
sukkari
sultan
summoner
sumner
sumpter
supercargo
superintendant
superintendent
superman
superordinate
supervising architect
supervisor
supplier
supply officer
surgeon
surgeon general
surveyor
sutler
swailer
swain
swamper
sweep
swell maker
swimmer
swineherd
swineherder
swineyard
swingler
switchboard operator
sword cutler
swordsmith
system administrator
systems analyst
systems designer
tabler
tackler
tacksman
tailor
tajir
tallow chandler
tallowchandler
tally clerk
tally clerk
tallyfellow
tallyman
tamer
tan bark stripper
tank driver
tankard bearer
tanker
tanner
taper weaver
tapester
tapestry dealer
tapestrymaker
tapicer
tapiser
tapiter
tapley
tapper
tappiologist
tapster
tar boy
tasker
taskmaster
taskmistress
tasseler
taste maker
taster
tavern keeper
taverner
tawer
tawyer
tax assessor
tax collector
tax lawyer
taxi driver
taxicab driver
taxidermist
taxman
taxonomist
tea lady
tea master
tea masters [n]
teacher
teamer
teamer man
teamster
teaser
technical sergeant
technical writer
technician
technicians
technocrat
technologist
teemer
telecoms analyst
telegraph operator
telegrapher
telegraphers
telegraphist
telemarketer
telephone operator
televangelist
teller
temp
temporary worker
tennis coach
tennis player
tenter
tenterer
territorial
test developer
test pilot
text editor
textile worker
textor
thacker
thane
thatcher
thaumaturgist
theater prompter
theatre director
theologian
theologians
theologist
therapist
thief
thieves
thimbler
thimblerigger
third mate
thirdborough
thonger
threadmaker
thresher
throwster
thurifer
ticket writer
tickney man
tide gauger
tide waiter
tidesman
tiemaker
tiger
tile maker
tile burner
tile theeker
tiler
tiller
tillerman
tillman
tiltmaker
time police
timekeeper
times ironer
tinctor
tinker
tinman
tinner
tinsmith
tinter
tinter or teinter
tipper
tippler
tipstaff
tirewoman
tixtor
tobacco spinner
todhunter
toe rag
toiler
toilinet manufacturer
toll agent
toll collector
toll keeper
toller
tollgate keeper
tollgatherer
tollie
tollkeeper
tollman
tolltaker
tolman
tonsor
tool and die maker
tool helver
toolmaker
top sawyer
topman
topsman
torchbearer
torturer
tosher
touch holer
touch typist
tow card maker
town chaberlain
town clerk
town crier
town husband
town marshal
townswaiter
toymaker
tozer
tracker
tracklayer
trade unionist
trademark attorney
trader
tradesman
traffic engineer
trail boss
trainbearer
trained worker
trainer
traines
trainman
trainmaster
trammer
tramper
trampler
tranqueter
transit planner
translator
transportation engineer
transportation secretary
tranter
trapper
traunter
travel agent
traveling salesman
travelling salesman
travers
treasurer
treasurers
treasury
treasury secretary
treen maker
treenail maker
trenchermaker
trencherman
trend setter
trepanger
trial judge
tribal chief
tribune
trier
trigonometer
trimmer
triumvir
troacher
trobairitz
troller
trombonist
troner
trooper
trotman
trott
trotter
troubadour
trouble shooter
troubleshooter
trouchman
trover
truchman
truck driver
trugger
trumpeter
trustee
tsar
tsarina
tsaritsa
tubber
tubedrawer
tubman
tuck pointer
tucker
tucker in
tumbler
tuner
tunist
turncock
turner
turnkey
turnpike keeper
turnspit
tutor
tweenie
tweeny
twist hand
twister
twisterer
tyler
typefounder
typesetter
typist
typographer
tyrant
tzarina
ufologist
ulama
ulnager
umpire
undercover agent
undersecretary
undertaker
underwriter
unemployed
unguentary
union member
unionist
unpaid worker
unskilled worker
upholder
upholsterer
upright worker
urban designer
urban forester
urban historian
urban planner
urchin
urologist
us marshal
userer
usher
usherette
utility man
vageler
vaginarius
valet
valet de chambre
valuator
vampire
vaquero
varlet
vassal
vatman
venator
venur
vendor
veneerer
verderer
verge maker
verger
verrier
verser
vestal virgin
vestmentmaker
vestryman
vestrywoman
vet
veteran
veteran 
veteran soldier
veterinarian
vibraphonist
vicar
vicar apostolic
vicar of christ
vicar general
vice admiral
vice chairman
vice chancellor
vice president
vice president of business development
vice president of business development and worldwide sales
vice president of sales
vice admiral
vice president
vice regent
vicereine
viceroy
viceroys
victualer
victualler
video editor
video game developer
viewer
vigilance man
vigilante
viking
villein
vineroon
vintager
vinter
vintner
violinist
violist
virginal player
viscount
viscountess
vizier
vocalist
vogelaar
vogeler
vogler
voluntary
volunteer
votary
vote counter
vowler
vp
vulcan
wabster
wadsetter
waferer
wage earner
waggoner
waggonwright
wagoner
wagonwright
wailer
wainwright
wait
waiter
waiter's assistant
waitman
waitress
wakeman
wakil
wali
walker
walking stick filer
wallah
waller
wantcatcher
wanter
ward heeler
warden
wardens
warder
warder  in charge of prisoners
warehouseman
warehouser
warlord
warper
warrant officer
warrener
warrior
washer
washerman
washerwoman
washman
washwoman
wasteman
watch finisher
watch maker
watchmaker
watchman
water bailiff
water baliff
water boy
water carrier
water dog
water gilder
water leader
water rat
waterman
waterseller
wattle hurdle maker
wattler
waulkmiller
waxchandler
way man
way maker
weaponsmith
weatherman
weatherspy
weaver
web designer
web developer
webber
webmaster
webster
wedding planner
weeder
weeper
weigher
weirkeeper
welder
welfare worker
well sinker
well wright
wellmaster
wellsinker
wellwright
western union man
wet glover
wet nurse
wetter
whacker
whaler
wharfinger
wheel tapper
wheeler
wheelwright
wheeryman
wherryman
whig
whipcord maker
whipmaker
whipper in
whipper in
whipperin
whit cooper
white collar worker
white limer
white smith
whitear
whitener
whitening roll maker
whiter tawer
whitesmith
whitester
whitewing
whitster
whittawer
wholesaler
wigmaker
willow plaiter
winder
window cleaner
window dresser
window trimmer
window washer
windster
wine seller
wine steward
wine waiter
winemaker
wing commander
wingman
wiredrawer
witch
wizard
wood cutter
wood reeve
wood seller
woodbreaker
woodcarver
woodcutter
woodman
woodmonger
woodranger
woodsman
woodturner
woodward
woodworker
wool driver
wool factor
wool grower
wool man 
wool sorter
wool stapler
wool winder
woolcomber
woolen billy piecer
woolman
woolsted man
worker
working cowboy
working girl
working man
working person
workman
workmate
worsted manufacturer
wrangler
wrecker
wrestler
wright
wyrth
xray technician
x ray technician 
xenobiologist
xylographer
xylophonist
yachtsman
yachtswoman
yardman
yardmaster
yatman
yearman
yeoman
yodeler
youth worker
zincographer
zitherist
zoetrope maker
zoographer
zookeeper
zoologist");
1;

