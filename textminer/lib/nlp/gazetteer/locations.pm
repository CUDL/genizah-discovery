####################################################################################
##Author:       Chris Stokoe
##File:         stemmer.pm 
##Date:         28/02/13
##Version:      1.0
####################################################################################
package nlp::gazetteer::locations;

use strict;
use warnings;

use base qw(Exporter);

our @EXPORT_OK = qw( %locations );

our %locations = map { $_ => 1 } split(/\n/, "damiyatta
ramli
tabari
al-shim
zifti
minyat
lukk
jerusalem
minyat
zifta
alexandria
tahort
busir
daltun
demona
zirid
flinders
ghaza
fuwwah
suisa
fayyum
al-mahdiyya
jazirah
qawasaniyah
qiisinah
dabiqi
bilbays
candia
fust
barqa
samannfid
maghrib
khaybar
dammfih
naples
denia
rfimi
denia
aswin
ashkelon
maliki
tilimsan
palermo
cairo
milij
hebron
al-mahdiyya
sahrajt
sunbat
trapani
mangalore
yemen
safaqus
sinai
benha
damsis
biniyis
al-Mahalla
tatai
jacfariyah
al-jacfariyah
cordova
granada
sijilmassa
tanan
mahdiyya
gaza
ascolon
damirah
damanhiir
damanhilr
fustat
misr
al-qdhirah
hadrak
sura
qabis
ascalon
qabis
ramlah
fustit
ramlah
nahrwara
selucia
rosetta
rossetta
tabaristan
maghrib
qus
");
1;
