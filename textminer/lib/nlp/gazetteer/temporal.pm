####################################################################################
##Author:       Chris Stokoe
##File:         temporal.pm 
##Date:         12/04/13
##Version:      1.0
##Description: Shallow temporal tagger loosly based on the mitre model used in MUC7
####################################################################################
package nlp::gazetteer::temporal;

use strict;
use Data::Dumper;
use Lingua::EN::Tagger;

###########################################
## Global declarations for temporal expressions

my %Ord2Num = (   "zero", 0, "first", 1, "second", 2, "third", 3, "fourth", 4,
                  "fifth", 5, "sixth", 6, "seventh", 7, "eighth", 8,
                  "ninth", 9, "tenth", 10, "eleventh", 11, "twelfth", 12,
                  "thirteenth", 13, "fourteenth", 14, "fifteenth", 15,
                  "sixteenth", 16, "seventeenth", 17, "eighteenth", 18,
                  "nineteenth", 19, "twentieth", 20, "twenty", 20, "twenty-first", 21,
                  "twenty-second", 22, "twenty-third", 23, "twenty-fourth", 24,
                  "twenty-fifth", 25, "twenty-sixth", 26, "twenty-seventh", 27,
                  "twenty-eighth", 28, "twenty-ninth", 29, "thirtieth", 30,
                  "thirty", 30, "thirty-first", 31, "fourty", 40, "fifty", 50, "sixty", 60,
                  "seventy", 70, "eighty", 80, "ninety", 90, "hundred", 100, "thousand", 1000,
                  "million", 1000000, "billion", 1000000000, "trillion", 1000000000000);

my $OrdinalWords = "(tenth|eleventh|twelfth|thirteenth|fourteenth|fifteenth|sixteenth|seventeenth|eighteenth|nineteenth|twentieth|twenty-first|twenty-second|twenty-third|twenty-fourth|twenty-fifth|twenty-sixth|twenty-seventh|twenty-eighth|twenty-ninth|thirtieth|thirty-first|first|second|third|fourth|fifth|sixth|seventh|eighth|ninth)";

my $NumOrds = "([23]?1-?st|11-?th|[23]?2-?nd|12-?th|[12]?3-?rd|13-?th|[12]?[4-90]-?th|30-?th)";


my %Periods2Range = ( "fatimid" => "909-1173",
		      "pre-ottoman" => "1243-1453",
		      "geniza" => "900-1299",
		      "genizah" => "900-1299",
		      "ottoman" => "1453-1922",
		      "ayyubid" => "1171-1250",
		      "umayyad" => "661-750",
		      "gaonic" => "589-1033",
		      "byzantium" => "330-1453",
		     );
		
my $OT = "(<[^\/][^>]*>)";
my $CT = "(<\\\/[^>]*>)";
my $OTCD = "<lex[^>]*pos=\\\"?CD[^>]*>";
my $OTNNP = "<lex[^>]*pos=\\\"?NNP?[^>]*>";

my $p = new Lingua::EN::Tagger;
####################################################################################
sub new {
        my $class = shift;
        my %defaults =  ('lower' => 600,
			 'upper' => 1800);
	my $self = {%defaults, @_};
        bless $self, $class;
        return $self;
}
####################################################################################
sub tagger {

    my $self = shift;
    my($sentence) = @_;

    my %dates = ();

    
    $sentence =~ s/\./ /g;
    $sentence =~ s/\s-\s/-/g;
    $sentence =~ s/\x{2013}/-/g;
    $sentence =~ s/\(\d+-\d+\/(\d+-\d+)\)/$1/g;
    $sentence = $p->add_tags($sentence);
    $sentence =~ s/<(.*?)>(.*?)<(.*?)>/\<lex pos=\"\U$1\">\E$2<\/lex>/g;

    ################################################################################
    #Three and Four digit years and decades
    ################################################################################
    my $tmp = $sentence;
    $sentence = "";
    while($tmp =~ /($OT+((mid-$CT*$OT*)?(\d{3,4})(s?|'s?))$CT+)/) {
        $sentence .= $`; $tmp = $';  my $time = $1;
        next if $tmp =~ /\A\s+$OT+\w+$CT+\s+($OT+(daylight|standard)$CT+\s+)?$OT+time$CT+/ois;
        next if $tmp =~ /\A\s+$OT*$OTNNP/os;
        next if $tmp =~ /\A\s+$OT+(\])+$CT/;
        next if ($7 < $self->{'lower'} || $7 > $self->{'upper'});
        next if $sentence =~ /$OT+(page|pages)+$CT/ois;
        #next if $sentence =~ /$OT+([)+$CT\s$/ois;
	my $tag = "";
	if ($time =~ /$OT*(s|'s)$CT/) {
		$tag = "DECADE"; 
	} else {
		$tag = "YEAR";
	}
        $sentence .= "<TIME TYPE=\"$tag\">$time</TIME>";
    }
    $sentence  .= $tmp;

    ################################################################################
    #Centuries
    ################################################################################
    $sentence =~ s/((($OT+(late|early)$CT+\s+$OT+in$CT+\s+)?$OT+the$CT+\s+($OT+(late|early|mid-?)$CT*\s*)?)?$OT*($NumOrds-?|$OrdinalWords)$CT*\s*$OT*century$CT+)/<TIME TYPE=\"CENTURY\">$1<\/TIME>/sogi;

    ################################################################################
    #periods and era's
    ################################################################################
    my $regexp = join('|',keys %Periods2Range);
    $sentence =~ s/($OT*($regexp)$CT*\s*$OT*(period|era)$CT+)/<TIME TYPE=\"PERIOD\">$1<\/TIME>/sogi;

    ################################################################################
    #Ranges
    ################################################################################
    $sentence =~ s/($OT*(\d{3,4})(-|\/)(\d{1,4})$CT)/<TIME TYPE=\"RANGE\">$1<\/TIME>/sogi;

    
    $sentence =~ s/<lex[^>]*>//g;
    $sentence =~ s/<\/lex>//g;

    my (@signatures) = $sentence =~ /(<TIME.*?<\/TIME>)/g;
    foreach my $event (@signatures) {
        my ($type, $value) = $event =~ /TYPE=\"(.*?)\">(.*?)</;
        if ($type eq "YEAR") { $value =~ s/\D//g; $dates{$value}++; }
        if ($type eq "CENTURY") {
                my ($startdate, $enddate) = "";
                foreach my $token ( split(/\s/, $value) ) {
                        if (defined($Ord2Num{lc($token)})) {
                                $startdate = $Ord2Num{lc($token)} - 1;
                        }
                }
                if ($value =~ /(\d+)/o) {  $value =~ s/\D//g; $startdate = $value - 1; }
                $startdate = $startdate * 100;
                $enddate = $startdate + 99;
                my @range = ($startdate..$enddate);
                foreach my $year (@range) {
                        next if ($year < $self->{'lower'} || $year > $self->{'upper'});
                        $dates{$year}++;
                }
        }
	if ($type eq "DECADE") {
		$value =~ s/\D//g;
		my @range = ($value..$value + 9);
		foreach my $year (@range) {
                        next if ($year < $self->{'lower'} || $year > $self->{'upper'});
                        $dates{$year}++;
                }
	}
	if ($type eq "RANGE") {
		my ($start, $end) = split(/\/|-/, $value);
		if (($start + 614) == $end) { $start = $end; }
		if (length($start) > length($end)) {
			my $window = length($start) - length($end); 
			$start =~ m/(.{$window})/;
			$end = $1.$end;
		}
		 my @range = ($start..$end);
                foreach my $year (@range) {
			next if ($start < $self->{'lower'} || $end > $self->{'upper'});
                        $dates{$year}++;
                }
	}
	if ($type eq "PERIOD") {
		my ($period) = $value =~ m/^(\w+)/;
		my ($startdate, $enddate) = "";
                if (defined($Periods2Range{lc($1)})) {
                     	my ($startdate, $enddate) = split(/-/,$Periods2Range{lc($period)});
                	my @range = ($startdate..$enddate);
                	foreach my $year (@range) {
                        	next if ($year < $self->{'lower'} || $year > $self->{'upper'});
                        	$dates{$year}++;
                	}
		}
	}
    }
    return($sentence, \%dates);
};
#####################################################################################
1;
