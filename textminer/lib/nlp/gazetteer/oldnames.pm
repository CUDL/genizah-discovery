####################################################################################
##Author:       Chris Stokoe
##File:         stemmer.pm 
##Date:         28/02/13
##Version:      1.0
####################################################################################
package nlp::gazetteer::names;

use strict;
use warnings;

use base qw(Exporter);

our @EXPORT_OK = qw( %names );
our %names = map { $_ => 1 } split(/\n/, "'abbas
'abbas.
'abbasi
'abbud
'abd
'abd-al-baqi
'abd-al-majid
'abd-al-wahhab
'abd-allah
'abdailah
'abdalla
'abdalla-h
'abdallah
'abdulla
'abir
'abr
'ad
'adaw
'adaya
'adi
'adiya
'adiyya
'adya
'aff
'afif
'afs
'ajam
'akkaw
'akubas
'al
'ala
'alam
'alan
'ali
'ali-
'aliyya
'all
'alla-n
'allal
'allan
'allu-n
'allun
'allus
'allush
'alvan
'amaim
'amar
'amd
'amid
'amir
'amma-r
'amman
'ammanl
'ammar
'ammat
'amram
'amran
'amrs
'amrum
'amtan
'an
'anan
'anbar
'annan
'ant.u-h.
'aqil
'aqil.
'aqiva
'aqni-n
'aqnln
'aqraban
'ars
'aru-s
'arus
'arfis
'asar
'assar
'ata
'ata'
'atar
'atias
'atiyya
'av
'avlan
'awa'id
'awa-d
'awad
'awaid
'awkal
'ayya-sh
'ayyal
'ayyas
'ayyash
'azariah
'azarya
'azaryahu
'azaz
'azi-za
'azizah
'azlza
'azr
'azz
'azza
'azzay
'azzun
'e-zer
'eli
'ezer
'fadl
'ibad
'ibn
'imad
'imra-n
'imran
'ira-n
'iraq
'iwad
'iwadh
'izrun
'l
'l-.*
'l-'ala
'l-'aziz
'l-'izz
'l-afrah
'l-baha
'l-baqa
'l-baraka-t
'l-barakat
'l-bashar
'l-bayan
'l-fadha'il
'l-fadhl
'l-fadl
'l-fakhar
'l-fakhr
'l-farah
'l-faraj
'l-fudhail
'l-hajjaj
'l-hasan
'l-hayy
'l-husain
'l-kahayr
'l-karam
'l-karim
'l-kayr
'l-khair
'l-khayr
'l-ma'ali
'l-ma'ani
'l-mahasin
'l-majd
'l-makarim
'l-mansur
'l-matar
'l-mufadhdhal
'l-muna
'l-munah
'l-munajja
'l-najm
'l-nasib
'l-parnes
'l-qasim
'l-qurra
'l-rabi
'l-ridha
'l-shuja'
'l-surur
'l-taha
'l-thana
'l-wafa
'l-wahsh
'l-walid
'l-yaman
'olah
'onen
'shaq
'ubayd
'ubaydallah
'ud
'udayb
'ulah
'ulla
'ulla-
'ullah
'umair
'uman
'umar
'umayra
'ums
'uqba
'uqban
'usaq
'uzza
'uṯman
-ma'ali
a-rah.
aaron
abarbanel
abba
abbas
abd
abdalla
abdalla-h
abdallah
abdulla
abi
abi'l-'izz
abi'l-bayan
abi'l-fakhr
abi'l-hasan
abi'l-hayy
abi'l-khayr
abi'l-rida
abi'l-sa'id
abi-hay
abiathar
abifl-hasan
abigdor
abir
abitur
abl
abr
abraham
abraham.
abrahams
abram
abramson
abravanel
abu
abu'l
abu'l.*
abu'ali
abu'i-faraj
abu'l-'ala
abu'l-'izz
abu'l-aynayn
abu'l-baha
abu'l-barakat
abu'l-bayan
abu'l-bishr
abu'l-facll
abu'l-fada'il
abu'l-fadl
abu'l-fadl-abu'l-munajja
abu'l-fakhr
abu'l-faraj
abu'l-fatfl
abu'l-fath
abu'l-futuh
abu'l-gharib
abu'l-hajjaj
abu'l-hasan
abu'l-hayy
abu'l-husayn
abu'l-jud
abu'l-karam
abu'l-khayr
abu'l-ma'all
abu'l-ma'ani
abu'l-ma'ruf
abu'l-mahasin
abu'l-majd
abu'l-makarim
abu'l-mansur
abu'l-mufaddal
abu'l-muna
abu'l-munajja
abu'l-mune
abu'l-murajja
abu'l-najm
abu'l-qasim
abu'l-qasim's
abu'l-rabf
abu'l-rida
abu'l-riza
abu'l-surur
abu'l-tahir
abu'l-thana
abu'l-thana'
abu'l-tzz
abu'l-'ula
abu'l-wafa
abu'l-yumn
abu-
abu-fudhail
abu-sa'ra
abu'l-fadl
abu'l-macali
abu'l-faraj
abui-cala
abui-fakhr
abui-hasan
abui-najm
abulafia
abun
abuvala
abzar
abzardil
ad
adam
adams
adaw
adaya
aderet
adham
adhan
adi
adiya
adiyya
adonijah
adret
adunim
adya
afdal
aff
afs
agent
agostaro
ah
aha
ahai
aher
ahitophel
ahliyya
ahmad
ahub
ahuv
ahwal
ajall
ajam
akhuh
akiva
akkaw
aklabi
akramiyya
akubas
al
al'abid
al'adani-
al'ami
al-
al-'abd
al-'adani
al-'adil
al-'aff
al-'afif
al-'afs
al-'afsi
al-'ajam
al-'ajami
al-'ajaml
al-'akkaw
al-'amm
al-'amman
al-'ammani
al-'ammanl
al-'ammar
al-'amtan
al-'an
al-'anbar
al-'ani
al-'ann
al-'aqraban
al-'aqrabani
al-'arf
al-'ars
al-'asab
al-'asar
al-'asqalani
al-'assar
al-'attar
al-'avlan
al-'ayyal
al-'azi-z
al-'aziz
al-'azr
al-'azz
al-'ilm
al-'imran
al-'iraq
al-'iraqi
al-'iwadh
al-'izz
al-'omarl
al-'ud
al-'udl
al-'Ābid
al-abzar
al-abzari
al-afdal
al-afsi
al-aftar
al-ahl
al-ahwal
al-ajall
al-ajami
al-akah
al-akram
al-amidi
al-amir
al-amjad
al-amsat
al-amshaj
al-amshatl
al-andalus
al-andalusi
al-ansarl
al-aqrac
al-aqran
al-as'ad
al-ashmunl
al-ashqar
al-askenazi
al-asqar
al-atrabuls
al-aziz
al-aṯarib
al-ba'ba'
al-ba-qi-
al-badagus
al-badawi
al-baddl
al-bagdad
al-baghdadi
al-baghdadl
al-baha
al-bahall
al-bahir
al-bahnas
al-balk
al-balkhl
al-banat
al-baniyasi
al-banna'
al-baq
al-baqa
al-baql
al-baradan
al-barakat
al-baraki
al-bardani
al-barghawatl
al-barjalun
al-barq
al-barql
al-barzanj
al-bas.i-r
al-bas.ri-
al-basir
al-basmall
al-basr
al-basri
al-basrl
al-basut
al-bataihi
al-bathanunl
al-bawarid
al-baydayan
al-baylab
al-bayrut
al-baysani
al-bayt
al-bayyan
al-berutl
al-bilbaisi
al-bilbays
al-buktaj
al-buni
al-bunl
al-bustan
al-cadl
al-caf!f
al-cafif
al-camid
al-cammani
al-carab
al-cazim
al-caziz
al-da'ud
al-da'wa
al-da-'im
al-dabah
al-dabbag
al-dajaj
al-dajaji
al-dalal
al-dallala
al-damasqi
al-damati
al-damiyat
al-damlri
al-damr
al-damsisi
al-damss
al-damyat
al-dani
al-danwar
al-daqq
al-dar
al-dar'
al-dastur
al-dawla
al-dawud
al-dayyan
al-dazim
al-dbid
al-derci
al-dhahabi
al-dhuv
al-dihqdn
al-dimashql
al-dimasq
al-dimyat
al-dimyati
al-din
al-dln
al-dn
al-dustar
al-fa'izi
al-fa-si-
al-fada'il
al-fadil
al-fadl
al-fakhkhar
al-fakhr
al-fakr
al-faqqa'
al-faqr
al-faqus
al-farah
al-faraj
al-fararj
al-faras
al-fas
al-fasi
al-fayruz
al-fayyu-mi-
al-fayyumi
al-fdsid
al-furat
al-furs
al-gadol
al-garb
al-gazzal
al-ghazal
al-h.arizi
al-h6d
al-habab
al-hadaba
al-haddad
al-hadraml
al-hafiz
al-hakim
al-haklm
al-hakm
al-halab
al-halabi
al-halabl
al-halla
al-hamadanl
al-hamaw
al-hamdan
al-haqq
al-haram
al-harir
al-hariri
al-harizi
al-harr
al-harranl
al-hasab
al-hasan
al-hasana
al-hasd
al-haver
al-hawar
al-haytham
al-hazam
al-hazzan
al-hi-ti-
al-hijaziyya
al-hirbis
al-hulaybi
al-husain
al-husayn
al-husn
al-iktisar
al-ikwa
al-ilblrl
al-imam
al-isfahanl
al-iskandar
al-iskandaran
al-iskandaranl
al-islam
al-isralll.
al-istiban
al-ja'far
al-jabartl
al-jabban
al-jabbar
al-jahbidh
al-jalal
al-jall
al-jalla
al-jam'
al-jamall
al-jamf
al-janaw
al-jasus
al-jasus.
al-jawhar
al-jawz
al-jayidah
al-jayz
al-jazfn
al-jazifinl
al-jaziflnl
al-jibal
al-jibll
al-jubayl
al-jubayll
al-jubran
al-judayy
al-juyush
al-kabbaza
al-kabir
al-kabra
al-kadim
al-kafarmand
al-kalam
al-kallam
al-kamal
al-kamil
al-kamin
al-kams
al-kamuk
al-kamukhl
al-kari-m
al-karim
al-karm
al-karma
al-kastanani
al-kastantani
al-katib
al-katt
al-kawat
al-kawlan
al-kayr
al-kazin
al-kdtib
al-khaliq
al-khass
al-khassa
al-khawat
al-khaybari
al-khayyat
al-khayyaṭ
al-khazin
al-khddim
al-kind
al-kirmani
al-kirmanl
al-kitab
al-kohen
al-kufat
al-kuhl
al-kull
al-kurani
al-kuttab
al-kuz
al-laban
al-labban
al-ladhiqi
al-ladiq
al-laktus
al-latf
al-latif
al-lebdi
al-levi
al-luban
al-ma'al
al-ma'an
al-ma'arrl
al-ma'mun
al-mactuq
al-maghnbi
al-maghrebi
al-maghrebl
al-maghrib
al-maghribi-
al-magrebi
al-magrib
al-mahall
al-mahalla
al-mahalliyya
al-mahalll
al-mahdaw
al-mahdawi
al-mahmur
al-majd
al-majjan
al-majjani
al-majjanl
al-majjanl.
al-makhmuri
al-makln
al-makmur
al-makn
al-malatl
al-malih
al-malik
al-mamluk
al-mansur
al-maqdasi
al-maqdisi
al-marawihl
al-marjan
al-marrakushi
al-mas'ud
al-mashita
al-maskil
al-maskl
al-maskll
al-masmud
al-masos
al-masq
al-mass
al-massat
al-mastuba
al-masuna
al-masus
al-mawazinl
al-mawla
al-mazuq
al-melamed
al-mesorer
al-mevin
al-milah
al-minqar
al-misr
al-misrat
al-mu'add
al-mu'allim
al-mu'allima
al-mu'assil
al-mu'azzaza
al-mu'min
al-mubarak
al-mubariz
al-mubashshiri
al-mubayyin
al-mucallim
al-mucazzam
al-muhaddab
al-muhadhdhab
al-muhandiz
al-muhsin
al-mujawir
al-mukar
al-mukarram
al-mulah
al-mulham
al-mulk
al-mulk-aden
al-mulkl
al-mumajjid
al-mumhe
al-muna
al-munajja
al-munajjim
al-mundhiri
al-munr
al-muqaddam
al-muqaddas
al-muqaddasiyya
al-muqaffac
al-muqammas.
al-muqammis
al-muqassar
al-murid
al-murq
al-musall
al-musas
al-musawi
al-mushnaqa
al-musinn
al-musq
al-mutarris
al-mutatabbib
al-mutr
al-muttalib
al-muwaffaq
al-muzaghlil
al-na'ila
al-naba'
al-nacja
al-nacnac
al-nadlm
al-naffakh
al-nafis
al-nafs
al-nafus
al-nafusa
al-nafusi
al-naghira
al-nagid
al-nagira
al-naha-wandi-
al-nahhal
al-nahrawan
al-naja'
al-najar
al-najb
al-najib
al-nakhuda
al-namla
al-naqid
al-nas
al-nasab
al-nasir
al-nasl
al-nasr
al-natra
al-nazar
al-nihm
al-nisabur
al-nl
al-nu'm
al-nucman
al-nufus
al-nul
al-nusayr
al-parnas
al-qa'id
al-qabis
al-qabisi
al-qad
al-qadar
al-qadir
al-qadur
al-qafas
al-qafs
al-qahir
al-qala'i
al-qalaci
al-qalyub
al-qansi
al-qarawi
al-qarqsn
al-qas
al-qasb
al-qasbl
al-qassa'
al-qastalan
al-qata'f
al-qata'if
al-qataif
al-qathif
al-qatman
al-qawdaf
al-qaysaran
al-qaysarani
al-qazzaz
al-qazzdz
al-qiftl
al-qirqisa-ni
al-qirqisanl
al-qisas
al-qu-misi-
al-quda'i
al-quds
al-qudsi
al-qumisi
al-qumm
al-qur
al-qusayr
al-ra'sa
al-ra-bi'ah
al-radi
al-rahab
al-rahbl
al-rahm
al-rahman
al-rais
al-rajwa
al-raml
al-ramli
al-raqq
al-raqqa'
al-raqql
al-rashidi
al-rashidl
al-rashld
al-rav
al-rayyis
al-rayys
al-razi
al-rida
al-riyad
al-riyasa
al-ru'asa
al-ruhawi
al-rum
al-rumi
al-rumiyya
al-ruml
al-sa''al
al-sa'igh
al-sabbab
al-sabbag
al-sabbagh
al-sabbak
al-sada
al-sadld
al-sadr
al-safaqus
al-saffur
al-safl
al-sahl
al-sahrashti
al-sakhawi
al-salam
al-salamanti
al-salaml
al-salar
al-sald
al-sallar
al-salmon
al-sam
al-sama
al-samawal
al-samiyya
al-samma'
al-sammak
al-sams
al-samsud
al-sanal
al-sananiri
al-sananr
al-sangari
al-saqqaf
al-sarab
al-sarq
al-sarqaw
al-sarr
al-sarrab
al-saruj
al-sayk
al-sayraf
al-sayraji
al-sdgul
al-shalr
al-shami
al-shaml
al-shammas
al-sharabl
al-sharaf
al-shaykh
al-shumaym
al-sibag
al-sijilmas
al-simsar
al-sinja-ri-
al-siqill
al-siqilli
al-siqilliyya
al-siqilll
al-sirr
al-sis
al-sitt
al-sofer
al-sofet
al-sohet
al-somer
al-sraj
al-srat
al-su'ada
al-subk
al-sucada
al-sukkar
al-sukkari
al-sukr
al-sulaml
al-sumr
al-sur
al-szr
al-tabb
al-tabba-n
al-tablb
al-tadmur
al-tahertl
al-tajir
al-talmid
al-tanaml
al-tannur
al-taqi
al-tarabuls
al-tarabulusi
al-tasdlr
al-tawwan
al-tawwaz
al-tawwazi
al-tayb
al-thana'
al-tibran
al-tilimsan
al-tinnisi
al-tinnlsl
al-tinns
al-tu'ati
al-tujjar
al-tulatuli
al-tuns
al-turaf
al-tustari
al-tustarl
al-ukhtush
al-ukuwwa
al-wa-h.id
al-wahid
al-wakl
al-wal
al-waqar
al-wasit
al-wastaniyya
al-wazzan
al-wazzdn
al-wuhsa
al-wuhuf
al-yahud
al-yaman
al-yatim
al-yatm
al-yesod
al-yumn
al-zaffan
al-zaffat
al-zajjaj
al-zak
al-zakkar
al-zakl
al-zaman
al-zaqal
al-zarbl
al-zariz
al-zaylusi
al-zayyat
al-ziqli
al-ziza
al-zubayr
al-zulaf
al-Āmid
al-Ġalib
al-Ġarb
al-Ḍarura
al-Ḏahab
al-Ḫawa
al-‘attar
ala
alakhayr
alam
alan
alasqar
albaghda-di-
alfarabi
alfasi
algazi
ali
alisbili
aliyya
aljamall
aljuyush
alkyn
alla-h
alla-n
allah
allal
allan
allu-n
alluf
allun
allus
also
alu'el
aluf
alvan
ama
amaim
amar
amat
amd
ami-n
amid
aminah
amir
amittai
amln
amma-r
amman
ammar
ammat
amn
amr
amram
amran
amrs
amrum
amsat
amtan
amzafir
an
anan
anas
anatoli
anbar
and
andalus
andreas
andria
aniz
annan
anshe
ant.u-h.
apothecary
aqavya
aqiba
aqiva
aqraban
aqran
ar-rahman
arah
araham
aram
ardia
ardutiel
aripul
aristotle
arragel
ars
aru-s
arubas
arus
aruyevitz
arye
as
asa
asad
asad.
asar
asbag
ascad
aseo
aser
asher
ashkelon
ashkenazi
asi
askenaz
askenazi
asmum
asqar
asqawi
assalmun
assar
at.a-'
ata
atarib
atias
atiyya
atniq
atrabuls
attar
attiah
av
avd
averroes
avicenna
avitur
avlan
avon
avtalyon
avyatar
awa-d
awad
awaid
awkal
ayya-sh
ayyas
ayyash
ayyub
azariah
azarya
azaryahu
azaz
azhar
azi-za
aziz
azjaj
azlza
azr
azriel
azulay
azz
azza
b\\.
b.judah
ba-ba-i-
ba-havura
ba-na
baba
babli
baboi
badal
badr
badusa
baduss
badw
baha
baha'
baha-
bahall
bahiyya
bahlul
bahnas
bahya
bajriqa
baker
bakr
balaam
baladn
balagh
baliocos
balk
balluta
banat
baniyas
banln
bann
banna
banu
banuna
banuqa
banyās
baq
baqa
baqara
bar
barabik
baradan
baradanl
barak'el
baraka
baraka-t
barakat
barakat-berakhot
baraki
bardani
bardugo
bargut
barhon
barhu-n
barhun
barjalun
baron
baronito
barq
barra
baruch
baruk
barukh
barzanj
barzel
barzillai
basara
bashyachi
bashyatchi
basl
basr
basut
bat
battuta
bavli
bawarid
bayan
baydayan
baylab
bayrut
bayyan
bayys
beadle
bedere
bedrsi
behor
bejamin
ben
ben-asher
ben-meir
bena-ya-
benaiah
benaya
benayah
benayahu
benayya
benha
beninah
benjamin
benveniste
bera-khot
berachiah
berakhah
berakhel
berakhot
berakhoth
berakot
berakya
berav
berechiah
berekya
bernard
bertinoro
besalel
bet
beth
bezaleel
bihaya'
bikar
bint
binyam
bisha-ra
bishr
bisr
blml
bnu
boaz
bonfat
bonfed
bonfils
bonsenior
bonsenyor
brünn
bu
bu'l-barakat
bu'l-dimm
bu'l-fadl
bu'l-hasan
bu'l-makarim
Buluqqin
budayr
buhuri
bujd
buktaj
bunan
bunayya
bunda-r
bundar
bunyam
buraq
burayh
burayk
bushr
bushran
busir
busr
bustan
buvala
cabd
cadmiel
cah
caleb
carcassone
caro
caster
castro
cata
chizana
culla
d-dunya
d\\.
da'an
da'ud
da'ud.
da-'u-d
da-niya-l
da-wu-d
dabah
dabbag
dahab
dajaj
dalal
dalata
dalati
dalun
damiyat
dammuhi
damr
damss
damuhi
damyat
dan
danad
danan
daniel
daniyal
danwar
daqq
dar
dar'
darra
daruqs
dastur
dates.
daud
daudi
david
david.
davidson
dawla
dawud
dayan
dayyan
de
deayyan
del
delmedigo
denaphtah
derca
deri
dhiya'
di
dihqan
dimasq
dimasqiyya
dimyat
dimyati
dimyatl
din
diviner
diya
dlarya
dn
dnun
do-sa-
dolce
donna
donnolo
dosa
dragoman
dukan
dukhan
dukhr
dukr
dunas
dunash
duqmaq
dura
duran
durra
durri
dustar
dustari
e-zer
eber
efraim
el
el'azar
el'azav
elazar
eldad
eleazar
elhanan
eli
elia
eliah
eliakim
eliezer
elihu
elijah
elijah's
elijahs
elimelech
elisa'
elisha
eljiah
elkana
elnathan
elohim
elqana
elvira
elyaqim
elyon
emmanuel
enoch
ephraim
espadil
esther
etan
ethan
evyatar
ezar
ezekiel
ezer
ezmeralda
ezra
ezrah
fa'iza
fad.l
fada'il
fadail
faddal
fadhl-allah
fadhlah
fadil
fadila
fadl
fadlan
fahd
fahda
faiza
fakhir
fakhr
fakr
fakriyya
fanu
faqqa'
faqus
farah
faraj
farajiyya
farajl
faras
faraskur
fard
farha-d
farhun
fariente
farjiyyah
farrah
farraj
fas
fashsha-t.
fashshat
fasi
fassat
fatahya
fatima
fatla
fatum
fayru-z
fayruz
fayyumi
finhas
fitru
frances
francisco
fransis
frjlh
fudayl
fudhail
fuhayd
funduqan
fura-t
furah
furaij
furaj
furat
furayj
furqan
fusayl
futayh
futu-h.
futuh
ga'im
ga'on
gab-bay
gabirol
gabriel
galib
galican
galiyya
galiyā
galya
gana'im
ganiyya
ganm
garaba
garba
gascon
gatenio
gavison
gayyath
gaza-l
gazal
gazzali
ge'onim
gentile
gerondi
gershom
gersom
gerson
gha-lib
ghalb
ghalib
ghayyath
ghazal
ghiya-th
ghiyath
ghulayb
gina'
girba
giyyat
glazier
gordon
gorion
grisolin
gula
gulayb
gullab
h.alfo-n
h.asan
h.asday
h.assu-n
h.at.ib
h.ayyi-m
h.ayyu-j
h.esed
h.isday
h.o-ta-m
h.usayn
ha-
ha-alef
ha-andalus
ha-bahur
ha-baradan
ha-bavl
ha-bavli
ha-bavli.
ha-binot
ha-dani
ha-dayyan
ha-durmasqi
ha-ezrahi
ha-galuyot
ha-goyyim
ha-hazzan
ha-hazzdn
ha-hebroni
ha-hillel
ha-jall
ha-kenesd
ha-kohen
ha-levi
ha-magrib
ha-malkuth
ha-meculle
ha-meiri
ha-melammed
ha-meshorer
ha-mesorer
ha-mevin
ha-mumhe
ha-nagid
ha-nanyah
ha-nasi
ha-ndsi
ha-negidim
ha-parnas
ha-qal'i
#ha-qahal
ha-qatan
ha-qdhdl
ha-qehillot
ha-rofe
ha-rofe'
ha-roje
ha-ru-n
ha-sar
ha-sarfati
ha-seder
ha-sefardi
ha-shofet
ha-shqfet
ha-sofet
ha-sovi
ha-talmid
ha-talmld
ha-tov
ha-tulaytul
ha-yarh
ha-yerushalml
ha-yeshiva
ha-yesiva
ha-zaqen
ha-zaken
hab-babli
habab
habas
habb
habba
habib
habshush
hadaq
hadassi
haddad
hadid
hadr
haffad
hag-gaon
hag-ger
haggai
hai
hajar
hajjaj
hak-kohen
hakam
hakari
hakim
hakm
hakmon
hakmun
hal-levi
halab
halala
halevi
halfata
halfen
halfon
hallab
hamadani
hamaw
hamdan
hamdun
hami
hamma
hammad
hammam
hammu
hamn
hamud
hamza
han
han-n.*
han-nagid
han-nasi
hana
hanagid
hanan
hanan'el
hananeel
hananel
hananiah
hananiah.
hananya
hananya.
hani
hanlfa
hannah
hanuna
hap-parnas
hap-penini
har'
harb
harbis
harbonah
harith
harizah
harr
harun
haruni
harz
hasab
hasan
hasan.
hasana
hasanah
hasday
hash-shammash
hasid
hasna
hass
hassan
hassana
hassun
hater
hatim
hatimiyya
hawajib
hawar
hawqal
hay
haya
haydara
hayfa
hayfa-'
hayuna
hayy
hayya
hayyan
hayyi-m
hayyim
hayylm
hayym
hayyuj
hayyun
hazan
haziyya
hazqll.
hazquni
hazz
hazzan
he-h.asid
he-hakam
he-hasid
he-haver
he-hazan
he-hdsid
he-hdver
he-hevroni
he-me'ulle
hebron
hebroni
hefes
hemdat
hephez
hesed
hezekiah
hiba
hibah
hibat
hibat-allah
hibatulla
hibatullah
hibba
hiddiyya
hidhq
hidq
hifz
hikaya
hila
hilai
hilal
hilala
hillal
hillel
hini
hinnawi-husn
hirbish
hisda
hisday
hiwwi
hiwwl
hiyya
hod
hoda
hodaya
hodiah
hofni
hofnl
hophni
horosban
hosa'ana
hosa'na
hosha'na
hot.er
hotam
hubara
hubban
hubbs
hudayd
hudayda
hudayfa
hujayla
hulayf
hump
humra
huna
hunayn
huriyya
husain
husam
husan
husayn
hushiel
husi'el
husn
husna
hussan
ibad
ibadiah
ibn
ibn'ammar
ibn.
ibnjalal
ibnjamahir
ibnjami'
ibnjarwa
ibra-him
ibrahi-m
ibrahim
ibrahm
identified
ihtimam
ilai
ilawa-lsaac
ilcay
immanuel
imra-n
imran
in
ira-n
isaac
isac
isaiah
isaiah-shacya
ish.a-q
ishaq
ishmael
ishmale
isma'i-l
isma'il
isma'l
ismacil
ismail
ismail.
israq
isreal
issachar
isserlein
istiban
iwad
izrun
ja'bar
ja'far
ja-bir
jabal
jabarah
jabbara
jabir
jabl
jachin
jaco
jacob
jafuda
jair
jala
jalal
jaliyya
jall
jalla
jalladiti
jallla
jam'
jamal
jami'
jamla
janah
janaw
jannab
jannah
janub
janun
januni
japeth
japhet
japheth
japheth.
japtheth
jarir
jariyah
jarra-h.
jawad
jawar
jawhar
jawz
jayan
jayz
jaz
jazala
jazfn
jazrat
jazuliyya
jeaiah
jedidiah
jehiel
jehozadak
jeish
jekuthiel
jenkins
jephthah
jeremiah
jeroham
jeshua
jesse
jethro
jibal
jiqatilia
joel
jonah
jonathan
josef
joseph
joshia
joshiah
joshiahu
joshua
josiah
jospeh
joyya
jubayr
jubran
juda
judaeus
judah
judah.
judayy
juharah
juhda
jumay
jurj
ka-meshorer
kabbaza
kabra
kacb
kafarmand
kahla
kahna
kalaf
kalam
kalfa
kalfun
kalid
kalila
kall
kalla
kallabus
kallir
kallla
kalluf
kaluf
kamal
kamin
kammu-na
kammuna
kamuk
kamuḫ
kans
karam
karda'
karim
karimah
karlm
karm
karma
karo
karuf
karwina
kaspi
kastanani
kastantani
kathir
kathlr
kathlr.
katr
katt
katzenellenbogen
kawlan
kayr
kayran
kazin
kaṯr
kelaiah
kelll
khad.ir
khalaf
khalaf-allah
khaldun
khalfa
khallaf
khalluf
kharruba-labrat
khaybari
khazariyya
khiran
khudayr
khulaif
khulayf
khulla
khuluf
khurasan
khurasanl
khuzayr
kiba'
kifa'
killis
kimhl
kiyar
kiyat
kohen
kufa
kuku
kulaib
kulayb
kulayf
kulayfa
kulla
kulul
kunpaniya
kuraym
kushik
kuta
kuz
kuzayr
l-'ala
l-'ays
l-'azz
l-'izz
l-afrah
l-badr
l-baha
l-baqa
l-barakat
l-bayan
l-bayyan
l-bisr
l-dahab
l-dayyan
l-fada'il
l-fadail
l-fadl
l-fakr
l-farah
l-faraj
l-fasl
l-fatah
l-fath
l-futuh
l-gana'im
l-gaubar
l-gayt
l-hajjaj
l-hasan
l-hasn
l-hay
l-hay?
l-hayy
l-hujuj
l-husayn
l-ishaq
l-jud
l-kahayr
l-karam
l-karim
l-kayr
l-kaṯr
l-kerem
l-ma'al
l-ma'an
l-ma'ruf
l-mahasin
l-majd
l-makarim
l-mansur
l-maymun
l-mufaddal
l-muhasin
l-mukarim
l-muna
l-munajja
l-murajja
l-murja
l-najm
l-nas
l-nasr
l-qasim
l-qasm
l-rab'
l-rida
l-rida'
l-sa'd
l-saqr
l-sar
l-sayyar
l-surur
l-tafal
l-tahir
l-tana
l-tayyib
l-tina
l-wafa
l-wahs
l-wald
l-yaman
l-yasr
l-zabad
l-zahir
l-zikr
l-zuhd
labban
labra-t.
labrat
labrat.
labwa
ladiq
lahmon
lame'
lapidus
laqis
lavi
layla
leah
lebdi
leon
lesser
levites
liviti
loheshah
lord'
loria
lorqi
luban
lukhtush
lukhtush.
luria
lut.f
ma'a-ni-
ma'al
ma'ali
ma'all
ma'an
ma'ani
ma'aravi
ma'azuzi
ma'mar
ma'ruf
ma'tuq
ma'tuqa
macani
macarra
macarrat
machir
macmar
mad.mu-n
madini
madmun
madn
magrebi
magrib
mah.ru-z
mahalalel
mahall
mahalla
mahalliyya
mahara
mahasin
mahbub
mahd
mahdaw
mahdiah
mahfusah
mahfut
mahfuz
mahfuza
mahmud
mahmur
mahruz
maidservant
maimon
maimon.
maimonides
maimun
maimuni
majan
majd
majjan
majjana
majjani
makarim
makhlu-f
makhluf
makin
makk
makluf
makmur
makn
maksul
malal
malha
maliha
malik
malili
malj
malk
malka
mallal
maluk
mammatl
mammon
manana
manasse
manasseh
manassah
mans.u-r
mansur
mar
mardu-k
marduk
marhab
mari
marir
marjan
maruk
marwa-n
marwan
maryam
marzuq
mas'ud
mas'uda
mas.li-'ah.
mas.liah.
masat
mascud
masgavya
mash
mashiah
mashvah
masiah
maskil
maskl
maslah
maslf
maslfah
masli'ah
masliah
maslrah
masmud
mason
masq
massat
master
mastuba
matatiya
matbal-mevorakh
matruh
mattan
mattia
mattitya
mattityah
maw'a
mawadda
mawhu-b
mawhub
mawhuba
mawla
mawlat
maymon
maymun
mazal-tobh
mazhir
mazliah
me'ir
me'ulle
mebhasser
mebhorakh
mehulla-l
meir
meiri
melammed
melekh
melihah
meluhah
men'ahem
menahem
menasseh
menuha
merayot
mercada
mercado
mervan
mesayyem
meshulla-m
meshullam
mesorer
mesullam
mevasse-r
mevasser
mevo-ra-kh
mevorak
mevorakh
mevorak 
meyuhas
mi-sha-'e-l
michael
migash
milah
milh
miller
mirfa
miriam
miron
misael
misbah
misgaviya
misgaviya-
misha'el
mishael
miska
mismak
misr
misrat
mitrani
mivhar
mizjal
mizmar
mizrahi
modes
mohel
mona
monson
monsun
monzon
mordecai
mordechai
moses
moses.
moses 
moshe
mosque-munajja
mosseri
mu'alla
mu'ammal
mu'ammar
mu'awiya
mu'izza
mu't
mu'tazzah
mu'tl
mu-sa-
muammal
muba-rak
mubarak
mubaraka
mubarakah
mubashshir
mubayyin
mubhar
mubhhar
mucammar
mucizz
mudalalla
mudallala
mufad.d.al
mufaddal
mufaddat
mufarraj
mufarrij
muffaddal
muflih
mufrij
muh.riz
muh.sin
muhaddab
muhajir
muhammad
muhammed
muhasin
muhra
muhriz
muhsin
muhsina
mukar
mukarim
mukarram
mukhbar
mukhta-r
mukhtar
muktar
mulai
mulat
mulham
mulk
muluk
mumhe
muna
munajja
munajja'
munajja.
munajjas
mundhir
mundir
munjib
munlr
munr
muqaddas
muqaddasiyya
muqbil
murajja
murj
murq
murs
musa
musa-fir
musafir
musall
musallah
musallam
musaykln
musk
musq
mustafa
mutarris
mutasawwiq
mutiya
muvhar
muwadda'
muwaffaq
muwaffaqa
muwaffaqya
muwahib
muwalladah
muwammal
muwwas
muyassar
na'aman
na'im
na'ima
na'm
na'ms
naaman
nabata
nabla
nablla
nadb
nadd
nadiv
nadlv
nadv
nafis
nafls
naflsa
nafs
nafus
nafusi
naghri-la
nagrela
nagrela-sedaqa
nah.ma-n
nah.manides
nahman
nahmani
nahmias
nahrai
nahrawan
nahray
nahrir
nahrlr
nahrr
nahshon
nahum
nairn
naj
naja
naja'
najah
najb
najd
najiya
najiyya
najlb
najm
najma
najmiyya
najum
najzah
namer
namr
namus
nanu
nanu-adonlm
naphtali
naphthali
naqqad
narbonne
nas.r
nasab
nashi
nashon
nasi
nasir
nasir-i-khosraw
nasiyya
nasr
nassabah
nathan
nathan-hiba
nathan-sahlun
nathanel
nathaniel
nathan|abraham
natronai
nawsa
nazar
nazarah
nazru
nedib
nehawendl
nehemiah
nehorai
nehuma
nestorius
nethaneel
nethanel
nethanel-hibat
netira
nimr
nin
nis
nisabun-japheth
nisabur
nisaburi
nisan
nisi
nisin
nisrn
nissan
nissi
nissi-m
nissim
nissin
nissm
nizam
nlsan
noah
nsabur
nu'man
nu-h.
nufay
nufay'
nufayc
nufayc-sherbet
nufus
nuh
nuhaim
nuhaym
nuhman
numayla
numayr
nur
nusair
nusayr
obadiah
obadiah-'abdallah
obadyah
olah
onen
oppido
orah
oro
padawi
palieche
palonba
paltiel
paltoi
pappos
paqudah
parsi
partner
patrica
pe'er
pedath
perah
perah.ya-
perahiah
perahiel
perahya
perdinel
perdonel
perpignan
pesante
petahya
pethahiah
phineas
phinehas
pinhas
pirqoi
portos
posen
posquie'res
preselyte
profiat
psh't
psh't-sugar
ptolemy
qa'
qa'at
qabis
qabisi
qad
qadb
qaddis
qadiyya
qafs
qahal
qahir
qal'at
qal'i
qala'i
qalir
qalon
qalonymos
qalyub
qamhi
qaraite
qardus
qaro
qarqsn
qartana
qasa-sa-
qasama
qasasa
qasim
qassa'
qassab
qassas
qastalan
qataif
qatamf
qatman
qawsiniyya
qayara
qayn
qayoma
qaysar
qaysaran
qayyn
qayyoma
qazir
qazzaz
qda
qimh.i
qimha
qira
qirqisani
qisas
qisqas
qitos
qiyyam
qlda
qoraysh
qorqos
qubha
quds
qumm
qumma
quraish
qurdus
quriel
qurra
qusasā
qusayr
qustantin
quzman-rebekah
r\\.
ra'isah
ra's
ra'sa
rab'
rab'a
rabbana
rabbi
rabbih
rabf
rabi'
rachel
rad
rada
radi
rafa'el
rafael
rah.ma
rah.mami-n
rahab
rahaj
rahamim
rahamlm
rahamm
rahla
rahma
rahman
rahmun
raja
raja'
raja-
rajf
raml
raphael
raqq
rasd
rasda
rashi
rashi-d
rasid
rason
rassas
rav
raw'
rawh
rayh.a-n
rayhan
raymond
rayyisa
razi'el
razn
rebecca
rebekah
rebhi'ah
reina
rephi'ah
res
reuben
reuveni
rhazes
rida
rida'
ridwan
rika
romano
ronda
ros
rosenblatt
rosenthal
rosenzweig
#rosh
row
rum
rumi
rumiyya
runa
rushd
rusr
ruza
s.a-fi-
s.a-li-h.
s.adaqa
s.addiq
s.ado-q
s.adr
s.eda-qa-
s.emah.
sa''al
sa'a-da
sa'ada
sa'ada-ikhtiydr
sa'ada.
sa'adel
sa'adya
sa'adyahu
sa'adāt
sa'd
sa'd'e-l
sa'd'el
sa'd.
sa'da
sa'dan
sa'dlik.
sa'dun
sa'id
sa'ir
sa'luk
sa'sam.
sa'ul
sa'ya
sa'yun
sa-so-n
saadya
sab'
saba
sabah
sabara
sabat
sabazi
sabb
sabbag
sabbat
sabbetay
sabg
sabha
sabikh
sabiq
sabra
sabri
sabyan
sacada
sacadya
sacd
sacid
sada
sadaqa
sadakah
sadda
saddiq
saddya
sadld
sadoq
saduq
saf
safaqus
saffur
safi
safi-samuel
safl
safwan
sagad'el
saghir
saghir.
saghlr
sagis
sagr
sagri
sags
sahayl
sahda
sahib
sahl
sahla-n
sahlan
sahlawaya
sahlawayh
sahq
sahr
sahrashti
said
saida
saif
sajr
sakan
sala-m
sala-ma
saladin
salam
salama
salama.
salamah
salamanti
salamiyya
salhan
salhun
salih
salik
salim
salla.
sallm
sallum
salm
salma
salma-n
salman
salmon
salmu-na
salmun
salom
saloniko
saluman
sam
sam'an
sam'ar
samau'al
samhun
sami
samiyyah
samma'
sammanud
samo'ah
samran
sams
samsam
samsiyya
samson
samsud
samsur
samual
samuel
samuel-mizr
san
sana'
sananr
sanbat
sandar
sanhur
sani'
sanji
sanua'
saqqaf
sar
sarab
saraf
saragossi
sarah
sarfati
sargah
sarif
sariyy
sarjado
sarqaw
sarr
sarra
saruj
saruq
sarur
sarurah
sarwa
sason
sasson
sattara
saul
sawab
sawad
sawda
sayd
saydalan
sayf
sayk
sayraf
sayrafl
sayyar
sayyid
sayyidah
sayyidat
sba'a
se'adah
se'erit
se'eriṯ
seadiah
sedaqa
sedakah
sefad
sefaradi
sefardi
segullat
sekanya
sela
sema'ya
sema'yahu
semah
semarya
semek
senor
sephardi
serid
serira
sevi
sevili
sha'arayim
shacban
sha'ra
sha'ul
sha'ya
sha'ya-
sha-'u-l
sha-hi-n
sha-lo-m
shabat
shabbat
shabbath
shabbazi
shabbetai
shabbetay
shabbethai
shabbethay
shabha.a
shabhethai
shabur
shacya
shah
shahin
shahryar
shalom
sham'a-n
sham'an
shama
shamarya
shapira
shaprut
shaq
she'erith
shearim
shechaniah
shefatya
shekhanya
shekhanya.
shela
shelah
shem
shema'ya
shemaiah
shemaria
shemariah
shemarya
shemarya-joseph
shemaryah
shemimor
shephatiah
sheria
sherira
shesheth
sheshna
shiblun
shiite
shimeah
shoeib
shuyillzh
shu'a
shu'aib
shu'ayb
shu-'a-
siba'
siba-'
sibag
sibyan
sid
sid-al-kul
sidonil
sidra
sighma-r
sighmar
sighmar-lithographs
sighmar.
sigmar
sihab
sijill
sijilmas
sijilmasi
silh
silo
sim'an
sim'on
siman
simeon
simh.a-
simha
simhah
simon
simonson
sippes
siqill
sir.
sira
siraj
sirilio
sirn
sis
sitt
sitt-al-ahl
sitt-al-bait
sitt-al-banin
sitt-al-dar
sitt-al-hana
sitt-al-hasab
sitt-al-husn
sitt-al-karam
sitt-al-khassah
sitt-al-kull
sitt-al-ma'ali
sittan
sitthum
sittuna
sittut
sizana
skandar
slna
sofet
solal
solel
solomon
solomon.
somek
somer
sovi
sraj
srat
su'a
su'aqah
su'ayb
su'ba
su'l
su'ud
subag
subk
sugmar
suhail
suhayl
sujmar
sukkar
sukr
sulaiman
sulayma-n
sulayman
sulh
sulhan
suli
sultan
sumaisa
sumbat
sunaynat
sunina
suqar
sur
surayr
suru-r
surur
surura
sutayt
sutayta
sutur
sutut
suwayd
suwaykiyya
szr
t.a-h-or
t.a-hir
t.a-jin
t.arib
t.o-via
t.odros
ta'lab
tabarl
tabb
tabbon
tabit
tadmur
tadmuri
taghribirdi
taglab
tahert
taherti
tahertl
tahir
tahkemon
tahor
tahr
tahtahl
tailor
taj
tajir
talala
talb
talmd
talmid
talw
talyun
talyun.
tam
tamam
tamar
tamharon
tamim
tamis
tamlm
tamm
tamma-m
tammam
tana
tanaml
tanan
tanhum
tani-m
tanner
tannur
tantan
tarabuls
tarf
tarfon
tarib
tarif
tariq
tarson
tarsun
tashfln
tattar
tawwaz
tayyib
tban
tdhari
thabit
thiqat
thumna
tiban
tibbon
tibran
tilimsan
tinnisi
tinnlsl
tinns
tiqat
tiqava
tiqva
tiqvah
tiqwa
tmran
tobh
tobh-elem
tobhah
tobiah
tobias
toldeano
tolosa
tov
tovia
toviya
toviyahu
toviyya
toviyyahu
trani
trinquetaille
tripolis
tsa
tsevi
tudela
tujjar
tulatuli
tulaytul
tulun
tuns
turayk
turfa
turkiyya
tustar
tustari
ubayd
ubaydallah
ud
udayb
ukht
ukt
ulah
ulla
ulla-
ullah
uman
umar
umayra
umm
ums
upright
uqba
usaq
usaybi'a
usayd
utman
uzza
uzziel
verga
vidal
villarreal
vilon
vives
vizier
wadada
wadi
wadis
waf
wafa
wafa'
wahab
wahb
wahban
wahib
wahish
wakl
walid
warda
wasl
wazi
wazra
weaver
wuhaib
wuhsa
wuhsha
ya'aves.
ya'i-sh
ya'ir
ya'ish
ya'ish-yahya
ya'qu-b
ya'qub
ya'qub.
ya'r
ya's
ya-'i-r
ya-kh-i-n
yabqi
yacqub
yah.ya-
yahboy
yahud
yahudai
yahya
yahya-yihye
yahya.
yair
yakhln
yakhln.
yakin
yalr
yalsh-hay-yim
yaman
yannai
yannay
yaqar
yaqub
yaqut
yasar
yashar
yasn
yaz-dad
yazdad
yazliah
yedayah
yedidyahu
yeduthun
yefet
yefet'
yehalelel
yehi'el
yehiel
yehosedeq
yehosef
yehosua'
yehuda
yehuday
yekuthiel
yequti'el
yeruham
yerusalm
yerusalmi
yerushalmi
yesha'
yeshfi'a
yeshu'a
yeshu'ah
yeshu-'a-
yeshuca
yesiva
yesu'a
yesu‘a
yeter
yiftah
yih.ye-
yihye
yiju
yinon
yis'
yisay
yishaq
yivhar
ylshay
yofi
yohai
yohanan
yom
yomtob
yonah
yoseh
yoseh-hag-gelili
yu-sha'
yu-suf
yukuthiel
yumn
yunus
yusha'
yusuf
zabi
zabqala
zabyan
zacharia
zachariah
zaddiq
zadok
zaffan
zafira
zahir
zaid
zain
zajjaj
zak
zakar
zakari
zakaria
zakariyya
zakarriyya-
zakay
zakkai
zakkar
zakkariyya
zakkay
zakkur
zanar
zaqal
zar'
zarfa
zayd
zayn
zaynab
zebedee
zecharia
zechariah
zedaqah
zedekiah
zedeq
zefanya
zemah
zenji
zephana
zephaniah
zerah
zerubabel
zibrij
zichri
zigdon
zikn
zikr
zikri
zimra
zirari
ziyada
ziza
zoe
zoreph
zrahiyya
ztn
zuba'
zuhayr
zuhra
zulaf
zulaq
zulati
zumaih
zur
zur'ah
zuraiq
zussman
zutfa
");
1;
