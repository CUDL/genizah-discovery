####################################################################################
##Author:       Chris Stokoe
##File:         spellchecker.pm 
##Date:         28/02/13
##Version:      1.0
####################################################################################
package nlp::editdistance;

use strict;
use utf8;

use List::Util qw(min max);

sub new {
        my $class = shift;
        my %defaults =  (vocabulary => {},
			 memory => '1',
			 term => '');
        my $self = {%defaults, @_};
        bless $self, $class;
        return $self;
}
####################################################################################
#sub spellcorrect 
#{
#	my $self = shift;
#    	my $term = lc(shift);
#	my (%candidates) = ();
#	my @chars = split(//, $term);
#	my @alphabet = ('a','b','c','d','e','f','g','h','i','j','k','l','m',
#			'n','o','p','q','r','s','t','u','v','w','x','y','z');
#
#	if ($self->{'memory'} = 1) {
#	    $l = $self->{vocabulary}; 
#	} else {
#	    tie %lexicon, 'MLDBM', $main::config{'index'}."/cache/tf.bin", O_CREAT|O_RDWR, 0640 or die $!;
#	    $l = "lexicon";
#
#	}
#	if(exists ${$l}{$term}) { return($term); }
#	for $x(0..$#chars) {
#    		for $y(0..$#alphabet) {
#			#  Deletion -  del[x,y]
#        		@del = @chars; $del[$x] .= $alphabet[$y];
#        		if(exists ${$l}{(join "", @del)}) {
#            			$candidates{(join "", @del)} += ${$l}{(join "", @del)};
#        		}
#
#       		#  Substitution - sub[x,y]
#        		@subs = @chars; $subs[$x] = $alphabet[$y];
#        		if(exists ${$l}{(join "", @subs)}) {
#           	 		$candidates{(join "", @subs)} += ${l}{(join "", @subs)};
#        		}
#    		}
#
#		#  Insertion - ins[x,y]
#		@ins = @chars; $ins[$x] = "";
#		if(exists ${$l}{(join "", @ins)}) {
#        		$candidates{(join "", @ins)} += ${$l}{(join "", @ins)};;
#    		}
#    		
#		#  Transposition - trans[x,y]
#    		@trans = @chars;
#    		$first = $trans[$x]; $second = $trans[$x+1];
#    		$trans[$x] = $second; $trans[$x+1] = $first;
#    		if(exists ${$l}{(join "", @trans)}) {
#        		$candidates{(join "", @trans)} += ${$l}{(join "", @trans)};
#    		}
#	}
#	if ($main::config{'memory'} >= 1) { untie %lexicon; }
#	if (%candidates) {
#		@sorted = sort { $candidates{$b} <=> $candidates{$a} } keys %candidates;
#                return(@sorted[0]);
#	} else {
#		return($term);
#	}
#}
#######################################################################################################
sub editdistance {

    my $self = shift;	
    my $word1 = shift;
    my $word2 = shift;

    return 0 if $word1 eq $word2;
    my @d;

    my $len1 = length $word1;
    my $len2 = length $word2;

    $d[0][0] = 0;
    for (1 .. $len1) {
	 $d[$_][0] = $_;
	 return $_ if $_!=$len1 && substr($word1,$_) eq substr($word2,$_);
    }
    for (1 .. $len2) {
	$d[0][$_] = $_;
	return $_ if $_!=$len2 && substr($word1,$_) eq substr($word2,$_);
    }
    for my $i (1 .. $len1) {
	my $w1 = substr($word1,$i-1,1);
	for (1 .. $len2) {
	  $d[$i][$_] = _min($d[$i-1][$_]+1, $d[$i][$_-1]+1, $d[$i-1][$_-1]+($w1 eq substr($word2,$_-1,1) ? 0 : 1));
	}
    }
    return $d[$len1][$len2];
}
#########################################################################################
sub sub_edit_distance {

	my $self = shift;
	my $substring = shift;
        my $string = shift;

        my @substring = split //, $substring;
        my @string = split //, $string;

        my @row1 = (0) x (length($substring) + 1);
        for (my $i = 0; $i < length($substring); $i++) {
                my @row2 = ($i+1);
                for (my $j = 0; $j < length($string); $j++) {
                        my $cost = 1;
                        if (@substring[$i] eq @string[$j]) { $cost = 0; }
                        @row2[$j+1] =  min((@row1[$j+1]+1), # deletion
                                           (@row2[$j]+1), #insertion
                                           (@row1[$j]+$cost)); #substitution
                }
                @row1 = @row2;
        }
        my ($result) = min @row1;

        return $result;
}
#################################################################################
sub levenshtein {
    my $self = shift;
    my ($str1, $str2) = @_;
    my @ar1 = split //, $str1;
    my @ar2 = split //, $str2;
 
    my @dist;
    $dist[$_][0] = $_ foreach (0 .. @ar1);
    $dist[0][$_] = $_ foreach (0 .. @ar2);
 
    foreach my $i (1 .. @ar1){
        foreach my $j (1 .. @ar2){
            my $cost = $ar1[$i - 1] eq $ar2[$j - 1] ? 0 : 1;
            $dist[$i][$j] = min(
                        $dist[$i - 1][$j] + 1, 
                        $dist[$i][$j - 1] + 1, 
                        $dist[$i - 1][$j - 1] + $cost );
        }
    }
 
    return $dist[@ar1][@ar2];
}
#################################################################################
1;
