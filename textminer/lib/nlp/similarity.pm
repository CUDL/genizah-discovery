####################################################################################
##Author:       Chris Stokoe
##File:         similarity.pm 
##Date:         21/03/13
##Version:      1.0
####################################################################################
package nlp::similarity;

use strict;
use Data::Dumper;

sub new {
        my $class = shift;
        my %defaults =  ();
	my $self = {%defaults, @_};
        bless $self, $class;
        return $self;
}
####################################################################################
sub cosine  {
	
        my $self = shift;
	my $vector_a = shift;
	my $vector_b = shift;

	my $num = 0;
	my $sum_sqrt_a = 0;
	my $sum_sqrt_b = 0;

	my @values_a = values %{ $vector_a };
	my @values_b = values %{ $vector_b };

	if ((scalar @values_a) > (scalar @values_b)) {
		my $t = $vector_a; $vector_a = $vector_b; $vector_b = $t;
	}
	
	while (my ($key, $value) = each %{ $vector_a }) {
		$num += $value * ($$vector_b{ $key } || 0);
	}

	foreach my $term (@values_a) { $sum_sqrt_a += $term * $term; }
	foreach my $term (@values_b) { $sum_sqrt_b += $term * $term; }

	return ( $num / sqrt( $sum_sqrt_a * $sum_sqrt_b));
}
#####################################################################################
sub dice {
# If needed ? 
}
#####################################################################################
sub jaccard {
# if needed ?
}
1;
