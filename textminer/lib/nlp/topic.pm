####################################################################################
##Author:       Chris Stokoe
##File:         topic.pm 
##Date:         04/05/13
##Version:      1.0
####################################################################################
package nlp::topic;

use strict;
use utf8;
use open qw(:std :utf8);

use Data::Dumper;

sub new {
        my $class = shift;
        my %defaults =  ('topicmap' => '1',
			 'topicmodel' => './',
			);

        my $self = {%defaults, @_};
	$self->{topicmodel_data} = [];

	unless (open(TOPICMAP, $self->{topicmap})) { die "Cant find topicmap file $self->{topicmap}\n" };
        while(chomp(my $line = <TOPICMAP>)) {
		my @fragment = split(/,/,$line);
		$self->{topicmap_data}{@fragment[0]} = [@fragment[1 .. $#fragment]]; 
	} ;
        close(TOPICMAP);
	
#	print Dumper $self->{topicmap_data};	
	unless (open(TOPICMODEL, $self->{topicmodel})) { die "Cant find topicmodel file $self->{topicmodel}\n" };
        while(chomp(my $line = <TOPICMODEL>)) {
        	push($self->{topicmodel_data}, $line);        
        } ;
        close(TOPICMODEL);

#	print Dumper $self->{topicmodel_data};
        bless $self, $class;
        return $self;
}

####################################################################################
sub fetch_topics {

	my $self = shift;
	my $fragment = shift;

	my @return=();
	my $count = 0;
	foreach my $topic (@{$self->{topicmap_data}{$fragment}}) {
                 push @return, {'name' => $self->{topicmodel_data}[$count],
                                'value' =>  sprintf("%.4f", $topic)};
		 $count++;
        }

	return @return;
	
}
#############################################################################################
1;
