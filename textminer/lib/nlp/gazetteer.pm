####################################################################################
##Author:       Chris Stokoe
##File:         gazetteer.pm 
##Date:         04/05/13
##Version:      1.0
####################################################################################
package nlp::gazetteer;

use strict;

use nlp::gazetteer::temporal;
use nlp::gazetteer::occupations qw(%occupations);
use nlp::gazetteer::religions qw (%religions);
use nlp::gazetteer::nationalities qw (%nationalities);
use Lingua::LinkParser;
use Data::Dumper;

my $temporal = nlp::gazetteer::temporal->new('lower' => 800,
                                  'upper' => 1850);

my $parser = new Lingua::LinkParser;        # create the parser

sub new {
        my $class = shift;
	my %defaults =  (stopword  => '1');
	
	my $self = {%defaults, @_};
        bless $self, $class;
        return $self;
}
####################################################################################
sub extractOccupations  {
        my $self = shift;	
        my $text = shift;

	my %result = ();

        $text = _cleanText($text);

	foreach my $token (keys %occupations) {
		next if ($text !~ /\b($token)\b/);
		 my ($term) = split(/\|/, $token);
		$result{$term} =()= $text =~ /\b($token)\b/g;
	}
        return \%result;
}
#####################################################################################
sub extractOccupations2  {
        my $self = shift;
        my $text = shift;

        my %result = ();
	print "$text\n";
        $text = _cleanText($text);
	print "$text\n";
    	my $sentence = $parser->create_sentence($text); # parse the sentence
	print Dumper $sentence;
	if ($sentence->{_num_linkages} >= 1) {
  		my $linkage  = $sentence->linkage(1);        # use the first linkage
  		print $parser->get_diagram($linkage);   
		<STDIN>;
	}
        foreach my $token (keys %occupations) {
                next if ($text !~ /\b($token)\b/);
                 my ($term) = split(/\|/, $token);
                $result{$term} =()= $text =~ /\b($token)\b/g;
        }
        return \%result;
}
#####################################################################################
sub extractReligions {
	my $self = shift;
        my $text = shift;

        my %result = ();
        $text = _cleanText($text);

        foreach my $token (keys %religions) {
                next if ($text !~ /$token/);
		my ($term) = split(/\|/, $token);
                $result{$term} =()= $text =~ /$token/g;
		
        }
        return \%result;
}
#####################################################################################
sub extractNationalities {

    	my $self = shift;
        my $text = shift;

        my %result = ();
	my %boost = ("born" => 1);

        $text = _cleanText($text);
        foreach my $token (keys %nationalities) {
                next if ($text !~ /\b($token)\b/);
		my ($term) = split(/\|/, $token);
                $result{$term} =()= $text =~ /\b($token)\b/g;
        }
        return \%result;
}
#####################################################################################
sub extractGender {

	my $self = shift;
        my $text = shift;

	my %gender = ("male|boy|man|he|hes" => 1,
		      "female|girl|woman|she|shes|her" => 1);

        my %result = ();

        $text = _cleanText($text);
        foreach my $token (keys %gender) {
                next if ($text !~ /\b($token)\b/);
                my ($term) = split(/\|/, $token);
                $result{$term} =()= $text =~ /\b($token)\b/g;
        }
        return \%result;
	
}
#####################################################################################
sub extractLocations {


}
#####################################################################################
sub extractTemporalExpressions {

	my $self = shift;
        my $text = shift;

	#$text = _cleanText($text);
	#print "$text\n";
	my %results = ();
	my ($tagged, $years) = $temporal->tagger($text);
        #my (@signatures) = $tagged =~ /(<TIME.*?<\/TIME>)/g;
        #$sigs{$doc} = \@signatures;
	#print "$tagged\n";
        foreach my $year ( keys % { $years } ) {
              $results{$year} += $years->{$year};
        }
	#print Dumper %results;
	#<STDIN>;
	return \%results;
}
#####################################################################################
sub extractNames {

}
#####################################################################################
sub _cleanText{

	my $text = shift;
	$text = lc($text);
	$text =~ s/—/-/g;
	$text =~ s/al-\n/al-/g;
	$text =~ s/\n/ /g;
        $text =~ s/ al / al-/g;
        $text =~ s/(\w)[\'\‘\˙](\w)/$1$2/g;
        $text =~ s/[^A-Za-z\-]/ /g;
	$text =~ s/- //g;
        $text =~ s/\s-/ /g;
        $text =~ s/\s+/ /g;
        $text =~ s/^\s//g;
        $text =~ s/\s$//g;
	return $text;

}
#####################################################################################
1;
