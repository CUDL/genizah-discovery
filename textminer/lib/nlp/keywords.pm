####################################################################################
##Author:       Chris Stokoe
##File:         keywords.pm 
##Date:         04/05/13
##Version:      1.0
####################################################################################
package nlp::keywords;

use strict;
use utf8;
use open qw(:std :utf8);

use nlp::tokeniser;
use nlp::tfidf;
use nlp::stemmer;

use Data::Dumper;

sub new {
        my $class = shift;
        my %defaults =  ('stopword' => '1',
			 'corpus' => './',
			);

        my $self = {%defaults, @_};
	$self->{'tf'} = nlp::tfidf->new(normalise => 0);
	$self->{'stemmer'} = nlp::stemmer->new();
	$self->{'tokeniser'} = nlp::tokeniser->new('stopwords' => $self->{'stopword'},
                                    		   'general' => $self->{'general-stopwords'},
                                    		   'corpus' =>  $self->{'corpus-stopwords'},
						   'authors' => $self->{'authors-stopwords'});
	$self->{'cache'} = ();
        bless $self, $class;
        return $self;
}

####################################################################################
sub extract_keywords {
	
	my $self = shift;
        my $fragment = shift;
	my $citations = shift;

	my %counts = ();
	my %lemmas = ();
	my %constituents = ();
	my @keywords = ();

        foreach my $doc ( keys %{$citations} ) {
                if (!exists $self->{'cache'}{$doc}) {
                	if ($doc =~ /\.CSV/) {
                        	unless (open(FILE, "$self->{'corpus'}/$doc")) { die "Couldn't open global $self->{'corpus'}/$doc" }
                        	my %frequencies = ();
                        	<FILE>;
                        	while (<FILE>) {
                                	my ($key, $value) = split(/\,/,$_);
                                	next if ($self->{'tokeniser'}->stopword($key));
                                	$frequencies{$key} = $value;
                                	$self->{'cache'}{$doc} = { %frequencies };
                        	}
                	} else {
                        	my $text = "";
                        	unless (open(FILE, "$self->{'corpus'}/$doc")) { die "Couldn't open global $self->{'corpus'}/$doc" }
                        	while (<FILE>) { $text .= $_; }
                        	my @tokens = $self->{'tokeniser'}->terms($text);
                        	my %frequencies = $self->{tf}->tf(@tokens);
                        	$self->{'cache'}{$doc} = { %frequencies };
                	}
                	close(FILE);
		}
		my $max = 0;
                foreach my $term (sort { $self->{'cache'}{$doc}->{$b} <=> $self->{'cache'}{$doc}->{$a} } keys %{ $self->{'cache'}{$doc} }) {
                     if ($max == 0) { $max = $self->{'cache'}{$doc}{$term}; }
                     #next if ($cache{$doc}{$term} <= 2);
                     #$counts{$term} += $cache{$doc}{$term} / $max;
                     $counts{$term} += $self->{'cache'}{$doc}{$term};
                }

        }

	foreach my $key ( keys %counts ) {
                my $lemma = $self->{'stemmer'}->porter($key);
                $lemmas{$lemma} += $counts{$key};
                $constituents{$lemma}{$key} += $counts{$key};
        }
	my $max = 0;
	foreach my $lemma ( sort { $lemmas{$b} <=> $lemmas{$a} } keys %lemmas ) {
                if ($max == 0) { $max = $lemmas{$lemma}; }
                my ($common) = (sort { $constituents{$lemma}{$b} <=> $constituents{$lemma}{$a} } keys % { $constituents{$lemma} })[0];
                my $raw = $lemmas{$lemma};
		$lemmas{$lemma} = sprintf("%.5f", $lemmas{$lemma} / $max);
		push @keywords, {'name' => $common,
				 'raw' => $raw,
			         'value' => $lemmas{$lemma}};
        }
        return @keywords;
}
#############################################################################################
1;
