####################################################################################
##Author:       Chris Stokoe
##File:         ngrams.pm 
##Date:         04/05/13
##Version:      1.0
####################################################################################
package nlp::ngrams;

use strict;
use utf8;
use open qw(:std :utf8);

use nlp::tokeniser;
use nlp::tfidf;
use nlp::stemmer;

use Data::Dumper;

sub new {
        my $class = shift;
        my %defaults =  ('stopword' => '1',
			 'corpus' => './',
			);

        my $self = {%defaults, @_};
	$self->{'tf'} = nlp::tfidf->new(normalise => 0);
	$self->{'stemmer'} = nlp::stemmer->new();
	$self->{'tokeniser'} = nlp::tokeniser->new('stopwords' => $self->{'stopword'},
                                    		   'general' => $self->{'general-stopwords'},
                                    		   'corpus' =>  $self->{'corpus-stopwords'},
						   'authors' => $self->{'authors-stopwords'},
						   'minimum_length' => 0);
	$self->{'cache'} = ();
        bless $self, $class;
        return $self;
}

####################################################################################
sub extract_ngrams {
	
	my $self = shift;
        my $fragment = shift;
	my $citations = shift;
	my $window = shift;

	my %counts = ();
	my @keywords = ();

        foreach my $doc ( keys %{$citations} ) {
                if (!exists $self->{'cache'}{$window}{$doc}) {
                        my $text = "";
			my %frequencies = ();
                        unless (open(FILE, "$self->{'corpus'}/$doc")) { die "Couldn't open global $self->{'corpus'}/$doc" }
                        while (<FILE>) { $text .= $_; }
                        my @tokens = $self->{'tokeniser'}->terms($text);
#			print "$doc\n";
#			print Dumper @tokens;
#			print length(@tokens) . "\n";
			
			for (my $count = 0; $count <= (@tokens - ($window - 1)); $count++) {
			   my $ngram = "";
			   for (my $inner = $count; $inner <= ($count + ($window - 1)); $inner++) {
			    $ngram .= "@tokens[$inner] ";
			   }
				$ngram =~ s/ $//g;
				$frequencies{$ngram}++;
#				print "\t$ngram\n";
			}
                        $self->{'cache'}{$window}{$doc} = { %frequencies };
                	close(FILE);
		}
		my $max = 0;
                foreach my $ngram (sort { $self->{'cache'}{$window}{$doc}->{$b} <=> $self->{'cache'}{$window}{$doc}->{$a} } keys %{ $self->{'cache'}{$window}{$doc} }) {
                     if ($max == 0) { $max = $self->{'cache'}{$window}{$doc}{$ngram}; }
                     #next if ($cache{$doc}{$term} <= 2);
                     #$counts{$term} += $cache{$doc}{$term} / $max;
                     $counts{$ngram} += $self->{'cache'}{$window}{$doc}{$ngram};
                }

        }

	my $max = 0;
	foreach my $ngram ( sort { $counts{$b} <=> $counts{$a} } keys %counts ) {
                if ($max == 0) { $max = $counts{$ngram}; }
                my $raw = $counts{$ngram};
		$counts{$ngram} = sprintf("%.5f", $counts{$ngram} / $max);
		next if $raw <= 1;
		push @keywords, {'name' => $ngram,
				 'raw' => $raw,
			         'value' => $counts{$ngram}};
        }
        return @keywords;
}
#############################################################################################
1;
