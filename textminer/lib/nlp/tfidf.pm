####################################################################################
##Author:       Chris Stokoe
##File:         tokeniser.pm 
##Date:         29/02/13
##Version:      1.0
####################################################################################
package nlp::tfidf;

use Data::Dumper;

sub new {
        my $class = shift;
        my %defaults =  (normalise => 0);
	my $self = {%defaults, @_};
        bless $self, $class;
        return $self;
}
####################################################################################
sub tf  {
	
        my $self = shift;	
        my @terms = @_;
	my $length = scalar(@terms);
	my %tf;
	my %ltf;
	
	foreach $term (@terms) {
		$tf{$term}++;
	}
	if ($self->{normalise} == 1) {
		foreach $key (keys %tf) {
			$ltf{$key} = $tf{$key} / $length;
		}
		return %ltf;
	} else {
        	return %tf;
	}
}
#####################################################################################
sub idf {
# If needed ? 
}
1;
