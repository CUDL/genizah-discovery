####################################################################################
##Author:	Chris Stokoe
##File:		lemmatizer.pm 
##Date:		04/03/13
##Version:	1.0
####################################################################################
package nlp::lemmatizer;

use WordNet::stem;
use WordNet::QueryData;
use WordNet::Similarity::ICFinder;
use Data::Dumper;
use List::Util qw( max );

my $WN = WordNet::QueryData->new;
my $SIM = WordNet::Similarity::ICFinder->new( $WN );
my $WNSTEM = WordNet::stem->new( $WN );

sub new {
	my $class = shift;
	my %defaults = (WN => $WN);
	my $self = {%defaults, @_};
        my $self->{cache} = {};
	bless $self, $class;
        return $self;
}

#####################################################################################
sub shallow_pos {

  #Wraps WordNet::Stem which is actually a most common POS based lemmatiser! 

  my $self=shift;
  my $word=shift;
   
  my @candidates = $WNSTEM->stemWord($word);
  if (@candidates[0] eq "") {
 	return $word;
  } else {
        return @candidates[0];
  }
}
#########################################################################################
sub shallow_frequency {

   #A concordance based frequency lemmatiser derived from the BestStem module! 
   
   my $self = shift;
   my $word = shift;

   return( wantarray? ('', '', 0) : '' ) unless $word =~ /^\w/;
   return $self->{cache}->{$word} unless wantarray or !$self->{cache}->{$word};
		
   my ($best_w, $best_p, $best_fre) = ('', '', undef);
   my %wps;   
   for my $wp_0 ($WN->validForms($word)) {
            $wps{$_} = 1 for ($WN->querySense($wp_0));
   }

   my (%c, $n_sense);
   for (keys %wps) {
       my ($w, $p, $s) = split '#', $_;

       # use num sense instead of fre of part_of_speech to choose form
       $n_sense ||= 1
       if $p eq 'a' or $p eq 'r';
       my $fre = $SIM->getFrequency($_, $p, 'wps') || 0;
       $c{$w}{$p}[0] ++;         # num of part_of_speech senses
       $c{$w}{$p}[1] += $fre;    # fre of part_of_speech across senses
  } 

  if (!$n_sense) {  # use fre of part_of_speech to choose form
    for my $w ( keys %c ) {
      for my $p ( keys %{ $c{$w} }) {
        ($best_w, $best_p, $best_fre) = ($w, $p, $c{$w}{$p}[1])
          if !defined($best_fre) or $c{$w}{$p}[1] > $best_fre;
      }
    }
   }
   else {           # use num of sense of part_of_speech to choose form
     # no fre profile for r and a
     # so when r or a are involved, use num of senses to help choose form
     # get the pos with highest num of senses for each wp
     for my $w (sort keys %c) {
	# do it in this order, ie bias towards early one when have same value
	# more nouns than other forms in language => nouns have lower fre
	# ditto re r and a ?
       for my $p (qw( r a v n )) {
          ($best_w, $best_p, $best_fre) = ($w, $p, $c{$w}{$p}[0])
            if !defined($best_fre) or ($c{$w}{$p} and $c{$w}{$p}[0] > $best_fre);
        }
      }
    }
  if ($best_w eq '') { 
  	$self->{cache}->{$word} = $word;
	return $word;
  } else {
	$self->{cache}->{$word} = $best_w;
  	return wantarray? ($best_w, $best_p, $best_fre) : $best_w;
  }
}
########################################################################################
1;
