####################################################################################
##Author:      	Chris Stokoe 
##File:         bibliography.pm 
##Date:         18/03/13
##Version:      1.0
####################################################################################
package library::bibliography;
use strict;

#Perl Internal Modules
use Data::Dumper;
use DBI;

#Application Specific Implementations
use library::classmark;


my $library = new library::classmark();

sub new {
        my $class = shift;
        my %defaults =  ();
	my $self = {%defaults, @_};
        bless $self, $class;
        return $self;
	
}
####################################################################################
sub associate_jstor_vectors {

	my $self = shift;
        my $title_id = shift;
        my $filename = shift;

	my %citations = ();

        my $dbh = DBI->connect("DBI:mysql:database=$self->{'database'};host=$self->{'server'};port=$self->{'port'}",
                     $self->{'username'}, $self->{'password'},
                     {'RaiseError' => 1});

        my $sql = "SELECT Reference.*, Fragment.* 
                   FROM Reference LEFT JOIN Fragment 
                   ON Reference.Fragment = Fragment.ID 
                   where Reference.Title = \"$title_id\"";

        my $references = $dbh->prepare($sql);
        $references->execute();

        while (my $r = $references->fetchrow_hashref()) {
                 my $fragment = $library->classmark($r->{'LB'});
                 $citations{$fragment}{$filename}++;
        }
	
	$dbh->disconnect;
	return \%citations;
}
#####################################################################################
sub associate_manual {

        my $self = shift;
	my $filename = shift;
        my $mapping = shift;
        my $offset = shift;
        my $limit = shift;

        my %citations = ();

	unless (open(FILE, $mapping)) { die "Couldn't open $mapping"; }
        while (<FILE>) {
                chomp($_);
                my ($fragment,$pages) = split(/\t/, $_);
                $fragment = $library->classmark($fragment);
                my (@p) = split(/\,/, $pages);
                foreach my $p (@p) {
                        $p =~ s/n\..*//g;
                        $p =~ s/\s//g;
                        next if $p eq "";
                        if ($p =~ /-|\//) {
                                my ($first, $last) = split(/\/|-/, $p);
                                my $window = length($first) - length($last);
                                $first =~ m/(.{$window})/;
                                $last = $1.$last;
                                my @pages = ($first..$last);
                                foreach my $page (@pages) {
                                        next if ($page > $limit);
                                        $page += $offset;
                                        my $ref = $filename."_".$page.".txt";
                                        $citations{$fragment}{$ref}++;
                                }
                        } else {
                                next if ($p > $limit);
				next if ($p eq "NULL");
                                $p += $offset;
                                my $ref = $filename."_".$p.".txt";
                                $citations{$fragment}{$ref}++;
                        }
                }
        }
	return \%citations;
}
####################################################################################
sub associate_auto {

	my $self = shift;
	my $filename = shift;
        my $title_id = shift;
        my $offset = shift;
	my $drop = shift;

	my %citations = ();

        my $dbh = DBI->connect("DBI:mysql:database=$self->{'database'};host=$self->{'server'};port=$self->{'port'}",
                     $self->{'username'}, $self->{'password'},
                     {'RaiseError' => 1});

        my $sql = "SELECT Reference.*, Fragment.* 
                   FROM Reference LEFT JOIN Fragment 
                   ON Reference.Fragment = Fragment.ID 
                   where Reference.Title = \"$title_id\"";

        my $references = $dbh->prepare($sql);
        $references->execute();

        while (my $r = $references->fetchrow_hashref()) {
                 my $fragment = $library->classmark($r->{'LB'});
                 my @pages = split(/\,/,$r->{'C6'});
                 foreach my $p (@pages) {
			$p =~ s/\s//g;
                        $p =~ s/n\..*//g;
			next if (($drop) && ($p =~ /index/));
                        if ($p =~ /\-/) {
                                my ($first, $last) = split(/\/|-/, $p);
                                my $window = length($first) - length($last);
                                $first =~ m/(.{$window})/;
                                $last = $1.$last;
                                my @pages = ($first..$last);
                                foreach my $page (@pages) {
                                        $page += $offset;
                                        my $ref = $filename."_".$page.".txt";
                                        $citations{$fragment}{$ref}++;                     
                        	} 
                        } else {
                                $p += $offset;
                                my $ref = $filename."_".$p.".txt";
                                $citations{$fragment}{$ref}++;   
                        }
                 }      
        }
	$dbh->disconnect;
	return \%citations;
}
#####################################################################################
1;
