#!/usr/bin/env perl
####################################################################
##Author:       Chris Stokoe
##File:         textminer.pl 
##Date:         19/05/13
##Version:      1.0
##
## Associates fragments with texts from the collection and perform
## information extraction to produce metadata
####################################################################
#Perl Internal Modules
use strict;
use utf8;
use encoding 'utf8';

#CPAN Venilla
use MLDBM qw(DB_File Storable);
use Fcntl qw(O_RDWR O_CREAT);
use Data::Dumper;
use XML::Simple;
use FindBin qw($Bin); 
use lib "$Bin/../lib";
#Application Librairies
use library::bibliography;
use nlp::keywords;
use nlp::ngrams;
use nlp::features;
use nlp::timex;
use nlp::topic;

#####################################################################
#Global Variables#
##################

if ( scalar( @ARGV ) < 1 ) { die( "No config file specified\n" ); }
my $config = XMLin(@ARGV[0]) || die ("Couldn't parse config file)\n" );
my %documents = ();
tie my %citations, 'MLDBM', "$config->{output}/mappings.bin", O_CREAT|O_RDWR, 0640 or die $!;
###################################################################
sub bibliographic_mappings {

	my $bibliography = library::bibliography->new('server' => $config->{'server'},
						      'port' => $config->{'port'},
						      'username' => $config->{'username'},
						      'password' => $config->{'password'},
						      'database' => $config->{'database'});	

	foreach my $item ( @{ $config->{scholarly_works}{item} }) {
		my $mappings = "";
		if ($item->{'processor'} eq "manual") { 
		   	$mappings = $bibliography->associate_manual($item->{shortname}, 
								    $item->{mapping}, 
								    $item->{pageoffset}, 
								    9999);
		}
		if ($item->{'processor'} eq "auto") { 
		    $mappings = $bibliography->associate_auto($item->{shortname}, 
						 	     $item->{mapping}, 
						             $item->{pageoffset},
							     $item->{dropindex});
		}
		foreach my $fragment ( keys %{ $mappings } ) {
			my $pointer = $citations{$fragment};
			foreach my $citation ( keys %{ $mappings->{$fragment} }) {
				$pointer->{$citation}++;
			}
			$citations{$fragment} = $pointer;
		}
	} 	
}
######################################################################
my $keywords = nlp::keywords->new('corpus' => $config->{'corpus'},
				  'general-stopwords' => $config->{'general-stopwords'},
                                  'corpus-stopwords' => $config->{'corpus-stopwords'},
				  'authors-stopwords' => $config->{'authors-stopwords'});

my $ngrams = nlp::ngrams->new('corpus' => $config->{'corpus'},
                                  'general-stopwords' => $config->{'general-stopwords'},
                                  'corpus-stopwords' => $config->{'corpus-stopwords'},
                                  'authors-stopwords' => $config->{'authors-stopwords'});

my $features = nlp::features->new('corpus' =>  $config->{'corpus'},
                                  'authors-stopwords' => $config->{'authors-stopwords'});

my $temporal = nlp::timex->new('corpus' => $config->{'corpus'});

my $topics = nlp::topic->new('topicmap' => $config->{'topicmap'},
			   'topicmodel' => $config->{'topicmodel'});

#open STDERR, '>/dev/null';
my $running = 0;
bibliographic_mappings();
#print Dumper %citations;
my $debug = 0;
if (!$debug) {
foreach my $fragment (keys %citations) { 
	my %document = ();
	$running++;
	print "$running Creating document representation for $fragment\n";

	my @keywords = $keywords->extract_keywords($fragment, $citations{$fragment});
	my @bigrams = $ngrams->extract_ngrams($fragment, $citations{$fragment}, 2);
	my @trigrams = $ngrams->extract_ngrams($fragment, $citations{$fragment}, 3);
	my @qgrams = $ngrams->extract_ngrams($fragment, $citations{$fragment}, 4);
	my (%features) = $features->extract_features($fragment, $citations{$fragment});
	my @timex = $temporal->extract_temporal_references($fragment,  $citations{$fragment});	
	my @topics = $topics->fetch_topics($fragment);
	my $dec = '<?xml version="1.0" encoding="utf-8"?>';
	my $document = {'ID' => $fragment,
			'KEYWORDS' => {KEYWORD => \@keywords},
			'BIGRAMS' => {BIGRAM => \@bigrams},
			'TRIGRAMS' => {TRIGRAM => \@trigrams},
			'QUADGRAMS' => {QUADGRAM => \@qgrams},
			'LOCATIONS' => {LOCATION =>  \@{$features{'LOCATIONS'}}},
			'PEOPLE' => {PERSON => \@{$features{'NAMES'}}},
			'TEMPORAL' => {TIMEX => \@timex},
			'TOPICS' => {TOPIC => \@topics},
			'CITATIONS' => \%{$citations{$fragment}}};
	my $file = "$config->{output}/fragments/$fragment.xml";
	XMLout($document, XMLDecl => $dec, RootName => 'DOCUMENT', KeyAttr => [ ], OutputFile => $file);
}

}
print Dumper %citations;
untie %citations;
