#!/usr/local/bin/perl
####################################################################################
##Author:       Chris Stokoe
##File:         indexer.pl 
##Date:         28/02/13
##Version:      1.0
## 
####################################################################################

#use strict;
use DBI;
use MLDBM qw(DB_File Storable);
use Fcntl qw(O_RDWR O_CREAT);
use FindBin qw($Bin);
use lib "$Bin/../lib";
use nlp::stemmer;
use Data::Dumper;
use Devel::Size qw(total_size);
use Term::ProgressBar;
use XML::Simple;

####################################################################################
#global Variables#
##################
my $lemmatizer = nlp::stemmer->new();
my %index = ();
my $directory = "../../metadata";

####################################################################################
sub documentVectors {
	my $c = 0;

	#Open df
	tie my %df, 'MLDBM', "$directory/df.bin", O_CREAT|O_RDWR, 0640 or die $!;

	#Open tfidf bin
	tie my %tfidf, 'MLDBM', "$directory/tfidf.bin", O_CREAT|O_RDWR, 0640 or die $!;

	#open squares
	tie my %squares, 'MLDBM', "$directory/squares.bin", O_CREAT|O_RDWR, 0640 or die $!;

	#get document tf vectors
        tie my %documents, 'MLDBM', "$directory/documents.bin", O_CREAT|O_RDWR, 0640 or die $!;
	my $csize = keys %documents;
        
	my $progress = Term::ProgressBar->new({name => 'I/O write',
                                              count => $csize,
                                              ETA => linear});

	foreach my $doc (keys %documents) {
		my %document_vector;
		my $sum_sqrt = 0;
		my $docref = $documents{$doc};
		foreach my $term (keys %{ $docref }) {
			my $tf = $docref->{$term};
			my $idf = log($csize/$df{$term})/log(10);
			my $tfidf = sprintf("%.5f", $tf * $idf);
			 $document_vector{$term} = $tfidf;
			$sum_sqrt += $tfidf * $tfidf;	
#		print "$doc ---> $term ---> $tf ----> $df{$term} ---> $csize ---> $idf ----> $tfidf\n";
			
		}
		$squares{$doc} = $sum_sqrt;
		$tfidf{$doc} = \%document_vector;
		$progress->update($c++);
	}
        untie %documents;
	untie %tfidf;
	untie %squares;
	untie %df;
}
####################################################################################
sub dumpBins {

	print "Dumping bins to disk\n";
	my $count = keys %index;
	$count += keys %{$index{'postings'}};

	my $c = 0;
	my $progress = Term::ProgressBar->new({name => 'I/O write',
					      count => $count,
					      ETA => linear});
	
	#merge df counts
	tie my %df, 'MLDBM', "$directory/df.bin", O_CREAT|O_RDWR, 0640 or die $!;
	foreach my $ref (keys %{$index{'df'}}) { $df{$ref} += $index{'df'}->{$ref}; }
	untie %df;
	$progress->update($c++);

	#Add new document vectors
	tie my %documents, 'MLDBM', "$directory/documents.bin", O_CREAT|O_RDWR, 0640 or die $!;
	foreach $doc (keys %{$index{'documents'}}) { $documents{$doc} = $index{'documents'}->{$doc}; }
	untie %documents;
	$progress->update($c++);

	#Add new document locations
        tie my %locations, 'MLDBM', "$directory/locations.bin", O_CREAT|O_RDWR, 0640 or die $!;
        foreach $doc (keys %{$index{'locations'}}) { $locations{$doc} = $index{'locations'}->{$doc}; }
        untie %locations;
        $progress->update($c++);

	#Add new document timex's
        tie my %dates, 'MLDBM', "$directory/temporal.bin", O_CREAT|O_RDWR, 0640 or die $!;
        foreach $doc (keys %{$index{'timex'}}) { $dates{$doc} = $index{'timex'}->{$doc}; }
        untie %dates;
        $progress->update($c++);

	#Add new document names
        tie my %names, 'MLDBM', "$directory/names.bin", O_CREAT|O_RDWR, 0640 or die $!;
        foreach $doc (keys %{$index{'people'}}) { $names{$doc} = $index{'people'}->{$doc}; }
        untie %names;
        $progress->update($c++);

	#topics
	tie my %topics, 'MLDBM', "$directory/topics.bin", O_CREAT|O_RDWR, 0640 or die $!;
        foreach $doc (keys %{$index{'topics'}}) { $topics{$doc} = $index{'topics'}->{$doc}; }
        untie %topics;
        $progress->update($c++);

	#Add new term vectors
	tie my %postings, 'MLDBM', "$directory/postings.bin", O_CREAT|O_RDWR, 0640 or die $!;
	foreach my $term (keys %{$index{'postings'}}) {
		my $t = $postings{$term};
		foreach my $doc (keys %{$index{'postings'}->{$term}}) {
			$t->{$doc} = $index{'postings'}->{$term}->{$doc};
		}
		$postings{$term} = $t;
		$progress->update($c++);
	}
	untie %postings;
	$progress->update($c++);
	$progress->update($c++);
	%index = ();
}
#######################################################################################
#main#

opendir( DATA_DIR, $ARGV[0] ) || die "Cannot open $ARGV[0]\n";
while ( my $name = readdir(DATA_DIR) ) {
        next if ( $name eq '.' or $name eq '..' );
        push(@files, $name);
}

my $max = scalar(@files);
my $counter = 1;
foreach $file (@files) {
   
   print "Indexing $file $counter of $max\n";
   my $document_length = 0;
   my %posting_list;
   my %seen = ();

   #Read in XML File   
   my $doc = XMLin("@ARGV[0]/$file", ForceArray=>1,  KeyAttr => [ ]) || die ("Couldn't parse document file)\n" );
   my ($id) = $file =~ /(.*)\.xml/;
   
   #Build posting list
   foreach my $keywords (  @{$doc->{KEYWORDS}}) {
	foreach my $keyword ( @{$keywords->{KEYWORD}}) {
		my $term = $lemmatizer->porter($keyword->{'name'});
#		print "$keyword->{'name'}\n";
       		$index{'postings'}->{$term}->{$id} += $keyword->{'value'};
		$index{'documents'}->{$id}->{$term} += $keyword->{'value'};
		$seen{$term}++;
	}
   }
   
   #Update Document Frequency Table
   foreach my $t ( keys %seen ) {
   	$index{'df'}->{$t}++;
   }

   #Index Locations
   my %places = ();
   foreach my $locations ( @{$doc->{LOCATIONS}} ) {
	foreach my $location ( @{$locations->{LOCATION}} ) {
		next if $location->{'raw'} <= 2;
		$index{'locations'}->{$id}->{$location->{'name'}} = {'centroid' => $location->{'centroid'},
								     'type' => $location->{'type'},
								     'county' => $location->{'country'},
								     'weight' => $location->{'weight'}}; 
	}
   }
   
   #Index Dates
   foreach my $dates ( @{$doc->{TEMPORAL}} ) {
	foreach my $date ( @{$dates->{TIMEX}} ) {
		$index{'timex'}->{$id}->{$date->{'year'}} = $date->{'value'};
	}
   }	

   #Index Names
   foreach my $people (  @{$doc->{PEOPLE}}) {
           foreach my $person ( @{$people->{PERSON}} ) {
	   	next if $person->{'raw'} <= 2;
		next if $person->{'weight'} <= 0.25;
		$index{'people'}->{$id}->{$person->{'name'}} = $person->{'weight'};
           }
   }

   #topics
   foreach my $topics (  @{$doc->{TOPICS}}) {
           foreach my $topic ( @{$topics->{TOPIC}} ) {
                next if $topic->{'value'} < 0.05;
		next if $topic->{'value'} eq "nan";
                $index{'topics'}->{$id}->{$topic->{'name'}} = $topic->{'value'};
           }
   }

   #Serialise to disk
   my $mem = total_size(\%index);
   if ($mem >= 52428800) { dumpBins(); }
   $counter++;
}
dumpBins();
documentVectors();
