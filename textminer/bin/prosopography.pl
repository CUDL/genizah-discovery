#!/usr/local/bin/perl
####################################################################################
##Author:       Chris Stokoe
##File:         times.pl 
##Date:         16/04/13
##Version:      1.0
##
## Extract temporal references and associate them with fragments
## 
####################################################################################
use strict;
use utf8;

use FindBin qw($Bin);
use lib "$Bin/../lib";
use MLDBM qw(DB_File Storable);
use Fcntl qw(O_RDWR O_CREAT O_RDONLY);
use Lingua::EN::Sentence qw( get_sentences add_acronyms );
use Lingua::Sentence;
use nlp::tfidf;
use nlp::tokeniser;
use nlp::gazetteer::occupations qw(%occupations);
use nlp::gazetteer::religions qw (%religions);
use nlp::gazetteer::nationalities qw (%nationalities);
use nlp::gazetteer::names qw ( %names );
use nlp::gazetteer::authors qw ( %authors );
use nlp::gazetteer::locations qw ( %locations );
use nlp::gazetteer::temporal;
use Text::Unidecode;
use encoding 'utf8';

use Data::Dumper;

my $temporal = nlp::gazetteer::temporal->new('lower' => 800,
                                                   'upper' => 1700);

my $tokeniser = nlp::tokeniser->new();

my $splitter = Lingua::Sentence->new("en");

my %cache = ();
my @files = ();
my $dir = "../../corpus/text/";
opendir( DATA_DIR, $dir ) || die "Cannot open $ARGV[0]\n";
       while ( my $name = readdir(DATA_DIR) ) {
       next if ( $name eq '.' or $name eq '..' );
       push(@files, $name);
}

####################################################################################
#global Variables#
##################
my @names;

my %pnames = ();
my $tf = nlp::tfidf->new(normalise => 0);
my %cache = ();

################################################################################
sub get_names { 
	
	my %joins = ();
	tie my %names, 'MLDBM', "../../metadata/names.bin", O_RDWR, 0640 or die $!;
	foreach my $fragment (keys %names) {
		my $handle = $names{$fragment};
		foreach my $name (keys %{$handle} ) {
			$pnames{$name}++;
		}
	}

	foreach my $outer ( sort { length($a) <=> length($b) } keys %pnames ) {
		print "$outer \n";
		foreach my $inner ( sort { $pnames{$b} <=> $pnames{$a} } keys %pnames ) {
			next if $outer eq $inner;
			if ($inner =~ m/^$outer/){ 
				print "Left Join Candidate: $outer => $inner \n";
				$joins{$outer} = $inner;
				foreach my $join (keys %joins) {
					if ($joins{$join} eq $outer) {
						$joins{$join} = $inner;
					}
				}
				last;
			}
			if (($outer =~ m/^B\./i) && ($inner =~ m/(\b.*?\b$outer)$/i)) {
				print "Right Join Candidate: $outer => $1 \n";
				$joins{$outer} = $1;
                                foreach my $join (keys %joins) {
                                        if ($joins{$join} eq $outer) {
                                                $joins{$join} = $1;
                                        }
                                }
                                last;	
			}

		}
	}

	foreach my $fragment (keys %names) {
                my $handle = $names{$fragment};
                foreach my $name (keys %{$handle} ) {
                        if ($joins{$name}) {
				$handle->{$joins{$name}} += $handle->{$name};
				delete $handle->{$name};
			}
                }
		$names{$fragment} = $handle;
        }

	untie %names;
}
################################################################################
sub build_profile {
	
	my $name = shift;
	my %vectors = ();

	$name = lc($name);
	$name =~ s/^[A-Za-z]\.//g;
	$name =~ s/^[A-Za-z]\.\s[A-Za-z]\.//g;
	$name =~ s/^[A-Za-z] //g;
        $name =~ s/( b\. | ibn | abu | abu \'l | bat | bet)/ /g;
	$name =~ s/\s[A-Za-z0-9]\.\s/ /g;
	$name =~ s/\s[A-Za-z0-9]\s/ /g; 
        $name =~ s/\s+/ /g;
        $name =~ s/^\s//g;
        $name =~ s/\s$//g;
        my @names = split(/\s/, $name);
	my $match1 = "";
	my $match2 = "";
	if (scalar @names <= 1) {
		$match1 = " @names[0] ";
		$match2 = " @names[0] ";
	} else {
		my $m1 = join ".*", @names[0..1];
        	my $m2 = join ".*", @names[0..2];
        	my $m3 = "@names[0].*@names[-1]";
        	my $m4 = "@names[0].*";
        	my $m5 = "@names[-2].*@names[-1]";
		$match1 = "($m1|$m2|$m3|$m5)";
		$match2 = "($m1|$m2|$m3|$m4|$m5)";
	}
	my $filecount = 1;
	my $sentence_count = 1;
	foreach my $file (@files) {
	     my $count = 1;
	     if (!exists $cache{$file}) { 
             		my $text = "";
             		unless (open(FILE, "$dir/$file")) { die "Couldn't open global $ARGV[0]/$file" }
			binmode FILE, ':encoding(utf8)';
			{
   				local $/;
    				$text = <FILE>;
			}
			close(FILE);
	     		$text = $tokeniser->clean($text);
			$cache{$file}{case} = $text;
			$cache{$file}{lower} = lc($text);
			#my @sentences = split(/\n/, $splitter->split(lc($text)));
			my @sentences = split(/\s.\s/, lc($text));
			$cache{$file}{sentences} = \@sentences;
	     }
	     next unless ($cache{$file}{lower} =~ /$match1/);
	     $filecount++;
	   #  my $sentences=get_sentences($cache{$file});
	    foreach my $sentence (@ { $cache{$file}{sentences} } ) {
#	     foreach my $sentence (@$sentences) {

		 next unless ($sentence =~ /$match1/);
		#my $result = extract_features($sentence);
		my $result = extract_features($cache{$file}{lower});
		 # if ($name =~ /'awkal/) {
                 #       print "$sentence\n"; <STDIN>;
		#	print Dumper $result;
                 #}

		# print "$sentence";
		# print Dumper $result->{'occupation'};
		 #<STDIN>;
		 foreach my $year (keys %{ $result->{'timex'} }) { $vectors{'timex'}{$year} += $result->{'timex'}{$year}; }
		 foreach my $gender (keys %{ $result->{'gender'} }) { $vectors{'gender'}{$gender} += $result->{'gender'}{$gender}; }
		 foreach my $occupation (keys %{ $result->{'occupation'} }) { $vectors{'occupation'}{$occupation} += $result->{'occupation'}{$occupation}; }
		 foreach my $religion (keys %{ $result->{'religion'} }) { $vectors{'religion'}{$religion} += $result->{'religion'}{$religion}; }
		 foreach my $nationality (keys %{ $result->{'nationality'} }) { $vectors{'nationality'}{$nationality} += $result->{'nationality'}{$nationality}; }		 
	     	$sentence_count++;
		}		
 
	}
	
	print "\t Searched $filecount files and $sentence_count sentences\n";
	return \%vectors;
}
################################################################################
sub extract_features {
	
	my $text = shift;
	my %result = ();
	
	my %gender = ("male|boy|man|he|him|his" => 1,
                      "female|girl|woman|she|her|sitt|umm" => 1);
	
	$text =~ s/[^A-Za-z0-9\-\']/ /g;
	#Extract Gendar

	my @tokens = split(/\s/, $text);
	
	foreach my $token (@tokens) {
				
		next unless exists $occupations{$token};
		$result{'occupation'}{$token}++;

	}

	foreach my $token (keys %gender) {
                next if ($text !~ /\b($token)\b/);
                my ($term) = split(/\|/, $token);
                $result{'gender'}{$term} =()= $text =~ /\b($token)\b/g;
        }

	#Extract Occupations
	#foreach my $token (keys %occupations) {
        #        next if ($text !~ /\b($token)\b/);
        #         my ($term) = split(/\|/, $token);
        #        $result{'occupation'}{$term} =()= $text =~ /\b($token)\b/g;
        #}
	
	#Extract Religions
	# foreach my $token (keys %religions) {
        #        next if ($text !~ /\b($token)\b/);
        #        my ($term) = split(/\|/, $token);
        #        $result{'religion'}{$term} =()= $text =~ /\b($token)\b/g;
        #
        #}
	
	#Nationality
	#foreach my $token (keys %nationalities) {
        #        next if ($text !~ /\b($token)\b/);
        #        my ($term) = split(/\|/, $token);
        #        $result{'nationality'}{$term} =()= $text =~ /\b($token)\b/g;
        #}

	#timex
        my ($tagged, $years) = $temporal->tagger($text);
        foreach my $year ( keys % { $years } ) {
              $result{'timex'}{$year} += $years->{$year};
        }
	
        return \%result;
}
################################################################################
sub build_prosopography {

	tie my %names, 'MLDBM', "../../metadata/names.bin", O_RDONLY, 0640 or die $!;
	tie my %pro, 'MLDBM', "../../metadata/pro.bin", O_CREAT|O_RDWR, 0640 or die $!;
	tie my %coocurence, 'MLDBM', "../../metadata/neighbours.bin", O_CREAT|O_RDWR, 0640 or die $!;
	my $count = 0;
	my %seen = ();
        foreach my $fragment (keys %names) {
		$count++;
		my $time = localtime;
		print "processing fragment number $count time $time\n";
                my $handle = $names{$fragment};
			
		#build sparse co-occurence matrix
                foreach my $outer (keys %{$handle} ) {
			my $matrix = $coocurence{$outer};
			#build sparse co-occurence matrix
			foreach my $inner (keys %{$handle}) {
				$matrix->{$inner}++;
			}
			
			$coocurence{$outer} = $matrix;
			#build pro-sopograhy entry
			next if exists $seen{$outer};
			my $profile = build_profile($outer);
			
			my %person = ();
			my $o = join ",",(sort { $profile->{'occupation'}{$b} <=> $profile->{'occupation'}{$a} } keys % {$profile->{'occupation'} })[0..2];

			if ($o eq "") { $o = "Unknown"; }
			$o = join " , ", map {ucfirst lc} split ",", $o;
			$o =~ s/(\,+?)$//g;
		        $person{'occupation'} = $o;
			
			my $g = (sort { $profile->{gender}{$b} <=> $profile->{gender}{$a} } keys % { $profile->{'gender'} } )[0];
			if ($g eq "") { $g = "male"; }
			$person{'gender'} = $g;

			my $r = (sort { $profile->{'religeon'}{$b} <=> $profile->{'religeon'}{$a} } keys % { $profile->{'religion'} } )[0];
			if ($r eq "") { $r = "Unknown"; }
                        $person{'religion'} = $r;

			my %century = ();
			foreach my $year (keys % { $profile->{'timex'} } ) {
				my $period = "";
				if (length($year) == 3) { ($period) = $year =~ m/(.)/; }
                                if (length($year) == 4) { ($period) = $year =~ m/(..)/; }
				$period += 1;
				$century{$period} += $profile->{'timex'}{$year};
			}

			my $y = (sort { $century{$b} <=> $century{$a} } keys %century )[0];
			if ($y) { 
                            $person{'period'} = $y."th Century";
                        } else {
                            $person{'period'} = "Unknown";
                        }


			#my $y = (sort { $profile->{'timex'}{$b} <=> $profile->{'timex'}{$a} } keys % { $profile->{'timex'} } )[0];
			#	 my $period = "";
			#	 if (length($y) == 3) { ($period) = $y =~ m/(.)/; }
                        #	 if (length($y) == 4) { ($period) = $y =~ m/(..)/; }
                         #        if ($period) { 
			#		$period += 1;
			#		$person{'period'} = $period."th Century";
			#	 } else {
			#	 	$person{'period'} = "Unknown";
			#	 }

			$seen{$outer}++;
			$pro{$outer} = \%person;
			print "$outer\n";
			print Dumper %person;
		}
        }
	
	untie %names;
	untie %pro;
	untie %coocurence;
}
################################################################################
get_names();
build_prosopography();
#my $profile = build_profile(@ARGV);
#print "Occupations: ";
#my $o = join ",",(sort { $profile->{'occupation'}{$b} <=> $profile->{'occupation'}{$a} } keys % {$profile->{'occupation'} })[0..3];
#print "$o \n";
#print "Gender: ";
#my $g = (sort { $profile->{gender}{$b} <=> $profile->{gender}{$a} } keys % { $profile->{'gender'} } )[0];
#print "$g \n";
#print Dumper $profile->{'occupation'};




