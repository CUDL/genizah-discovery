var parse = d3.time.format("%Y-%m-%d %H:%M").parse,
    format = d3.time.format("%Y");

var svg, area, line, gradient, xAxis, yAxis;
var color = d3.scale.category20();
var Y_val;

function getDate(d) {
	if (typeof(data[d]) !== 'undefined' ||  data[d] != null) {
		return new Date(d.year); 
	} 

}

function drawGraph (container, w, h) {
    var m = [5, 15, 20, 35],
    w = w - m[1] - m[3],
    h = h - m[0] - m[2];

    var x = d3.time.scale()
//		.domain([new Date(600,1,1), new Date(1890,1,1)])
		.range([0, w]);


    var y = d3.scale.linear().range([h, 0]);

    xAxis = d3.svg.axis().scale(x).orient("bottom").ticks(d3.time.years, 100).tickFormat(d3.time.format("%Y")),
    yAxis = d3.svg.axis().scale(y).orient("left");

 
    svg = d3.select(container).append("svg:svg")
        .attr("width", w + m[1] + m[3])
        .attr("height", h + m[0] + m[2])
        .append("svg:g")
        .attr("transform", "translate(" + m[3] + "," + m[0] + ")");

    svg.append("svg:clipPath")
        .attr("id", "clip")
        .append("svg:rect")
        .attr("x", x(0))
        .attr("y", y(1))
        .attr("width", x(1) - x(0))
        .attr("height", y(0) - y(1));

    svg.append("svg:g")
        .attr("class", "y axis")
        .attr("transform", "translate(0,0)");

    svg.append("svg:g")
        .attr("class", "x axis")
	.call(xAxis)
        .attr("transform", "translate(0," + h + ")");


    svg.append("svg:path")
        .attr("class", "line")
        .style("stroke", "blue")
        .attr("clip-path", "url(#clip)");

    var rect = svg.append("svg:rect")
        .attr("class", "pane")
        .attr("width", w)
        .attr("height", h);

 
    line = d3.svg.line()
        .interpolate("step-before")
        // .interpolate("linear")
        .x(function(d) { return x(d.year); })
        .y(function(d) { return y(d.weight); });

    x.domain(d3.extent(data.map(function(d) { return d.year; })));
    ymin = d3.min(data.map(function(d) { return d.weight; }));
    ymax = d3.max(data.map(function(d) { return d.weight; }));
    ymargin = (ymax - ymin)/50;
    y.domain([ymin-ymargin, ymax+ymargin]);

    var curve1 = svg.select("path.line").data([data]);

    var guidline = svg.append("line")
        .attr("class", "guideline")
        .attr("y1", 0)
        .attr("y2", h)
        .attr("opacity", 0)
        .attr("stroke", "red")
        .attr("pointer-events", "none");

    var title = svg.append("svg:text")
        .attr("class", "title")
        .attr("x", w - 130)
        .attr("y", 10)
        .attr("dy", ".71em")
	.attr("opacity", 0)
        .text(2000);

    var message = svg.append("svg:text")
	.attr("class", "title")
	.attr("x", (w / 2) - 150)
	.attr("y", (h / 2) - 10) 
	.attr("dy", ".71em")
	.attr("opacity", 0)
	.text("No Temporal Data")

if (typeof(data[0]) !== 'undefined' ||  data[0] != null) {
 rect.on("mousemove", function() {
          var X_px = d3.mouse(this)[0]+1,
          X_date = x.invert(X_px);
          var Y_px = y(Y_val);
          var pathData = curve1.data()[0]; 

          pathData.forEach(function(element, index, array) {
             if ((index+1 < array.length) && (array[index].year <= X_date) && (array[index+1].year >= X_date)) {
                 if (X_date-array[index].year < array[index+1].date-X_date)  Y_val = array[index].weight;
                    else Y_val = array[index+1].weight;
                 }
             });
 
      guidline
        .attr("opacity", 1)
        .attr("x1", d3.mouse(this)[0])
        .attr("x2", d3.mouse(this)[0]);

      title
	.text(format(X_date))
	.attr("opacity", 1);
    });

    svg.select("g.y.axis").call(yAxis);
    svg.select("g.x.axis").call(xAxis);
    svg.select("path.line").attr("d", line);
} else {
	message
	   .attr("opacity", 1);
}
}
	drawGraph ('#display0', 1100, 450);
