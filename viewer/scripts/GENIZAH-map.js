var map;
var center = new google.maps.LatLng(30.30, 31.150);     
function initialize() {

        var gradient = [
                'rgba(0, 128, 0, 0)',
                'rgba(0, 128, 0, 0.2)',
		'rgba(0, 128, 0, 0.4)',
		'rgba(8, 132, 0, 0.6)',
		'rgba(16, 136, 0, 0.8)',
		'rgba(24, 140, 0, 1)',
		'rgba(32, 144, 0, 1)',
		'rgba(40, 148, 0, 1)',
		'rgba(48, 152, 0, 1)',
		'rgba(56, 156, 0, 1)',
		'rgba(64, 160, 0, 1)',
		'rgba(72, 164, 0, 1)',
		'rgba(80, 168, 0, 1)',
		'rgba(88, 172, 0, 1)',
		'rgba(96, 176, 0, 1)',
		'rgba(104, 180, 0, 1)',
		'rgba(112, 184, 0, 1)',
		'rgba(120, 188, 0, 1)',
		'rgba(128, 192, 0, 1)',
		'rgba(135, 195, 0, 1)',
		'rgba(143, 199, 0, 1)',
		'rgba(151, 203, 0, 1)',
		'rgba(159, 207, 0, 1)',
		'rgba(167, 211, 0, 1)',
		'rgba(175, 215, 0, 1)',
		'rgba(183, 219, 0, 1)',
		'rgba(191, 223, 0, 1)',
		'rgba(197, 227, 0, 1)',
		'rgba(207, 231, 0, 1)',
		'rgba(215, 235, 0, 1)',
		'rgba(223, 239, 0, 1)',
		'rgba(231, 243, 0, 1)',
		'rgba(239, 247, 0, 1)',
		'rgba(255, 255, 0, 1)',
		'rgba(255, 247, 0, 1)',
		'rgba(255, 239, 0, 1)',
		'rgba(255, 231, 0, 1)',
		'rgba(255, 223, 0, 1)',
		'rgba(255, 215, 0, 1)',
		'rgba(255, 207, 0, 1)',
		'rgba(255, 199, 0, 1)',
		'rgba(255, 191, 0, 1)',
		'rgba(255, 183, 0, 1)',
		'rgba(255, 175, 0, 1)',
		'rgba(255, 167, 0, 1)',
		'rgba(255, 159, 0, 1)',
		'rgba(255, 151, 0, 1)',
		'rgba(255, 143, 0, 1)',
		'rgba(255, 135, 0, 1)',
		'rgba(255, 128, 0, 1)',
		'rgba(255, 120, 0, 1)',
		'rgba(255, 112, 0, 1)',
		'rgba(255, 104, 0, 1)',
		'rgba(255, 96, 0, 1)',
		'rgba(255, 88, 0, 1)',
		'rgba(255, 80, 0, 1)',
		'rgba(255, 72, 0, 1)',
		'rgba(255, 64, 0, 1)',
		'rgba(255, 56, 0, 1)',
		'rgba(255, 48, 0, 1)',
		'rgba(255, 40, 0, 1)',
		'rgba(255, 32, 0, 1)',
		'rgba(255, 24, 0, 1)',
		'rgba(255, 16, 0, 1)',
		'rgba(255, 0, 0, 1)'];


        var mapOptions = {
                 treasureMode: 'aye',
                 center: center,
                 zoom: 2,
                 mapTypeId: google.maps.MapTypeId.ROADMAP,
                 panControl: false,
                 streetViewControl: false,
                 mapTypeControl: true,
                 minZoom: 2
        };

        map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
        var heatmap = new google.maps.visualization.HeatmapLayer({ data: heatmapData,
                                                                   radius: 20,
                                                                   gradient: gradient});
        heatmap.setMap(map);
}
                
google.maps.event.addDomListener(window, 'load', initialize);
