/**
 * A force directed graph to display nearest neighbour
 */

      var width = 1150,
          height = 900,
          centerNodeSize = 20;
          nodeSize = 10;
          colorScale = d3.scale.category20();

      var svgCanvas = d3.select("#chart").append("svg:svg")
          .attr("width", width)
          .attr("height", height);

      var node_hash = [];

      // Create a hash that allows access to each node by its id
      nodeSet.forEach(function(d, i) {
        node_hash[d.id] = d;
      });
      
      // Append the source object node and the target object node to each link
      linkSet.forEach(function(d, i) {
        d.source = node_hash[d.sourceId];
        d.target = node_hash[d.targetId];
        if (d.sourceId == focalNodeID)
          { d.direction = "OUT"; }
        else
          { d.direction = "IN"; }
      });

      // Create a force layout and bind Nodes and Links
      var force = d3.layout.force()
          .charge(-1000)
          .nodes(nodeSet)
          .links(linkSet)
          .size([width, height])
          //.linkDistance( function(d) {return d.weight} ) // Controls edge length
     	  .linkDistance( function(d) { return d.weight })
	  //.linkStrength( function(d) { return d.weight })
	  //.gravity(0.5)
          .on("tick", tick)
          .start();

      // Draw lines for Links between Nodes
      var link = svgCanvas.selectAll(".gLink")
          .data(force.links())
        .enter().append("g")
          .attr("class", "gLink")
        .append("line")
          .attr("class", "link")
          .style("stroke", "#ccc")
          .attr("x1", function(d) { return d.source.x; })
          .attr("y1", function(d) { return d.source.y; })
          .attr("x2", function(d) { return d.target.x; })
          .attr("y2", function(d) { return d.target.y; });

      // Create Nodes
      var node = svgCanvas.selectAll(".node")
          .data(force.nodes())
        .enter().append("g")
          .attr("class", "node")
          .on("mouseover", nodeMouseover)
          .on("mouseout", nodeMouseout)
          .call(force.drag);

      // Append circles to Nodes
      node.append("circle")
          .attr("x", function(d) { return d.x; })
          .attr("y", function(d) { return d.y; })
          .attr("r", function(d) { if (d.id==focalNodeID) { return centerNodeSize; } else { return nodeSize; } } ) // Node radius
          .style("fill", "White") // Make the nodes hollow looking
          .style("stroke-width", 5) // Give the node strokes some thickness
          .style("stroke", function(d, i) { colorVal = colorScale(i); return colorVal; } ) // Node stroke colors
          .call(force.drag);

      // Append text to Nodes
      node.append("a")
          .attr("xlink:href", function(d) { return d.hlink; })
        .append("text")
          .attr("x", function(d) { if (d.id==focalNodeID) { return 0; } else {return 20;} } )
          .attr("y", function(d) { if (d.id==focalNodeID) { return 0; } else {return -10;} } )
          .attr("text-anchor", function(d) { if (d.id==focalNodeID) {return "middle";} else {return "start";} })
          .attr("font-family", "Arial, Helvetica, sans-serif")
          .style("font", "normal 16px Arial")
          .attr("fill", "Blue")
          .attr("dy", ".35em")
          .text(function(d) { return d.name; });

	// Append text to Link edges
      var linkText = svgCanvas.selectAll(".gLink")
          .data(force.links())

      function tick() {
        link
          .attr("x1", function(d) { return d.source.x; })
          .attr("y1", function(d) { return d.source.y; })
          .attr("x2", function(d) { return d.target.x; })
          .attr("y2", function(d) { return d.target.y; });
      
        node
          .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });

        linkText
          .attr("x", function(d) {
              if (d.target.x > d.source.x) { return (d.source.x + (d.target.x - d.source.x)/2); }
              else { return (d.target.x + (d.source.x - d.target.x)/2); }
          })
          .attr("y", function(d) {
              if (d.target.y > d.source.y) { return (d.source.y + (d.target.y - d.source.y)/2); }
              else { return (d.target.y + (d.source.y - d.target.y)/2); }
          });
      }

      function nodeMouseover() {
        d3.select(this).select("circle").transition()
            .duration(250)
            .attr("r", function(d,i) { if(d.id==focalNodeID) {return 65;} else {return 15;} } );
        d3.select(this).select("text").transition()
            .duration(250)
            .style("font", "bold 20px Arial")
            .attr("fill", "Blue");
      }

      function nodeMouseout() {
        d3.select(this).select("circle").transition()
            .duration(250)
            .attr("r", function(d,i) { if(d.id==focalNodeID) {return centerNodeSize;} else {return nodeSize;} } );
        d3.select(this).select("text").transition()
            .duration(250)
            .style("font", "normal 16px Arial")
            .attr("fill", "Blue");
      }

