#!/usr/local/bin/perl 

use strict;
use CGI;
use GD;
use visualisation::WordCloud2;
use Data::Dumper;
use XML::Simple;

my %hash;        
my $cgi=new CGI;


my $cloud_source=$cgi->param('frag') || "MS-TS-00010-J-00019-00016";
my $outer = $cgi->param('outer');
my $inner = $cgi->param('inner');

my $wc = visualisation::WordCloud2->new(font => 'Ubuntu-R.ttf',
                                font_path => '/usr/share/fonts/truetype/ubuntu-font-family/',
                                background => [255,255,255],
                                image_size => [$cgi->param('width'),$cgi->param('height')],
                                border_padding => '10');

my $count = 1;
my $doc = XMLin("../fragments/$cloud_source.xml", ForceArray=>1,  KeyAttr => [ ]) || die ("Couldn't parse document file)\n" );

foreach my $keywords (  @{$doc->{$outer}}) {
  foreach my $keyword ( @{$keywords->{$inner}}) {
 	last if ($count > 50);
	#print "$keyword->{'name'} = $keyword->{'value'}\n";
	$hash{lc($keyword->{'name'})} = $keyword->{'value'}; 	
  	$count++;
  }
}

$wc->words(\%hash);
my $gd = $wc->cloud();

print "Content-type: image/gif\n\n";

binmode STDOUT; 
print $gd->gif;
