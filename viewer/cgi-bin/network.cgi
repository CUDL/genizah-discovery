#!/usr/local/bin/perl 
####################################################################################
##Author:       Chris Stokoe
##File:         search.cgi 
##Date:         08/03/13
##Version:      1.0
##
## A cheap and cheerful search engine to showcase the current state of the genizah
## text mining dataset
####################################################################################
use strict;
use utf8;

use CGI;
use DBI;
use MLDBM qw(DB_File Storable);
use Fcntl qw(O_RDWR O_CREAT O_RDONLY);
use Data::Dumper;
use Encode;

my %config = ('path' => "../tables/");

my $cgi=new CGI;
my $query=$cgi->param('name');
my $action=$cgi->param('action');

tie my %pro, 'MLDBM', "$config{path}pro.bin", O_RDONLY, 0640 or die $!;
tie my %neighbours, 'MLDBM', "$config{path}neighbours.bin", O_RDONLY, 0640 or die $!;
##############################################################################################################################
sub output {

	my ($action) = @_;
	print "Content-type:text/html; charset=UTF-8\r\n\r\n";
	if ($action eq "display") {
	        my $text = "";
                unless (open(FILE, "./templates/network.html")) { die "Couldn't open display.html" }
                while (<FILE>) { $text .= $_; }
		my $nodes = "var nodeSet = [";
		$nodes .= "{id: \"N1\", name:\"$query\", hlink: \"network.cgi?query=$query&action=display\"}, ";
		my $links = "var linkSet = [";
		my $count = 2;
		my $ref = $neighbours{$query};
		my @people = sort { $ref->{$b} <=> $ref->{$a} } keys % { $ref };
		
		my $max = 0;
		foreach my $person ( @people ) {
		   #if ($ref->{$person} > $max) { $max = $ref->{$person}; }
		   next if ($person eq $query);
		   if ($ref->{$person} > $max) { $max = $ref->{$person}; }
		   my $weight = $ref->{$person} / $max;
	#	   next if ($weight <= 0.125);
		   $weight = $weight * -1;
		   $weight = 300 * $weight;
		   $weight = $weight + 300;
		   $nodes .= "{id: \"N$count\", name:\"$person\", hlink: \"network.cgi?name=$person&action=display\"},";	
		   $links .= " {sourceId: \"N1\", weight: \"$weight\", targetId: \"N$count\"}, ";
		   $count++;	
		}
		$nodes .= "];";
		$links .= "];";
		
		$text =~ s/\%\%nodes\%\%/$nodes/g;
		$text =~ s/\%\%links\%\%/$links/g;
		$text =~ s/\%\%person\%\%/$query/g;
		print $text;

	} 
}
#####################################################################################################################################
#main#
######

if ($action eq "") { output("error"); }
elsif ($query eq "") { output("error"); }
else { output("display"); } 
untie %neighbours;
untie %pro;
