#!/usr/local/bin/perl 

use strict;
use CGI;
use XML::Simple;
use Data::Dumper;
my $cgi=new CGI;


my $frag=$cgi->param('frag') || "MS-TS-00010-J-00019-00016";
my $outer = $cgi->param('outer');
my $inner = $cgi->param('inner');
my $height = $cgi->param('height');
my $width = $cgi->param('width');
my $images = "";

my $doc = XMLin("../fragments/$frag.xml", ForceArray=>1,  KeyAttr => [ ]) || die ("Couldn't parse document file)\n" );

if (scalar keys %{@{$doc->{'BIGRAMS'}}[0]}  == 1) {
	$images =  "<img src=\"./tag.cgi?frag=$frag&outer=BIGRAMS&inner=BIGRAM&height=$height&width=$width\" border=\"0\" alt=\"Slide 1\" class=\"slide\"  id=\"firstSlide\"/>\n";	
}

if (scalar keys %{@{$doc->{'TRIGRAMS'}}[0]}  == 1) {
        $images .=  "<img src=\"./tag.cgi?frag=$frag&outer=TRIGRAMS&inner=TRIGRAM&height=$height&width=$width\" border=\"0\" alt=\"Slide 1\" class=\"slide\"  style=\"display:none;\"/>\n";       
}

if (scalar keys %{@{$doc->{'QUADGRAMS'}}[0]}  == 1) {
        $images .=  "<img src=\"./tag.cgi?frag=$frag&outer=QUADGRAMS&inner=QUADGRAM&height=$height&width=$width\" border=\"0\" alt=\"Slide 1\" class=\"slide\"  style=\"display:none;\"/>\n";       
}


print <<END;
Content-Type: text/html; charset=iso-8859-1

<html lang="en">
<head>
  <meta charset="utf-8" />
  <script src="../scripts/jquery-1.9.1.js"></script>
  <script src="../scripts/jquery-ui.js"></script>
  <script>
	var refId;
	myCounter++;
  	function slideShow() {
    		var displayToggled = false;
    		var current1 = \$('.slide:visible');
    		var nextSlide = current1.next('.slide');
    		var hideoptions = {
        		"direction": "left",
        		"mode": "hide"
    		};
    		var showoptions = {
        		"direction": "right",
        		"mode": "show"
    		};
    		if (current1.is(':last-child')) {
        		current1.effect("slide", hideoptions, 2000);
        		\$("#firstSlide").effect("slide", showoptions, 2000);
    		}
    	else {
        	current1.effect("slide", hideoptions, 2000);
        	nextSlide.effect("slide", showoptions, 2000);
    	     }
	};
	if(myCounter >= 2) {
          clearInterval(refId);
        }
	refId = setInterval("slideShow()", 15000);
	
   </script>
<style>
.slide{
    width:$width px;
    height:$height px;
    display:inline;
    float:left;
    position: absolute;
}

#center
            {
                width: 1100px;
                height: 650px;
                margin-top: auto;
                margin-bottom: auto;
                margin-right: auto;
                margin-left: auto;
                background-color: white;
		 border: 1px solid grey;
            }


</style>
</head>
<body>
<div id="center">
<div id="slides">
                <div class="slides_container">
		$images
</div>
</div>
</div>


</body>
</html>
END
