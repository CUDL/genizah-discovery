#!/usr/local/bin/perl 
####################################################################################
##Author:       Chris Stokoe
##File:         search.cgi 
##Date:         08/03/13
##Version:      1.0
##
## A cheap and cheerful search engine to showcase the current state of the genizah
## text mining dataset
####################################################################################
use strict;
use utf8;

use CGI;
use GD;
use DBI;
use MLDBM qw(DB_File Storable);
use Fcntl qw(O_RDWR O_CREAT O_RDONLY);
use Data::Dumper;
use JSON -support_by_pp;
use WWW::Mechanize;
use Encode;

use nlp::tokeniser;
use nlp::stemmer;
use nlp::tfidf;
use nlp::similarity;
use library::classmark;
my %config = ('path' => "../tables/");

my $cgi=new CGI;
my $query=$cgi->param('query');
my $action=$cgi->param('action');

my @hits;
my %results = ();
my ($description, $fragment, $fragment_recto, $fragment_verso);

my $similarity = nlp::similarity->new();
my $tokeniser = nlp::tokeniser->new('stopword' => 0);
my $lemmatizer = nlp::stemmer->new();
my $tf = nlp::tfidf->new(normalise => 0);
my $library = library::classmark->new();

tie my %postings, 'MLDBM', "$config{path}postings.bin", O_RDONLY, 0640 or die $!;
tie my %documents, 'MLDBM', "$config{path}documents.bin", O_RDONLY, 0640 or die $!;
tie my %df, 'MLDBM', "$config{path}df.bin", O_RDONLY, 0640 or die $!;
tie my %tfidf, 'MLDBM', "$config{path}tfidf.bin", O_RDONLY, 0640 or die $!;
tie my %squares, 'MLDBM', "$config{path}squares.bin", O_RDONLY, 0640 or die $!;
tie my %locations, 'MLDBM', "$config{path}locations.bin", O_RDONLY, 0640 or die $!;
#tie my %mappings, 'MLDBM', "$config{path}mappings.bin", O_RDONLY, 0640 or die $!;
tie my %dates, 'MLDBM', "$config{path}temporal.bin", O_RDONLY, 0640 or die $!;
tie my %names, 'MLDBM', "$config{path}names.bin", O_RDONLY, 0640 or die $!;
tie my %pro, 'MLDBM', "$config{path}pro.bin", O_RDONLY, 0640 or die $!;
tie my %topicmodel, 'MLDBM', "$config{path}topics.bin", O_RDONLY, 0640 or die $!;
##############################################################################################################################
sub display 
{

    my ($fragment) = @_;
    my $description, $fragment_recto, $fragment_verso;
    my $json_url = "http://cudl.lib.cam.ac.uk/view/$fragment.json";
    my $browser = WWW::Mechanize->new( autocheck => 0 );
    # download the json page:
    my $res = $browser->get( $json_url );
    if ($res->is_success()) {
	my $content = $browser->content();
    	#$content = encode ('utf8', $content);
    	my $json = new JSON; 
    	my $json_text = $json->allow_nonref->utf8->relaxed->escape_slash->loose->allow_singlequote->allow_barekey->decode($content);
    	my $des = from_json( $content );
    	$description = $json_text->{descriptiveMetadata}->[0]->{abstract}->{displayForm};
	$fragment_recto = "$fragment-000-00001";
	$fragment_verso = "$fragment-000-00002";
     }
     else
     {
	$description = "Description for item $fragment is not available in the Digital Library";
	$fragment_recto = "../images/empty.png";
	$fragment_verso = "../images/empty.png";
     }
    
     return($description, $fragment, $fragment_recto, $fragment_verso);	
}
##################################################################################################################################
sub kwic_cmp {
    if ($a =~ /^\s*\d+ (.*)/) {
	my $a_words = $1;
	if ($b =~ /^\s*\d+ (.*)/) {
	    my $b_words = $1;
	    return $a_words cmp $b_words;
	}
    }
}
##################################################################################################################################
sub keyword {

	my ($query) = @_;
	my @tokens = $tokeniser->terms($query);
	my $csize = keys %squares;
	
	#build query vector
	my %tf = ();
	my %qv = ();
	my %candidates = ();

	foreach my $term (@tokens) { $term = $lemmatizer->porter($term); $tf{$term}++; }
	 my $max = 0;
	foreach my $term ( sort { $tf{$b} <=> $tf{$a} } keys %tf) {
		if ($max == 0) { $max = $tf{$term}; } 
		my $idf = log(($csize + 1)/($df{$term} + 1))/log(10);
		$qv{$term} = ($tf{$term} / $max) * $idf;
	}

	my $sum_sqrt_query = 0;

	foreach my $term (keys %qv) {
		my $posting = $postings{$term};
		$sum_sqrt_query += $qv{$term} * $qv{$term};
		foreach my $doc (keys %$posting) {
			$candidates{$doc}++; 
		}	
	}

	foreach my $doc (keys %candidates) {
			$results{$doc} = $similarity->cosine_pre(\%qv, $tfidf{$doc}, $sum_sqrt_query, $squares{$doc}) ;
		}
	my @hits = sort { $results{$b} <=> $results{$a} } keys %results;
	return @hits;
}
####################################################################################################################################
sub topics {
	my ($query) = @_;

	foreach my $doc (keys %topicmodel) {
		next unless $topicmodel{$doc}->{$query};
		$results{$doc} = $topicmodel{$doc}->{$query};
	}

	my @hits = sort { $results{$b} <=> $results{$a} } keys %results;
        return @hits;
}
####################################################################################################################################
sub _byexample {
  	
   my ($query) = @_;
   my %qv = ();
   my %candidates = ();
   my $count = 1; 
   my $p = $tfidf{$query};
   my $csize = keys %squares;
   my $max = 0;

   foreach my $term (sort { $p->{$b} <=> $p->{$a} } keys %$p) {
   	last if $count == 20;
	 if ($max == 0) { $max = $p->{$term}; }
         my $idf = log(($csize + 1)/($df{$term}+1))/log(10);
         $qv{$term} = ($p->{$term} / $max) * $idf;
	 $count++;
   } 

   my $sum_sqrt_query = 0;
   foreach my $term (keys %qv) {
         my $posting = $postings{$term};
	 $sum_sqrt_query += $qv{$term} * $qv{$term};
         foreach my $doc (keys %$posting) {
		next if $doc eq $query; 
                $candidates{$doc}++;
         }
   }

   foreach my $doc (keys %candidates) {
          $results{$doc} = $similarity->cosine_pre(\%qv, $tfidf{$doc}, $sum_sqrt_query, $squares{$doc}) ;
   }
   
   my @hits = sort { $results{$b} <=> $results{$a} } keys %results;
   return @hits;
}
####################################################################################################################################
sub output {

	my ($action) = @_;
	print "Content-type:text/html; charset=UTF-8\r\n\r\n";
	if ($action eq "results") {
		print "<html><head><title>Displaying results for $query </title>";
		print "<link rel=\"icon\" type=\"image/png\" href=\"../images/favicon.png\">";
		print "<script>function imgError(image){ image.onerror = \"\"; image.src = \"../images/empty.png\"; return true; } </script>";
                print "<script type=\"text/javascript\" src=\"../scripts/GENIZAH-Tracking.js\"></script>";
		print '</head><body><center>';
                print "<a href=\"../\"><img alt=\"Genizah\" src=\"../images/g.png\" height=\"110\" width=\"326\" border=0></a><br>";
		print "<table>";
		my $count = 1;
		foreach my $hit (@hits) {
			last if ($results{$hit} <= 0);
			my $fimg = "http://cudl.lib.cam.ac.uk//content/images/$hit-000-00001\_files/8/0_0.jpg";
			print "<TR><TD>$count</TD><TD><img alt=\"Fragment\" src=\"$fimg\" width=64 height=64 onerror=\"imgError(this);\"/></TD><TD><a href=\"search.cgi?query=$hit&action=display&tag=$query\">$hit</a href></TD><TD>$results{$hit}</TD></TR>";
			$count++; 
		}
		print '</table></center></body>';
                print '</html>';
	
	} elsif ($action eq "display") {
		
		my $text;
                unless (open(FILE, "./templates/display.html")) { die "Couldn't open display.html" }
                while (<FILE>) { $text .= $_; }
		my $nodes = "var nodeSet = [";
		my $t = $library->reverse($fragment);
		$nodes .= "{id: \"N1\", name:\"$t\", hlink: \"search.cgi?query=$fragment&action=display\"}, ";
		my $links = "var linkSet = [";
		my $count = 2;
		foreach my $hit (@hits) {
		   last if $count > 15;
		   my $weight = $results{$hit} * -1;
		   $weight += 10;
		   $weight = sprintf("%.3f", $weight);
		   $weight *= 200;
		   $weight -= 1800;
		   my $f = $library->reverse($hit);
		   $nodes .= "{id: \"N$count\", name:\"$f\", hlink: \"search.cgi?query=$hit&action=display\"},";	
		   $links .= " {sourceId: \"N1\", weight: \"$weight\", targetId: \"N$count\"}, ";
		   $count++;	
		}
		$nodes .= "];";
		$links .= "];";
		
		my $local = $locations{$fragment};
		my $markers = "";
		foreach my $place ( keys % { $local } ) {
			my ($lat, $lng) = split(/\,/, $local->{$place}->{'centroid'});
			$markers .= "\{location: new google.maps.LatLng($lat, $lng), weight: $local->{$place}->{'weight'}\}, //$place, $local->{$place}->{'cc'} $local->{$place}->{'type'}\n";
		}

		my $timestamps = $dates{$fragment};
		my $times = "";
		for (my $year = 600; $year <= 1890; $year++) {
 			if (exists $timestamps->{$year}) {
				$times .= "\{year: new Date($year, 1, 1), weight: $timestamps->{$year}\},\n";
			} else {
				$times .= "\{year: new Date($year, 1, 1), weight: 0\},\n";
			}
		}

		my $people = $names{$fragment};
		my $pout = "";
		foreach my $person ( sort { $people->{$b} <=> $people->{$a} } keys % { $people } ) {
			
			my $t = $pro{$person};
			my $image = "";
			if ($t->{'gender'} eq "male") { $image = "man-unknown.png"; }
			if ($t->{'gender'} eq "female") { $image = "woman-unknown.png"; }
			if ($t->{'gender'} eq "Unknown") { $image = "unknown.png"; }
			$pout .= "<BR>
					<div class=\"box\">
					        <img alt=\"Genizah\" src=\"../images/$image\" height=\"64\" width=\"64\" border=0 align=left>	
						Name: <a href=\"network.cgi?name=$person&action=display\">$person</a> ($people->{$person}) <BR>
						Occupation: $t->{'occupation'}<BR>
						Period: $t->{'period'}
					
				</div>";
  		}

		my $topics = $topicmodel{$fragment};
		my $t = "";
	        foreach my $topic (keys % { $topics } ) {
			$t .= "{\"name\": \"$topic\", \"size\": \"$topics->{$topic}\", \"url\": \"search.cgi?query=$topic&action=topic\"},\n";
		}
	#	my %totals = ();
	#	foreach my $fragment (keys %topicmodel) {
	#			my $topics = $topicmodel{$fragment};
	#		foreach my $topic (keys  % { $topics } ) {
	#			$totals{$topic}++;
	#		}
	#	}
	#	my $t = "";
	#	foreach my $h (keys %totals) {
	##		$t .= "{\"name\": \"$h\", \"size\": \"$totals{$h}\", \"url\": \"search.cgi?query=$h&action=topic\"},\n";
	#	}
		
	#	my $map = $mappings{$fragment};
	#	my $references = "";
	#	my ($content, $string) = "";
	#	my @tokens = $tokeniser->terms($cgi->param('tag'));
        #        #foreach my $token (@tokens) { $string .= "$token\|"; }
	#	#$string =~ s/\|//;
	#	#foreach my $citation ( keys % { $map } ) {
	#		$references .= "$citation ---> $map->{$citation}\n";
	#		$content .= `/bin/grep -wiHn \"$string\" ../text/$citation`;			
	#	}

		#foreach my $token (@tokens) { $content =~ s/$token/<font color=red>$token<\/font>/gi; }

		##$text =~ s/\%\%debug\%\%/$references/g;
		##$text =~ s/\%\%debug2\%\%/$content/g;

		$text =~ s/\%\%heatmap\%\%/$markers/g;
		$text =~ s/\%\%fragment\%\%/$fragment/g;
		$text =~ s/\%\%metadata\%\%/$description/g;
		$text =~ s/\%\%recto\%\%/$fragment_recto/g;
		$text =~ s/\%\%verso\%\%/$fragment_verso/g;
		$text =~ s/\%\%nodes\%\%/$nodes/g;
		$text =~ s/\%\%links\%\%/$links/g;
                $text =~ s/\%\%sim\%\%/@hits/g;
		$text =~ s/\%\%dates\%\%/$times/g;
		$text =~ s/\%\%names\%\%/$pout/g;
		$text =~ s/\%\%topics\%\%/$t/g;
		print $text;

	} elsif ($action eq "nomatch") {

		print "<html><head><title>Genizah Search </title>";
        	print '</head>';
        	print '<body><center>';
        	print "<br><br><a href=\"../\"><img alt=\"Genizah\" src=\"../images/g.png\" height=\"110\" width=\"326\" border=0></a href><br>";
		print "<pre> Search for $query \n Did not match any documents </pre>";
		print '</center></body>';
        	print '</html>';

	} 
}
#####################################################################################################################################
#main#
######

if ($action eq "") { output("search"); }
elsif ($query eq "") { $query="EMPTY"; output("nomatch"); }
elsif ($action eq "Submit") {
	@hits = keyword($query);
	if (scalar(@hits)) { output("results"); 
	} else {
		output("nomatch");
	}
}
elsif ($action eq "topic") {
	@hits = topics($query);
        if (scalar(@hits)) { 
                output("results");
        } else {
                output("nomatch");
        }
} 
elsif ($action eq "I\'m Feeling Lucky") {
	@hits = keyword($query);
        if (scalar(@hits)) { 
		($description, $fragment, $fragment_recto, $fragment_verso) = display(@hits[0]);
                output("display");
	} else {
        	output("nomatch");
	}
}
elsif ($action eq "display") {
	($description, $fragment, $fragment_recto, $fragment_verso) = display($query);	
	@hits = _byexample($fragment);
        output("display"); 
} 
#else { output("search"); }

untie %postings;
untie %documents;
untie %df; 
untie %squares;
untie %locations;
#untie %mappings;
untie %names;
untie %pro;
untie %topicmodel;
