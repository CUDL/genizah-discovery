#!/usr/local/bin/perl 
####################################################################################
##Author:       Chris Stokoe
##File:         map.cgi 
##Date:         08/03/13
##Version:      1.0
##
## A cheap and cheerful map visualisation for the entire genizah dataset
## 
####################################################################################
use strict;
use utf8;

use CGI;
use GD;
use DBI;
use MLDBM qw(DB_File Storable);
use Fcntl qw(O_RDWR O_CREAT O_RDONLY);
use Data::Dumper;
use JSON -support_by_pp;
use WWW::Mechanize;
use Encode;

use nlp::tokeniser;
use nlp::tfidf;
use nlp::similarity;
use library::classmark;
my %config = ('path' => "../tables/");

my $cgi=new CGI;
my $query=$cgi->param('query');
my $action=$cgi->param('action');

my @hits;
my %results = ();
my ($description, $fragment, $fragment_recto, $fragment_verso);

my $similarity = nlp::similarity->new();
my $tokeniser = nlp::tokeniser->new('stopword' => 0);
my $tf = nlp::tfidf->new(normalise => 0);
my $library = library::classmark->new();

tie my %locations, 'MLDBM', "$config{path}locations.bin", O_RDONLY, 0640 or die $!;
##############################################################################################################################
sub output {

		  print "Content-type:text/html; charset=UTF-8\r\n\r\n";
		
		my $text;
                unless (open(FILE, "./templates/map.html")) { die "Couldn't open display.html" }
                while (<FILE>) { $text .= $_; }
		my %consolidated = ();
		foreach my $fragment ( keys %locations ) {
			my $local = $locations{$fragment};
			foreach my $place ( keys % { $local } ) {
				$consolidated{$place}{'weight'} += $local->{$place}->{'weight'};
				$consolidated{$place}{'centroid'} = $local->{$place}->{'centroid'};
				$consolidated{$place}{'type'} = $local->{$place}->{'type'};
				$consolidated{$place}{'cc'} = $local->{$place}->{'cc'};
			}
		}
		my $markers = "";
		foreach my $place ( keys %consolidated ) {
			 next if $place =~ /Europe/;
			 next if $consolidated{$place}  eq "Misc"; 
			 my ($lat, $lng) = split(/\,/, $consolidated{$place}->{'centroid'});
                        $markers .= "\{location: new google.maps.LatLng($lat, $lng), weight: $consolidated{$place}->{'weight'}\}, //$place, $consolidated{$place}{'cc'}, $consolidated{$place}->{'type'}\n";

		}

		
		$text =~ s/\%\%heatmap\%\%/$markers/g;
		print $text;
}
#####################################################################################################################################
#main#
######

output();

untie %locations;
