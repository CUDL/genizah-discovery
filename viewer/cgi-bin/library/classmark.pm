####################################################################################
##Author:       Huw
##File:         classmark.pm 
##Date:         18/03/13
##Version:      1.0
####################################################################################
package library::classmark;

use strict;
use warnings;

use Data::Dumper;

sub new {
        my $class = shift;
        my %defaults =  ();
	my $self = {%defaults, @_};
        bless $self, $class;
        return $self;
}
####################################################################################
sub classmark {

     my $self = shift;
     my $text = shift;

     #upper case
     $text=uc($text);

     #sorts out T-S->TS
     $text=~s/^T\-S/TS/;

     #spaces to hyphens
     $text=~s/ /\-/g;

     #dots to hyphens
     $text=~s/\./\-/g;

     #surround brackets with hyphens
     $text=~s/\((.+)\)/\-\($1\)\-/g;

     #put a hyphen between letters and digits
     $text=~s/([A-Z])(\d)/$1\-$2/g;

     #put a hyphen between digits and letters
     $text=~s/(\d)([A-Z])/$1\-$2/g;

     #splits into constituent parts
     my @parts=split(/\-/, $text);

     my $part;

     #to tell which part you're on
     my $counter=0;

     #goes through parts
     foreach $part(@parts){

         #increment counter
         $counter++;

         $part=~s/^\(//;
         $part=~s/\)$//;

         #pad digits
         if ($part=~/(\d+)/){


             $part=~s/\d+/sprintf("%05d", $&)/ge;

         }

     }

     unshift (@parts, "MS");

     #sticks it all back together again
     $text=join('-', @parts);


     #just in case double hyphens have crept in ... -

     $text=~s/\-\-/\-/g;

     #hack for oddities
     $text=~s/LEAF\-001\-\+\-006/L1\-L6/;
     $text=~s/LEAF\-002\-\+\-005/L2\-L5/;
     $text=~s/LEAF\-003\-\+\-004/L3\-L4/;

     return $text;
}
#####################################################################################
sub reverse {

     my $self = shift;
     my $text = shift;
 
     $text =~ s/MS-//g;
     my @parts = split(/-/, $text);
     foreach my $part (@parts) {
     	$part =~ s/0*(\d+)/$1/;
	
     }
     $text=join(' ', @parts);
     return $text    
}
#####################################################################################
1;
