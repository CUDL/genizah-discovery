#!/usr/local/bin/perl 

use strict;
use CGI;

my $cgi=new CGI;


my $frag=$cgi->param('frag') || "MS-TS-00010-J-00019-00016";
my $outer = $cgi->param('outer');
my $inner = $cgi->param('inner');
my $height = $cgi->param('height');
my $width = $cgi->param('width');
 

print <<END;
Content-Type: text/html; charset=iso-8859-1

<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>jQuery UI Slider - Default functionality</title>
  <script src="../scripts/jquery-1.9.1.js"></script>
  <script src="../scripts/jquery-ui.js"></script>
  <script>
  \$(function() {
    \$( "slider #1" ).fadeIn(500);
  });
  </script>
</head>
<body>

<div id="slider">
	<img id="1" src="./tag.cgi?frag=$frag&outer=BIGRAMS&inner=BIGRAM&height=800&width=1000" border="0" alt="Image 1"/>
        <img id="2" src="./tag.cgi?frag=$frag&outer=TRIGRAMS&inner=TRIGRAM&height=800&width=1000" border="0" alt="Image 2"/>
        <img id="3" src="./tag.cgi?frag=$frag&outer=QUADGRAMS&inner=QUADGRAM&height=800&width=1000" border="0" alt="Image 3"/>
</div>


</body>
</html>
END
