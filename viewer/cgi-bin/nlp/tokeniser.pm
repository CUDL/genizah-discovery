####################################################################################
##Author:       Chris Stokoe
##File:         tokeniser.pm 
##Date:         28/02/13
##Version:      1.0
####################################################################################
package nlp::tokeniser;

use File::Spec;

sub new {
        my $class = shift;
	my ($volume, $directory) = File::Spec->splitpath( $INC{'nlp/tokeniser.pm'} );
	my $corpus = File::Spec->catpath ( $volume, $directory, '../../resources/genizah-stopwords.txt');
	my $general = File::Spec->catpath ( $volume, $directory, '../../resources/combined-stopwords.txt');
        my %defaults =  (stopword => '1',
			 corpus => $corpus, 
			 general => $general);
	
	my $self = {%defaults, @_};
	if ($self->{stopword} == 1) {
		if ($self->{general} eq '') { 
	         	my @stopwords = ['a', 'an', 'and', 'are', 'as', 'at', 'be', 
                        	 	 'but', 'by', 'for', 'if', 'in', 'into', 'is', 
                         		 'it', 'no', 'not', 'of', 'on', 'or', 's', 'such', 
                         		 't', 'that', 'the', 'their', 'then', 'there', 
                         		 'these', 'they', 'this', 'to', 'was', 'will', 'with'];
			$self->{stopwords} = map { $_ => 1 } @stopwords;
		} else {
			unless (open(STOPWORDS, $self->{general})) { die "Cant find stopwords file $self->{general}\n" };
   			while(chomp($stop = <STOPWORDS>)) { $self->{stopwords}{$stop} = 1; } ;
   			close(STOPWORDS);
		}
		if ($self->{corpus} ne '') {
			unless (open(STOPWORDS, $self->{corpus})) 
				{ die "Cant find stopwords file $self->{corpus}\n" };
                        while(chomp($stop = <STOPWORDS>)) { $self->{stopwords}{$stop} = 1; } ;
                        close(STOPWORDS);      
		}	
	}
        bless $self, $class;
        return $self;
}
####################################################################################
sub terms {
        my $self = shift;	
        my $text = shift;
	my @tokens;
        $text = lc($text);
        $text =~ s/\n/ /g;
	$text =~ s/(\w)\'(\w)/$1$2/g;
        $text =~ s/[^A-Za-z\-]/ /g;
        $text =~ s/\s-/ /g;
        $text =~ s/\s+/ /g;
        $text =~ s/^\s//g;
        $text =~ s/\s$//g;
	if ($self->{stopword} == 1) {
		foreach $candidate (split(/\s/, $text)) {
			next if (exists $self->{stopwords}{$candidate});
			next if ($candidate =~ /^\w$/);
			next if ($candidate =~ /^\w\w$/);
			push (@tokens, $candidate);
		}
	} else { @tokens = split(/\s/, $text); }
	#$self->{tokens} = \@tokens;
        return @tokens;
}
#####################################################################################
sub sentences {
# If needed ? 
}
1;
